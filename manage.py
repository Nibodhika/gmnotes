from app import create_app
from app.models import db
from flask_script import Manager, Shell

app = create_app()
manager = Manager(app)


def make_shell_context():
    return dict(app=app, db=db)


manager.add_command('shell', Shell(make_context=make_shell_context))


@manager.command
def init(name='admin', password='admin'):
    """
    Initializes the database for the current FLASK_CONFIG.
    WARNING: It destroys the database first, only use this for the first run
    """
    db.drop_all()
    db.create_all()
    from app.models import ServerRole, Player
    # Create the basic roless
    admin_role = ServerRole(name='admin')
    db.session.add(admin_role)
    manager = ServerRole(name='manager')
    db.session.add(manager)
    # Create an admin user
    admin_user, _ = Player.create_player(name=name, password=password)
    admin_user.roles.append(admin_role)
    db.session.add(admin_user)

    db.session.commit()

    from app.system import manager
    for system in manager.systems.values():
        system.init()


@manager.command
def bots():
    """
    Run the current bots in an infinite loop, without the rest of the flask framework
    """
    from app.models import Bot
    if len(Bot.query.all()) == 0:
        print("No bots to run")
        return
    from app.discord_bot import manager as bot_manager
    thread = bot_manager.run()
    print("Press Ctrl+C to quit")
    try:
        thread.join()
    except KeyboardInterrupt:
        print("Closing down")
        bot_manager.stop()


if __name__ == '__main__':
    manager.run()
