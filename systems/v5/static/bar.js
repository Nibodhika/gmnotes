
class BasicBar {
    constructor(bar_id, total_id, boxes=10) {
        this.bar_elem = $(bar_id)
        this.total_elem = $(total_id)
        this.boxes = boxes;
    }

    update_bar(){
        throw new Error("Not implemented error")
    }

    set_total(value){
        this.total_elem.val(value);
        this.update_bar();
    }

    box(classes){
        return `<i class="${classes}"></i> `;
    }

    box_empty(){
        return this.box('far fa-square');
    }

    box_full(){
        return this.box('fas fa-square');
    }
}

class Bar extends BasicBar {
    constructor(bar_id, total_id, aggravated_id, superficial_id, boxes=10) {
        super(bar_id, total_id, boxes)
        this.aggravated_elem = $(aggravated_id)
        this.superficial_elem = $(superficial_id)
        this.update_bar();
    }

    box_superficial(){
        return this.box('far fa-minus-square');
    }

    box_aggravated(){
        return this.box('far fa-plus-square');
    }

    update_bar(){
        var total = parseInt(this.total_elem.val());
        var aggravated = parseInt(this.aggravated_elem.val());
        var superficial = parseInt(this.superficial_elem.val());

        var html = "";
        for(var i = 0; i < this.boxes; i++){
            if(i < aggravated){
                html += this.box_aggravated();
            }
            else if(i < (aggravated+superficial)){
                html += this.box_superficial();
            }
            else if(i < total){
                html += this.box_empty();
            }
            else {
                html += this.box_full();
            }
        }

        this.bar_elem.html(html);
    }

}

class HumanityBar extends BasicBar {

    constructor(bar_id, total_id, stains_id) {
        super(bar_id, total_id, 10)
        this.stains_elem = $(stains_id)
        this.update_bar();
    }

    box_stain(){
        return this.box('far fa-minus-square');
    }

    update_bar(){
        var total = parseInt(this.total_elem.val());
        var stains = parseInt(this.stains_elem.val());
        var ts = 10-stains;
        var html = "";
        for(var i = 0; i < this.boxes; i++){
            if(i < total)
                html += this.box_full();
            else if(i >= ts)
                html += this.box_stain();
            else
                html += this.box_empty();
        }

        this.bar_elem.html(html);
    }

}

var health_bar = new Bar("#health_bar", "#health","#h_aggravated","#h_superficial")
// Health is stamina+3
$("#stamina").on('change', function(a){
    new_value = parseInt(a.target.value)
    health_bar.set_total(new_value + 3)
})


var willpower_bar = new Bar("#willpower_bar", "#willpower","#w_aggravated","#w_superficial")
// Willpower is composure + resolve
$("#composure").on('change', function(a){
    new_value = parseInt(a.target.value)
    other_value = parseInt($('input[name=resolve]:checked').val())
    willpower_bar.set_total(new_value + other_value)
})
$("#resolve").on('change', function(a){
    new_value = parseInt(a.target.value)
    other_value = parseInt($('input[name=composure]:checked').val())
    willpower_bar.set_total(new_value + other_value)
})

var humanity_bar = new HumanityBar("#humanity_bar", "#humanity","#stains")