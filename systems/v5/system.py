from itertools import zip_longest

import os

import yaml
from flask import render_template
from app.system import System, SystemView
from colors import cprint

from .commands import cmd_list


class V5View(SystemView):
    LIST_VIEW_TEMPLATE = "v5_list_view.html"

    def character_short_view(self, character):
        if character.is_npc:
            return super(V5View, self).character_short_view(character)
        return render_template("short_sheet.html", character=character)

    def build_character_elements(self, character, form, access_level):
        from systems.v5.models import Advantage, ThinBloodAdvantage, Discipline, Power
        from app.models import AccessLevel
        original_elements = super(V5View, self).build_character_elements(character, form, access_level)
        # Only the GM can see NPC's sheet, so if this is an NPC and we're not the GM return the original elements
        if character and character.is_npc and access_level != AccessLevel.GM:
            return original_elements

        if access_level & AccessLevel.EDIT:
            sheet = render_template("long_sheet.html", form=form)
            believes = render_template("believes.html", form=form)
            advantages = render_template("advantages.html", form=form, template_options=Advantage.choices())
            thin_blood_advantages = render_template("thin_blood_advantages.html", form=form,
                                                    template_options=ThinBloodAdvantage.choices())
            disciplines = render_template("disciplines.html", form=form, template_options=Discipline.choices())
            powers = render_template("powers.html", form=form, template_options=Power.choices())
        else:
            sheet = render_template("long_sheet.html", character=character)
            believes = render_template("believes.html", character=character)
            advantages = render_template("advantages.html", character=character)
            thin_blood_advantages = render_template("thin_blood_advantages.html", character=character)
            disciplines = render_template("disciplines.html", character=character)
            powers = render_template("powers.html", character=character)

        # Order matters, we want V5 data at the top
        elements = {'V5': [{
            'title': 'Sheet',
            'content': sheet,
            'id': 'concept'
        }, {
            'title': 'Believes',
            'content': believes,
            'id': 'believes'
        }, {
            'title': 'Advantages',
            'content': advantages,
            'id': 'advantages'
        }, {
            'title': 'Thin Blood Advantages',
            'content': thin_blood_advantages,
            'id': 'thin_blood_advantages'
        }, {
            'title': 'Disciplines',
            'content': disciplines,
            'id': 'disciplines'
        }, {
            'title': 'Powers',
            'content': powers,
            'id': 'powers'
        },
        ]}
        # Add the original elements
        for k in original_elements:
            elements[k] = original_elements[k]

        return elements

    def get_character_form(self, campaign, character):
        from systems.v5.forms import VampireForm
        from app.models import Player
        from systems.v5.models import Skill, HumanSkill, Advantage, ThinBloodAdvantage, \
            Discipline, Power, Clan, PredatorType, Resonance

        # First we need to ensure the character has all the skills
        # so that they get populated correctly on VampireForm constructor
        # Note that this will not actually add to the database unless a save is called afterwards
        if character:
            for skill in Skill.query.all():
                character_skill = HumanSkill.query.filter_by(skill=skill, character=character).first()
                if character_skill is None:
                    # We need to set both skill and skill_id because we use skill_id and the long_sheet uses skill.name
                    # when not editing
                    HumanSkill(skill=skill, skill_id=skill.id, character=character, value=0)

        form = VampireForm(obj=character)
        # Player choices
        player_choices = [(0, 'NPC')]
        # TODO eventually this needs to list the friends of the current user instead of all users
        for p in Player.query.all():
            # Only add players that don't have already have a player in this campaign
            # But also add the owner if the character exists
            if p in campaign.players:
                if not character or character and p is not character.player:
                    continue
            # GM can't have a player
            if p == campaign.gm:
                continue
            player_choices.append((p.id, p.name))
        form.player_id.choices = player_choices

        # clan choices
        clan_choices = Clan.choices()
        form.clan_id.choices = clan_choices

        # Predator Type choices
        form.predator_type_id.choices = PredatorType.choices()

        # Resonance choices
        form.resonance_id.choices = Resonance.choices()

        # If we're creating a new character the skill list was not populated automatically
        if len(form.skills) == 0:
            for skill in Skill.query.all():
                form.skills.append_entry()
                form.skills[-1].skill_id.data = skill.id
                form.skills[-1].value.data = 0

        # Put the skills in order as one physical, one social and one mental
        from systems.v5.models.skill import SkillType
        skills = {
            SkillType.Physical: [],
            SkillType.Social: [],
            SkillType.Mental: [],
        }
        while True:
            try:
                # Pop from the beginning because they will get added in the same order
                s = form.skills.entries.pop(0)
                skill = Skill.query.get(s.skill_id.data)
                # For the form on the sheet each skill only should have it's skill_id combo that selects only one skill
                s.skill_id.choices = [(skill.id, skill.name)]
                skills[skill.skill_type].append(s)
            except IndexError:
                break

        for x in zip_longest(*[i for i in skills.values()]):
            form.skills.entries.extend(x)

        # Advantage
        advantage_choices = Advantage.choices()
        for a in form.advantages:
            a.advantage_id.choices = advantage_choices

        # Thin blood advantage
        thin_blood_advantage_choices = ThinBloodAdvantage.choices()
        for a in form.thin_blood_advantages:
            a.thin_blood_advantage_id.choices = thin_blood_advantage_choices

        # Disciplines
        disciplines_choices = Discipline.choices()
        for a in form.disciplines:
            a.discipline_id.choices = disciplines_choices

        # Powers
        powers_choices = Power.choices()
        for a in form.powers:
            a.power_id.choices = powers_choices

        return form

    def get_character_model(self):
        from systems.v5.models import Vampire
        return Vampire()


class V5System(System):

    def __init__(self):
        super(V5System, self).__init__('v5', "Vampire the Masquerade 5th", cmd_list, V5View)
        from .commands import rule
        folder = os.path.abspath(os.path.dirname(__file__))
        rule.rules_file = os.path.join(folder, "rules.yaml")
        rule.validate_rules()

    def init(self):
        from app.models import db
        from systems.v5.models import Clan, PredatorType, Resonance, Skill, Advantage, ThinBloodAdvantage, Discipline, \
            Power, PowerRequirement, PowerPrerequisite, Alias

        folder = os.path.abspath(os.path.dirname(__file__))
        with open(os.path.join(folder, "initial_data.yaml"), 'r') as stream:
            data = yaml.safe_load(stream)

        def power_constructor(*args, **kwargs):

            # Deal with requirements
            if 'requirements' in kwargs:
                for requirement in kwargs['requirements']:
                    if 'name' in requirement:
                        discipline = Discipline.query.filter_by(name=requirement['name']).first()
                        if discipline is None:
                            err = "Problem importing data, {} is not a valid discipline name".format(
                                requirement['name'])
                            cprint(err, color='red')
                            continue
                        requirement['discipline_id'] = discipline.id
                        del requirement['name']
                # Only use the ones that we were able to populate discipline_id
                requirements = [i for i in kwargs['requirements'] if 'discipline_id' in i]
                kwargs['requirements'] = []
                for r in requirements:
                    kwargs['requirements'].append(PowerRequirement(**r))

            # Deal with prerequisites
            if 'prerequisites' in kwargs:
                # Since a prerequisite is another power better commit first
                db.session.commit()
                for prerequisite in kwargs['prerequisites']:
                    if 'name' in prerequisite:
                        power = Power.query.filter_by(name=prerequisite['name']).first()
                        if power is None:
                            err = "Problem importing data, {} is not a valid power name".format(
                                prerequisite['name'])
                            cprint(err, color='red')
                            continue
                        prerequisite['prerequisite_id'] = power.id
                        del prerequisite['name']
                # Only use the ones that we were able to populate prerequisite_id
                prerequisites = [i for i in kwargs['prerequisites'] if 'prerequisite_id' in i]
                kwargs['prerequisites'] = []
                for r in prerequisites:
                    kwargs['prerequisites'].append(PowerPrerequisite(**r))

            return Power(*args, **kwargs)

        initializer = {
            'clans': Clan,
            'predator_types': PredatorType,
            'resonances': Resonance,
            'skills': Skill,
            'advantages': Advantage,
            'thin_blood_advantages': ThinBloodAdvantage,
            'disciplines': Discipline,
            'powers': power_constructor,
            'aliases': Alias,
        }
        for list_name in initializer:
            # Powers requires the disciplines to have already been populated
            if list_name == 'powers':
                db.session.commit()
            model = initializer[list_name]
            for elem_values in data[list_name]:
                elem = model(**elem_values)
                db.session.add(elem)

        db.session.commit()


system = V5System()
