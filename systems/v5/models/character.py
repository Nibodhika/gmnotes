from app.models import Character as BaseCharacter

from app.models.commons import build_fk
from .clan import Clan
from .predator_type import PredatorType
from .resonance import Resonance
from systems.v5.system import system
from .commons import Dots

db = system.db


class Human(system.Mixin, BaseCharacter):

    id = db.Column(db.Integer, db.ForeignKey(BaseCharacter.id), primary_key=True)
    concept = db.Column(db.String(80))
    # Maybe these should be allowed to be larger, but they're meant to be short descriptions
    ambition = db.Column(db.String(80))
    desire = db.Column(db.String(80))

    # Physical
    strength = Dots('strength')
    dexterity = Dots('dexterity')
    stamina = Dots('stamina')

    # Social
    charisma = Dots('charisma')
    manipulation = Dots('manipulation')
    composure = Dots('composure')

    # Mental
    intelligence = Dots('intelligence')
    wits = Dots('wits')
    resolve = Dots('resolve')

    # Health
    health = Dots('health', 4, 10)
    h_superficial = Dots('h_superficial', 0, 10)
    h_aggravated = Dots('h_aggravated', 0, 10)

    @property
    def current_health(self):
        return self.health - self.h_superficial - self.h_aggravated

    # Willpower
    willpower = Dots('willpower', 2, 10)
    w_superficial = Dots('w_superficial', 0, 10)
    w_aggravated = Dots('w_aggravated', 0, 10)

    @property
    def current_willpower(self):
        return self.willpower - self.w_superficial - self.w_aggravated

    xp = db.Column(db.Integer, default=0, nullable=True)

    # Since we have two character classes the default Mixin is not enough
    __mapper_args__ = {
        'polymorphic_identity': 'v5_human',
    }

    def get_attributes(self):
        return {
            "Physical": [
                (i.capitalize(), getattr(self, i)) for i in ['strength', 'dexterity', 'stamina']
            ],
            "Mental": [
                (i.capitalize(), getattr(self, i)) for i in ['charisma', 'manipulation', 'composure']
            ],
            "Social": [
                (i.capitalize(), getattr(self, i)) for i in ['intelligence', 'wits', 'resolve']
            ],
        }

    def _elem(self, elem, model, elem_type, elem_name, elem_name_id=None):
        """
        Generic function that receives an element and checks a table to return an entry if it exists.
        It assumes the character is referenced as character
        It assumes the name of the id elem is elem_name +' _id', e.g. skill and skill_id.
        If that's not the case set elem_name_id

        :param elem:
        :param model:
        :param elem_type:
        :param elem_name:
        :return:
        """
        search_params = {'character': self}
        if type(elem) == int:
            if elem_name_id is None:
                elem_name_id = elem_name + '_id'
            search_params[elem_name_id] = elem
        elif type(elem) == elem_type:
            search_params[elem_name] = elem
        else:
            raise ValueError("{} is neither a {} nor a valid id (int)".format(elem, elem_type))

        out = model.query.filter_by(**search_params).first()
        return out

    def _get_elem(self, elem, model, elem_type, elem_name, elem_name_id=None):
        """
        Receive an Element or the id of an element (int)
        and return the value associated with that element and this character or 0 if None is found

        :param elem:
        :param model:
        :param elem_type:
        :param elem_name:
        :return: int
        """
        out = self._elem(elem, model, elem_type, elem_name, elem_name_id)

        if out:
            return out.value
        return 0

    def _set_elem(self, elem, value, model, elem_type, elem_name, elem_name_id=None, remove_on_zero=True, set_value=None):
        """
        Given an Element or the id of an element (int) sets the value associated with that element and this character

        :param elem: Either an int or an element_type, e.g. 1 or <Skill Athletic>
        :param value: The value to set
        :param model: The table model to search for the element
        :param elem_type: The type that elem should be if it's not an int
        :param elem_name: The name in the model that refers to the elem
        :param elem_name_id: The name in the model that refers to the id, if left None elem_name+'_id' will be used
        :param remove_on_zero: Whether when setting the value to 0 should delete the entry
        :param set_value: Optional function to set the value, if left None it will try to set the attribute value
        :return:
        """
        found_elem = self._elem(elem, model, elem_type, elem_name, elem_name_id)

        if set_value is None:
            # The default set_value is just setting an attribute called value
            def set_value(found_elem, value):
                found_elem.value = value

        if remove_on_zero and (value == 0):
            if found_elem:
                # Special case where the element exists but we are deleting it
                # It should remove and return
                # If we should remove it but we didn't found it nothing to do
                try:
                    db.session.delete(found_elem)
                    db.session.commit()
                except Exception as e:
                    from colors import cprint
                    db.session.rollback()
                    cprint(e, color='red')
                    return e
            return None
        elif found_elem and set_value:
            set_value(found_elem, value)
        elif found_elem:
            found_elem.value = value
        else:
            create_params = {'character': self}
            if type(elem) == elem_type:
                create_params[elem_name] = elem
            elif type(elem) == int:
                if elem_name_id is None:
                    elem_name_id = elem_name + '_id'
                create_params[elem_name_id] = elem
            found_elem = model(**create_params)
            if set_value:
                set_value(found_elem, value)
            else:
                found_elem.value = value

        try:
            db.session.add(found_elem)
            db.session.commit()
        except Exception as e:
            # TODO parse the error better
            db.session.rollback()
            return e

        return None

    def skill(self, skill):
        """
        Receive a Skill or the id of a skill (int) and return the value associated with that skill and this character

        :param skill: Skill or int
        :return:
        """
        from systems.v5.models import HumanSkill, Skill
        return self._get_elem(skill, HumanSkill, Skill, 'skill')

    def set_skill(self, skill, value):
        """
        Given a Skill or the id of a skill (int) sets the value associated with that skill and this character

        :param skill:
        :param value:
        :return:
        """
        from systems.v5.models import HumanSkill, Skill
        return self._set_elem(skill, value, HumanSkill, Skill, 'skill', remove_on_zero=False)

    def specialization(self, skill):
        """
        Receive a Skill or the id of a skill (int)
        and return the list of specializations associated with that skill and this character.

        :param skill: Skill or int
        :return:
        """
        from systems.v5.models import HumanSkill, Skill
        out = self._elem(skill, HumanSkill, Skill, 'skill')

        if out:
            return out.specializations
        return []

    def set_specialization(self, skill, specializations):
        from systems.v5.models import HumanSkill, Skill
        out = self._elem(skill, HumanSkill, Skill, 'skill')
        if not out or out.value == 0:
            raise ValueError("Cannot set speciality for a skill with value 0")
        out.specializations = specializations
        try:
            db.session.add(out)
            db.session.commit()
        except Exception as e:
            db.rollback()
            from colors import cprint
            cprint(e, color='red')
            return e
        return None

    def advantage(self, advantage):
        """
        Receive an Advantage or the id of an advantage (int)
        and return the value associated with that advantage and this character

        :param skill: Skill or int
        :return:
        """
        from systems.v5.models import HumanAdvantage, Advantage
        return self._get_elem(advantage, HumanAdvantage, Advantage, 'advantage')

    def set_advantage(self, advantage, value):
        """
        Given an Advantage or the id of an advantage (int) sets the value associated with that skill and this character

        :param skill:
        :param value:
        :return:
        """
        from systems.v5.models import HumanAdvantage, Advantage
        return self._set_elem(advantage, value, HumanAdvantage, Advantage, 'advantage')


class Vampire(Human):
    id = db.Column(db.Integer, db.ForeignKey(Human.id), primary_key=True)
    # Sire was originally a fk, but it didn't made much sense since that would mean to have to create sheets
    # for every vampire in the lineage. Also if sire was a fk, it could autopopulate clan and generation
    sire = db.Column(db.String(80))

    clan_id, clan = build_fk('v5_clan.id', Clan, 'members')
    predator_type_id, predator_type = build_fk('v5_predator_type.id', PredatorType, 'vampires', nullable=True)
    resonance_id, resonance = build_fk('v5_resonance.id', Resonance, 'vampires', nullable=True)
    generation = Dots('generation', 13, 15)

    # Humanity
    humanity = Dots('humanity', 7, 10)
    stains = Dots('stains', 0, 10)

    @property
    def free_humanity(self):
        return 10 - self.humanity - self.stains

    hunger = Dots('hunger', 0, 5, 0)
    blood_potency = Dots('hunger', 0, 10)

    # Since we have two character classes the default Mixin is not enough
    __mapper_args__ = {
        'polymorphic_identity': 'v5_vampire',
    }

    @property
    def is_thin_blood(self):
        return self.generation > 13

    def discipline(self, discipline):
        from systems.v5.models import VampireDiscipline, Discipline
        return self._get_elem(discipline, VampireDiscipline, Discipline, 'discipline')

    def set_discipline(self, discipline, value):
        """
        Given a Discipline or the id of a discipline (int) sets the value associated with that discipline
        and this character

        :param discipline:
        :param value:
        :return:
        """
        from systems.v5.models import VampireDiscipline, Discipline
        return self._set_elem(discipline, value, VampireDiscipline, Discipline, 'discipline')

    def power(self, power):
        from systems.v5.models import VampirePower, Power
        return self._get_elem(power, VampirePower, Power, 'power')

    def set_power(self, power, value):
        from systems.v5.models import VampirePower, Power
        # Powers have no value, setting to anything other than 0 means add it, while setting to 0 means remove it
        return self._set_elem(power, value, VampirePower, Power, 'power', set_value=lambda e, v: None)

    def thin_blood_advantage(self, power):
        from systems.v5.models import VampireThinBloodAdvantage, ThinBloodAdvantage
        return self._get_elem(power, VampireThinBloodAdvantage, ThinBloodAdvantage, 'thin_blood_advantage')

    def set_thin_blood_advantage(self, power, value):
        from systems.v5.models import VampireThinBloodAdvantage, ThinBloodAdvantage
        # Thin Blood Advantages have no value
        # Setting to anything other than 0 means add it, while setting to 0 means remove it
        return self._set_elem(power, value, VampireThinBloodAdvantage, ThinBloodAdvantage,
                              'thin_blood_advantage', set_value=lambda e, v: None)

    @classmethod
    def check_clan_generation(cls, clan, generation):
        """
        If generation is greater than 13 clan should be Thin Blood, and vice-versa

        :param clan: Clan instance to check
        :param generation: int generation to check
        :return: bool whether the clan and generation are coherent
        """
        thin_blood_clan = Clan.thin_blood_clan()
        return (clan == thin_blood_clan) == (generation > 13)
