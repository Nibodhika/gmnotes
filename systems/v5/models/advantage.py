from sqlalchemy.orm import validates

from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Human
from .commons import db, Dots


class Advantage(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    is_flaw = db.Column(db.Boolean, nullable=False, default=False)
    points = db.Column(db.Integer, nullable=False)
    # if set to True points is expected to be the maximum point (included)
    variable_points = db.Column(db.Boolean, nullable=False, default=False)
    min_points = db.Column(db.Integer, nullable=False, default=1)
    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Advantage {}>".format(self.name)

    @classmethod
    def choices(cls):

        def choice_elem(advantage):
            name = advantage.name
            if advantage.variable_points:
                name += " ({}-{})".format(advantage.min_points, advantage.points)
            else:
                name += " ({})".format(advantage.points)
            return advantage.id, name

        return {
            "Merits": [choice_elem(a) for a in cls.query.filter_by(is_flaw=False).all()],
            "Flaws": [choice_elem(a) for a in cls.query.filter_by(is_flaw=True).all()]
        }

    def valid_points(self):
        if self.variable_points:
            return [i for i in range(self.min_points, self.points+1)]
        else:
            return [self.points]

    def is_points_valid(self, value):
        if self.variable_points:
            return self.min_points <= value <= self.points
        else:
            return value == self.points


class HumanAdvantage(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = Dots('value', default=0, maximum=6)
    specification = db.Column(db.Text, nullable=True)
    character_id, character = build_fk('v5_human.id', Human, 'advantages', cascade="all, delete-orphan")
    advantage_id, advantage = build_fk('v5_advantage.id', Advantage, 'characters', cascade="all, delete-orphan")

    def __repr__(self):
        return "<{}: {} {}>".format(self.character.name, self.advantage.name, self.value)

    # See issue #17
    # @validates('value', 'advantage_id')
    # def validate_thin_blood(self, key, field):
    #     advantage = None
    #     value = None
    #     if key == 'value' and self.advantage_id:
    #         advantage = Advantage.query.get(self.advantage_id)
    #         value = field
    #     elif key == 'advantage_id' and self.value:
    #         advantage = Advantage.query.get(field)
    #         value = self.value
    #
    #     if advantage and value and not advantage.is_points_valid(value):
    #         raise ValueError("{} is not a valid value for {}, valid points are {}".format(
    #             value, advantage.name, advantage.valid_points()))
    #
    #     return field
