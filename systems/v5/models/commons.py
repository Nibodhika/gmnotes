from sqlalchemy import CheckConstraint
from app.models.commons import db


def Dots(name, default=1, maximum=5, minimum=0):
    return db.Column(
        db.Integer,
        CheckConstraint('{} >= 0 AND {}<={}'.format(name, name, maximum)),
        nullable=False,
        default=default,
        info={
            'min': minimum,
            'max': maximum
        }
    )
