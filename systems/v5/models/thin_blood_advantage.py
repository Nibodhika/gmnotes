from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Vampire
from .commons import db


class ThinBloodAdvantage(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    is_flaw = db.Column(db.Boolean, nullable=False, default=False)
    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Thin Blood Advantage {}>".format(self.name)

    @classmethod
    def choices(cls):
        return {
            "Merits": [(a.id, a.name) for a in cls.query.filter_by(is_flaw=False).all()],
            "Flaws": [(a.id, a.name) for a in cls.query.filter_by(is_flaw=True).all()]
        }


class VampireThinBloodAdvantage(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    specification = db.Column(db.Text, nullable=True)
    character_id, character = build_fk('v5_vampire.id', Vampire, 'thin_blood_advantages',
                                       cascade="all, delete-orphan")
    thin_blood_advantage_id, thin_blood_advantage = build_fk('v5_thin_blood_advantage.id', ThinBloodAdvantage,
                                                             'characters', cascade="all, delete-orphan")
    # See Issue #17
    # __table_args__ = (
    #     db.UniqueConstraint('character_id', 'thin_blood_advantage_id'),
    # )

    def __repr__(self):
        return "<{}: {} {}>".format(self.character.name, self.thin_blood_advantage.name)
