from systems.v5.system import system
from .commons import db


class PredatorType(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Predator Type {}>".format(self.name)

    @classmethod
    def choices(cls):
        out = [(0, 'No Type')]
        out.extend([(a.id, a.name) for a in cls.query.all()])
        return out
