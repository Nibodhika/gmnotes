from app.models.commons import build_fk
from .character import Human
from systems.v5.system import system

db = system.db


class Roll(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    hunger = db.Column(db.Integer, nullable=False, default=1)

    # Since V5 uses d10 we can represent each die result as char by decreasing 1
    # so a die of 5 is stored as the string "4"
    # that way converting back is as easy as int(i)+1
    # and storing is str(i-1) and we don't need to worry about a separator
    dice_str = db.Column(db.String(20), nullable=False)
    difficulty = db.Column(db.Integer, nullable=False, default=1)
    character_id, character = build_fk('v5_human.id', Human, 'rolls', nullable=True)

    @property
    def dice(self):
        return [int(i) for i in self.dice_str.split(',')]

    @dice.setter
    def dice(self, value):
        self.dice_str = ','.join(str(i) for i in value)

    def get_dice(self):
        hunger_dice = self.dice[:self.hunger]
        regular_dice = self.dice[self.hunger:]
        return self.dice, hunger_dice, regular_dice
