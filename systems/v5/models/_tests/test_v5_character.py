import pytest
import sqlalchemy
from app.models import Character as BaseCharacter, Campaign, Player

from systems.v5.models import Vampire, Clan


@pytest.fixture
def gm_campaign_clan(session):
    gm = Player(name='gm', password='gm')
    session.add(gm)
    campaign = Campaign(name='a campaign', system='generic', gm=gm)
    session.add(campaign)
    clan = Clan(name='a clan')
    session.add(clan)
    session.commit()
    return gm, campaign, clan


def test_default_values(session, gm_campaign_clan):

    _gm, campaign, clan = gm_campaign_clan
    campaign_id = campaign.id

    # Sanity check of the required information on base class
    base_character = BaseCharacter(name='test base', campaign_id=campaign_id)
    session.add(base_character)
    session.commit()

    assert base_character.id == 1

    def create_from_values(values):
        character = Vampire(**values)
        # Add in a nested transaction so if we rollback it only rollsback this add
        session.begin_nested()
        session.add(character)
        return character

    def should_fail(values):
        """
        Checks that trying to commit a character created from values raises an IntegrityError

        :param values:
        :return:
        """
        create_from_values(values)
        with pytest.raises(sqlalchemy.exc.IntegrityError):
            session.commit()
        session.rollback()

    def should_work(values):
        """
        Checks that it's possible to create and commit a set of values

        :param values:
        :return:
        """
        character = create_from_values(values)
        session.commit()
        return character

    # Ensure a character with all the needed information from the base class is failing to be commited
    values = {
        'name': 'test v5',
        'campaign_id': campaign_id
    }

    assert BaseCharacter.query.all() != []
    should_fail(values)
    assert BaseCharacter.query.all() != []
    values['concept'] = 'a concept'
    should_fail(values)
    assert BaseCharacter.query.all() != []
    values['clan_id'] = 1
    character = should_work(values)
    assert BaseCharacter.query.all() != []
    assert character.id == 2, "None of the others should have been commited"
    assert character.generation == 13
    assert not character.is_thin_blood
    # All attributes should be 1
    assert character.strength == 1
    assert character.dexterity == 1
    assert character.stamina == 1
    assert character.charisma == 1
    assert character.manipulation == 1
    assert character.composure == 1
    assert character.intelligence == 1
    assert character.wits == 1
    assert character.resolve == 1
    # No skills, advantages, etc
    assert character.skills == []
    assert character.advantages == []
    assert character.disciplines == []
    assert character.thin_blood_advantages == []
    assert character.powers == []


def test_thin_blood(gm_campaign_clan):
    _gm, campaign, clan = gm_campaign_clan

    c1 = Vampire(name='kindred', campaign=campaign, generation=13, concept='a', clan=clan)
    assert not c1.is_thin_blood

    c2 = Vampire(name='thin', campaign=campaign, generation=14, concept='a', clan=clan)
    assert c2.is_thin_blood
