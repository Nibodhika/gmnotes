from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Vampire
from .commons import db, Dots


class Discipline(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Discipline {}>".format(self.name)

    @classmethod
    def choices(cls):
        return [(d.id, d.name) for d in cls.query.all()]


class VampireDiscipline(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = Dots('value', default=1)
    character_id, character = build_fk('v5_vampire.id', Vampire, 'disciplines', cascade="all, delete-orphan")
    discipline_id, discipline = build_fk('v5_discipline.id', Discipline, 'characters', cascade="all, delete-orphan")

    # See Issue #17
    # __table_args__ = (
    #     db.UniqueConstraint('character_id', 'discipline_id'),
    # )

    def __repr__(self):
        return "<{}: {} {}>".format(self.character.name, self.discipline.name, self.value)
