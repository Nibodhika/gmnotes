from systems.v5.system import system
from .commons import db


class Clan(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    bane = db.Column(db.Text, nullable=True)
    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Clan {}>".format(self.name)

    @classmethod
    def choices(cls):
        return [(c.id, c.name) for c in cls.query.all()]

    @classmethod
    def thin_blood_clan(cls):
        """
        We assume the Thin Blood clan has an id 100
        this is so that in the future we can find it even if it's named differently, e.g. translations.

        If using the default settings this id was set by initial_data.yaml
        :return:
        """
        return cls.query.get(100)
