from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Vampire

from .commons import db


class Belief(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    character_id, character = build_fk('v5_vampire.id', Vampire, 'believes', cascade="all, delete-orphan")

    conviction = db.Column(db.String(250), nullable=False)
    touchstone = db.Column(db.String(250), nullable=False)

    specification = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<{} believes {}>".format(self.character.name, self.conviction)
