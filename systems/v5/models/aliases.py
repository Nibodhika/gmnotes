import enum

from systems.v5.system import system

from .commons import db


class Alias(system.Model):
    alias = db.Column(db.String(80), primary_key=True)
    references = db.Column(db.String(80), nullable=False)

    @classmethod
    def unalias(cls, alias):
        """
        Given an alias gets the referenced name, if none is find simply returns the alias

        :param alias:
        :return:
        """
        out = cls.query.get(alias)
        if out is not None:
            return out.references

        # Not a known alias
        return alias

    @classmethod
    def aliases(cls, name):
        return [a.alias for a in cls.query.filter_by(references=name).all()]

    @classmethod
    def get_referenced_value_and_name(cls, alias, character):
        """
        Given an alias and a character returns the value
        and the name of the actual attribute associated with the character.
        If the alias doesn't match to anything raises a ValueError

        :param alias:
        :param character:
        :return:
        """
        from systems.v5.models import Skill, Advantage, Discipline, Power, ThinBloodAdvantage

        name = cls.unalias(alias)
        if hasattr(character, name):
            return getattr(character, name), name

        getters = [
            (Skill, character.skill),
            (Advantage, character.advantage),
            (Discipline, character.discipline),
            (Power, character.power),
            (ThinBloodAdvantage, character.thin_blood_advantage),
        ]
        for table, getter in getters:
            elem = table.query.filter(table.name.ilike(name)).first()
            if elem is None:
                continue
            return getter(elem), name

        raise ValueError('ERROR in reference "{}" is not known'.format(name))

    @classmethod
    def get_referenced_value(cls, alias, character):
        """
        Same as get_referenced_value_and_name but discards the name

        :param alias:
        :param character:
        :return:
        """
        return cls.get_referenced_value_and_name(alias, character)[0]

    @classmethod
    def set_referenced_value(cls, alias, character, value):
        """
        Given an alias, a character and a value sets the associated element with the character.
        If the alias doesn't match to anything raises a ValueError

        :param alias:
        :param character:
        :param value:
        :return: str: The name of the actual attribute set
        """
        from systems.v5.models import Skill, Advantage, Discipline, Power, ThinBloodAdvantage

        name = cls.unalias(alias)
        has_set = False
        if hasattr(character, name):
            setattr(character, name, value)
            has_set = True
        else:
            setters = [
                (Skill, character.set_skill),
                (Advantage, character.set_advantage),
                (Discipline, character.set_discipline),
                (Power, character.set_power),
                (ThinBloodAdvantage, character.set_thin_blood_advantage),
            ]
            for table, setter in setters:
                elem = table.query.filter(table.name.ilike(name)).first()
                if elem is None:
                    continue
                setter(elem, value)
                has_set = True
                break
        
        if has_set:
            db.session.add(character)
            db.session.commit()
            return name
        else:
            raise ValueError('ERROR in reference "{}" is not known'.format(name))

