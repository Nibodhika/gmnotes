from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Vampire, Discipline
from .commons import db, Dots


class Power(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Power {}>".format(self.name)

    @classmethod
    def choices(cls):

        def choice_elem(power, req_level):
            name = "{} ({})".format(power.name, req_level)
            return power.id, name

        out = {}
        for power in cls.query.all():
            main_req = power.requirements[0]
            req_name = main_req.discipline.name
            if req_name not in out:
                out[req_name] = []
            out[req_name].append(choice_elem(power, main_req.value))

        return out


class PowerRequirement(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = Dots('value')
    discipline_id, discipline = build_fk('v5_discipline.id', Discipline, 'powers')
    power_id, power = build_fk('v5_power.id', Power, 'requirements')

    __table_args__ = (
        db.UniqueConstraint('discipline_id', 'power_id'),
    )

    def __repr__(self):
        return "<{}: requires {} {}>".format(self.power.name, self.value, self.discipline.name)


class PowerPrerequisite(system.Model):
    id = db.Column(db.Integer, primary_key=True)

    power_id, power = build_fk('v5_power.id', Power, 'prerequisites')
    prerequisite_id, prerequisite = build_fk('v5_power.id', Power, 'expansions')

    __table_args__ = (
        db.UniqueConstraint('power_id', 'power_id'),
    )

    def __repr__(self):
        return "<{}: requires {}>".format(self.power.name, self.prerequisite.name)


class VampirePower(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    character_id, character = build_fk('v5_vampire.id', Vampire, 'powers', cascade="all, delete-orphan")
    power_id, power = build_fk('v5_power.id', Power, 'characters', cascade="all, delete-orphan")

    # See Issue #17
    # __table_args__ = (
    #     db.UniqueConstraint('character_id', 'power_id'),
    # )

    def __repr__(self):
        return "<{}: {}>".format(self.character.name, self.power.name)
