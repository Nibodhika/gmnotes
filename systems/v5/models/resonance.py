from systems.v5.system import system
from .commons import db


class Resonance(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Resonance {}>".format(self.name)

    @classmethod
    def choices(cls):
        out = [(0, "No resonance")]
        out.extend([(r.id, r.name) for r in cls.query.all()])
        return out
