import enum

from app.models.commons import build_fk
from systems.v5.system import system
from systems.v5.models import Human
from .commons import db, Dots


class SkillType(enum.Enum):
    Physical = 1
    Social = 2
    Mental = 3


class Skill(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    free_specialization = db.Column(db.Boolean, nullable=False, default=False)
    skill_type = db.Column(db.Enum(SkillType), nullable=False)
    # Descriptions
    short_description = db.Column(db.String(200), nullable=True)
    description = db.Column(db.Text, nullable=True)

    def __repr__(self):
        return "<Skill {}>".format(self.name)


class HumanSkill(system.Model):
    id = db.Column(db.Integer, primary_key=True)
    value = Dots('value', default=0, minimum=0)
    character_id, character = build_fk('v5_human.id', Human, 'skills')
    skill_id, skill = build_fk('v5_skill.id', Skill, 'characters')
    # comma separated list of specializations
    specializations_str = db.Column(db.String(250), nullable=True)

    # See Issue #17
    # __table_args__ = (
    #     db.UniqueConstraint('character_id', 'skill_id'),
    # )

    def __repr__(self):
        return "<{}: {} {}>".format(self.character.name, self.skill.name, self.value)

    @property
    def specializations(self):
        if not self.specializations_str:
            return []
        return self.specializations_str.split(',')

    @specializations.setter
    def specializations(self, value):
        self.specializations_str = ','.join(value)
