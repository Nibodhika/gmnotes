from copy import copy
from wtforms import SelectField, FieldList, RadioField
from wtforms.validators import Optional, ValidationError
from wtforms.widgets import HTMLString, html_params
from wtforms.widgets.core import escape_html
from wtforms_alchemy import ModelFormField
from app.views.model_forms import ModelForm
from systems.v5.models import *


def DotsField(amount=5, default=0):
    return RadioField(choices=[(i + 1, i+1) for i in range(amount)], default=default, coerce=int,  validators=[Optional()])


class GroupSelect(object):
    """
    Widget to be used with GroupsSelectField that draws optgroup for the keys of the dictionary and regular options
    for the other options.
    """
    def __init__(self, multiple=False):
        self.multiple = multiple

    def __call__(self, field, **kwargs):
        kwargs.setdefault('id', field.id)
        if self.multiple:
            kwargs['multiple'] = True
        if 'required' not in kwargs and 'required' in getattr(field, 'flags', []):
            kwargs['required'] = True
        html = ['<select %s>' % html_params(name=field.name, **kwargs)]
        for val, label, selected, is_group in field.iter_choices():
            if is_group and label is not None:
                html.append(self.render_optgroup(label))
            elif is_group:
                html.append(self.render_optgroup_close())
            else:
                html.append(self.render_option(val, label, selected))
        html.append('</select>')
        return HTMLString(''.join(html))

    @classmethod
    def render_option(cls, value, label, selected, **kwargs):
        if value is True:
            # Handle the special case of a 'True' value.
            value = str(value)

        options = dict(kwargs, value=value)
        if selected:
            options['selected'] = True
        return HTMLString('<option %s>%s</option>' % (html_params(**options), escape_html(label, quote=False)))

    @classmethod
    def render_optgroup(cls, label):
        return HTMLString('<optgroup label="%s">' % (escape_html(label, quote=False)))

    @classmethod
    def render_optgroup_close(cls):
        return HTMLString('</optgroup>')


class GroupsSelectField(SelectField):
    """
    Extension of SelectField that allows to create optgroup by setting a dictionary to choices.
    """
    widget = GroupSelect()

    def __init__(self, label=None, validators=None, coerce=str, choices=None, **kwargs):
        super(SelectField, self).__init__(label, validators, **kwargs)
        self.coerce = coerce
        self.choices = copy(choices)

    def iter_choices(self):
        for group, items in self.choices.items():
            yield (None, group, None, True)
            for value, label in items:
                yield (value, label, self.coerce(value) == self.data, False)
            yield (None, None, None, True)

    def pre_validate(self, form):
        for group, items in self.choices.items():
            for v, _ in items:
                if self.data == v:
                    return
        else:
            raise ValueError('Not a valid choice {}'.format(self.data))


class HumanSkillForm(ModelForm):
    class Meta:
        model = HumanSkill
        csrf = False  # Since there are many of this form in the page, and the main form is already validating csrf
        # validating again here is convoluted and unnecessary

    skill_id = SelectField('Skill', coerce=int)
    value = DotsField()


class HumanAdvantageForm(ModelForm):
    class Meta:
        model = HumanAdvantage
        csrf = False  # Same as above

    advantage_id = GroupsSelectField('Advantage', coerce=int)
    value = DotsField(amount=6)

    def validate_value(self, field):
        advantage = Advantage.query.get(self.advantage_id.data)
        if not advantage.is_points_valid(field.data):
            raise ValidationError('{} is not a valid value for {}, valid values are {}'.format(
                field.data, advantage.name, advantage.valid_points()))


class VampireThinBloodAdvantageForm(ModelForm):
    class Meta:
        model = VampireThinBloodAdvantage
        csrf = False  # Same as above

    thin_blood_advantage_id = GroupsSelectField('Thin Blood Advantage', coerce=int)


class VampireDisciplineForm(ModelForm):
    class Meta:
        model = VampireDiscipline
        csrf = False  # Same as above

    discipline_id = SelectField('Discipline', coerce=int)
    value = DotsField()


class VampirePowerForm(ModelForm):
    class Meta:
        model = VampirePower
        csrf = False  # Same as above

    power_id = GroupsSelectField('Power', coerce=int)


class BeliefForm(ModelForm):
    class Meta:
        model = Belief
        csrf = False  # Same as above


class VampireForm(ModelForm):
    class Meta:
        model = Vampire

    player_id = SelectField('Player', coerce=int)
    clan_id = SelectField('Clan', coerce=int)
    predator_type_id = SelectField('Predator', coerce=int)
    resonance_id = SelectField('Resonance', coerce=int)

    strength = DotsField(default=1)
    dexterity = DotsField(default=1)
    stamina = DotsField(default=1)

    charisma = DotsField(default=1)
    manipulation = DotsField(default=1)
    composure = DotsField(default=1)

    intelligence = DotsField(default=1)
    wits = DotsField(default=1)
    resolve = DotsField(default=1)

    skills = FieldList(ModelFormField(HumanSkillForm))

    blood_potency = DotsField(amount=10)

    believes = FieldList(ModelFormField(BeliefForm))
    advantages = FieldList(ModelFormField(HumanAdvantageForm))
    thin_blood_advantages = FieldList(ModelFormField(VampireThinBloodAdvantageForm))
    disciplines = FieldList(ModelFormField(VampireDisciplineForm))
    powers = FieldList(ModelFormField(VampirePowerForm))

    def validate_generation(self, field):
        """
        Checks that the generation matches the clan for thin blood vampires.
        We validate generation instead of clan_id, simply because the field name is more user friendly.

        :param field:
        :return:
        """
        clan = Clan.query.get(self.clan_id.data)
        if not Vampire.check_clan_generation(clan, field.data):
            raise ValidationError('Generation should be >=14 for Thin Blood vampires and <=13 for others')

    def validate_thin_blood_advantages(self, field):
        """
        Only characters of generation >= 14 can have Thin Blood Advantages.

        :param field:
        :return:
        """
        if self.generation.data <= 13:
            if len(field) > 0:
                raise ValidationError('Only Thin Blood can have Thin Blood advantages')

    def validate_powers(self, field):
        """
        Checks that the powers have their requirements.

        :param field:
        :return:
        """
        discipline_levels = {f.discipline_id.data: f.value.data for f in self.disciplines}

        def get_value(discipline_id):
            if discipline_id not in discipline_levels:
                return 0
            return discipline_levels[discipline_id]

        for f in field:
            power = Power.query.get(f.power_id.data)
            for req in power.requirements:
                if get_value(req.discipline_id) < req.value:
                    raise ValidationError('Power {} requires level {} of Discipline {}'.format(
                        power.name, req.value, req.discipline.name
                    ))

    def validate_health(self, field):
        if field.data != (self.stamina.data + 3):
            raise ValidationError('Health should be stamina+3')

    def validate_willpower(self, field):
        if field.data != (self.composure.data + self.resolve.data):
            raise ValidationError('Willpower should be composure+resolve')
