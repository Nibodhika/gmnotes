import discord
from discord.ext import commands
from typing import Optional

from .commons import Who, humanity_bar


@commands.command(
    brief="Shows or alters a character Humanity bar",
    usage="[who(GM)] [option(GM)] [amount(GM)=1]",
    help="""who: Discord mention
option: stains (s), remorse (re), rationalize (ra)
amount: The amount of stains for the option stains

If no parameter is passed prints the character's Humanity bar

usage examples:

!humanity -> Shows your own character humanity
!humanity @player -> Shows the humanity of @player (GM only)
!humanity @player s -> Causes one stain to the character (GM only)
!humanity @player s 2 -> Causes two stains to the character (GM only)
!humanity re -> Does a remorse check for your character
!humanity ra -> Clears all stains at the cost of 1 humanity
"""
)
async def humanity(ctx,
                   who: Optional[Who] = None,
                   option=None,
                   amount=1):
    from app.models import db
    from .roll import RollResult

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character

    from discord.embeds import EmptyEmbed
    color = EmptyEmbed
    msg_list = []
    if option in ['s', 'stains']:

        if not who.is_gm:
            await ctx.send("Only GMs can do change values")
            return

        character.stains += amount
        free_humanity = character.free_humanity
        if free_humanity < 0:
            # Every extra box becomes a point of aggravated willpower damage
            damage = (free_humanity * -1)
            character.w_aggravated += damage
            # Reset the stains to the maximum allowed
            character.stains = 10 - character.humanity
            msg_list.append(("Degeneration", "You have suffered degeneration and taken {} point(s) "
                                             "of aggravated willpower damage "
                                             "and is now Impaired (page 239)".format(damage)))
            from .bar_cmds import build_bar
            w_bar = build_bar(character.willpower, character.w_aggravated, character.w_superficial)
            msg_list.append(("Willpower {}/{}/{}".format(
                character.willpower, character.w_aggravated, character.w_superficial),
                             w_bar))

        else:
            msg_list.append(("Stains", "You now have {} stains".format(character.stains)))

        try:
            db.session.add(character)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
            await ctx.send("ERROR setting stains: {}".format(str(e)))
            return

    elif option in ['re', 'remorse']:
        if character.stains == 0:
            msg_list.append(("Remorse", "No Remorse check needed"))
        else:
            dice = max(character.free_humanity, 1)
            roll = RollResult(dice)
            character.stains = 0
            if roll.is_success:
                color = 0x00ff00
                msg = "Success, Humanity stays the same"
            else:
                character.humanity -= 1
                color = 0xff0000
                msg = "Failed, Humanity decreased"

            msg_list.append(("Remorse", msg))
            msg_list.append(("Dice", roll.dice_str))
            try:
                db.session.add(character)
                db.session.commit()
            except Exception as e:
                from colors import cprint
                cprint(e, color='red')
                await ctx.send("ERROR saving character: {}".format(str(e)))
                return
    elif option in ['ra', 'rationalize']:
        if character.stains == (10 - character.humanity):
            character.humanity -= 1
            character.stains = 0
            msg_list.append(("Rationalize", "You rationalize the monster you've become, your humanity decreases "
                                            "but you are no longer impaired"))

            try:
                db.session.add(character)
                db.session.commit()
            except Exception as e:
                from colors import cprint
                cprint(e, color='red')
                await ctx.send("ERROR saving character: {}".format(str(e)))
                return
        else:
            msg_list.append(("Rationalize", "No need to rationalize just yet, "
                                            "you can do a remorse check at the end of the session"))
    else:
        await ctx.send('Unknown option "{}"'.format(str(option)))
        return

    bar = humanity_bar(character.humanity, character.stains)
    embed = discord.Embed(title="Humanity {}/{}/{}".format(
        character.humanity, character.free_humanity, character.stains),
        description='{}\n{}'.format(who.affected.mention, bar), color=color)
    for n, v in msg_list:
        embed.add_field(name=n, value=v, inline=False)

    await ctx.send(embed=embed)
