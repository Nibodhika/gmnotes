import discord
from discord.ext import commands
from typing import Optional

from .commons import Who


@commands.command(
    brief="Does a Frenzy test",
    usage="[who(GM)] [difficulty]",
    help="""who: Discord mention
difficulty: Target difficulty

Rolls a pool composed of:
- Available willpower (i.e. minus damage)
- One third of the humanity (rounded down)

And interprets the results by showing whether the character succumbs to frenzy.

usage examples:

!frenzy -> Performs a frenzy test for your character
!frenzy @player -> Performs a frenzy test for the character of @player (GM only)
"""
)
async def frenzy(ctx,
                 who: Optional[Who] = None,
                 difficulty=1,
                 ):

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character

    third_humanity = character.humanity // 3
    amount = character.current_willpower + third_humanity

    from systems.v5.commands.roll import RollResult
    # Willpower tests don't use hunger dice
    roll = RollResult(amount, difficulty, 0)

    if roll.is_critical:
        title = "You are able to control yourself instantly"
        color = 0x00ff00
    elif roll.is_success:
        title = "You are able to control yourself, although it takes a turn to do so"
        color = 0x00841f
    else:
        title = "You succumb to frenzy"
        color = 0xff0000

    embed = discord.Embed(title=title, description=who.affected.mention, color=color)
    embed.add_field(name="Dice", value=roll.dice_str)
    await ctx.send(embed=embed)
