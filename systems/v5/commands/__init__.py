from .roll import roll
from .reroll import reroll
from .hunger import hunger
from .bar_cmds import health, willpower
from .character import character
from .status import status
from .rouse import rouse
from .belief import belief
from .humanity import humanity
from .frenzy import frenzy
# Importing the entire module so we can access it on system init to set the rules_file
from . import rule

cmd_list = [
    roll,
    reroll,
    hunger,
    health,
    willpower,
    character,
    status,
    rouse,
    belief,
    humanity,
    frenzy,
    rule.rule,
]
