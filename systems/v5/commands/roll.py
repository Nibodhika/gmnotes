import discord
from discord.ext import commands
from random import randint
from typing import Optional

from .commons import Who


class RollResult(object):

    def __init__(self, dice, difficulty=1, hunger=0):
        self.difficulty = difficulty
        self.hunger = hunger
        # Roll the dice
        if type(dice) == int:
            self.dice = [randint(1, 10) for _ in range(dice)]
        else:
            self.dice = dice
        # Count successes
        self.successes = sum(i > 5 for i in self.dice)
        # Each pair of 10 is a critical
        crits = self.dice.count(10) // 2
        # Each critical counts as 4 successes, the 2 from the dice plus an additional 2
        self.successes += crits * 2
        # A critical success is a success that contains at least a critical
        self.is_critical = self.is_success and crits > 0

    def _build_dice_str(self, l):
        return ', '.join(str(i) for i in l)

    @property
    def dice_str(self):
        return self._build_dice_str(self.dice)

    @property
    def is_success(self):
        return self.successes >= self.difficulty

    @property
    def margin(self):
        return abs(self.difficulty - self.successes)

    @property
    def hunger_dice(self):
        return self.dice[:self.hunger]

    @property
    def hunger_dice_str(self):
        return self._build_dice_str(self.hunger_dice)

    @property
    def regular_dice(self):
        return self.dice[self.hunger:]

    @property
    def regular_dice_str(self):
        return self._build_dice_str(self.regular_dice)

    @property
    def is_messy(self):
        # Messy Critical is when it's a critical and at least a 10 was rolled in the hunger dice
        return self.is_critical and self.hunger_dice.count(10) > 0

    @property
    def is_bestial(self):
        # A bestial failure is a failure that has at least a 1 in the hunger dice
        return not self.is_success and self.hunger_dice.count(1) > 0

    def embed(self):
        # Depending on the success type change the color, title and description of the embed
        if self.is_messy:
            color = 0xff9200
            title = 'Messy Critical!'
            description = "Not you, but the beast succeeds"
        elif self.is_critical:
            color = 0x00ff00
            title = 'Critical Success!'
            description = "You succeed with outstanding results"
        elif self.is_success:
            color = 0x00841f
            title = 'Success'
            description = "You succeed"
        elif self.is_bestial:
            color = 0xff0000
            title = 'Bestial Failure!'
            description = 'You fail, and either the Beast gets angry at you, or it manifested and made you fail'
        else:
            color = 0x000000
            title = 'Failure'
            description = 'You fail'

        description = "{}d10 vs {} with {} Hunger\n{}".format(len(self.dice), self.difficulty, self.hunger, description)
        embed = discord.Embed(title=title, description=description, color=color)

        embed.add_field(name="Margin", value="Successes: {}\nMargin: {}".format(self.successes, self.margin),
                        inline=False)

        if self.hunger > 0:
            embed.add_field(name="Hunger Dice", value=self.hunger_dice_str, inline=False)
        if self.regular_dice_str:
            embed.add_field(name="Regular Dice", value=self.regular_dice_str, inline=False)

        return embed

    def save(self, character):
        # Store the result in the database
        from systems.v5.models import Roll
        r = Roll(hunger=self.hunger, difficulty=self.difficulty, character=character)
        r.dice = self.dice
        from app.models import db
        try:
            db.session.add(r)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
            db.session.rollback()
            return e
        return None


@commands.command(
    brief="Rolls dice using V5 rules",
    usage="[who(GM)] [amount=1] [difficulty=1] [hunger(GM)]",
    help="""who: Discord mention
amount: The amount of dice to roll, can be a string using alias (see below)
difficulty: The target difficulty
hunger: Only the GM can roll dice setting hunger, for everyone else it uses your character's current hunger

Amount:
Amount can either be a number (1)
The name of an attribute/skill/etc (strength)
An alias to one of those names (str)
Or a list concatenated by + (str+athletics+2)

Then performs the roll again and shows the result

usage examples:

!roll -> Rolls a single dice
!roll @player str -> Rolls an amount of dice equal to the character strength (GM only)
!roll str+ath 3 -> Rolls strength+athletic dice against a difficulty 3
!roll 6 2 2 -> Rolls 6 dice against a difficulty 2 with 2 hunger dice (GM only)
"""
)
async def roll(ctx,
               who: Optional[Who] = None,
               amount='1',
               difficulty=1,
               hunger=0):
    from systems.v5.models import Alias

    if who is None:
        who = Who(ctx)

    rolls = amount.split('+')

    # If there is an affected character then use it instead of whatever hunger was passed
    if who.affected_character is not None:
        hunger = who.affected_character.hunger

    total_amount = 0
    for r in rolls:
        try:
            v = int(r)
        except ValueError:
            v = None
        if v is None:
            try:
                v = Alias.get_referenced_value(r, who.affected_character)
            except ValueError:
                # Not a valid int or name
                await ctx.send('"{}" is not a valid name for roll'.format(r))
                return
        total_amount += v

    result = RollResult(total_amount, difficulty, hunger)

    await ctx.send(embed=result.embed())

    e = result.save(who.affected_character)
    if e is not None:
        await ctx.send("ERROR saving to database: {}".format(str(e)))
