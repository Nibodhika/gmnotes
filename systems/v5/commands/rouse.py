import discord
from discord.ext import commands
from random import randint
from typing import Optional

from .commons import Who


@commands.command(
    brief="Does a Rouse check",
    usage="[who(GM)] [amount=1]",
    help="""who: Discord mention
amount: The amount of dice (usually just one)

Rolls the amount of dice for the rouse check
If it fails increases the character hungry and gives a message accordingly.

usage examples:

!rouse -> Performs a rouse for your character
!rouse 2 -> Performs a rouse for your character using 2 dice
!rouse @player 2 -> Performs a rouse check for the character of @player using 2 dice (GM only)
"""
)
async def rouse(ctx,
                 who: Optional[Who] = None,
                 amount=1
                 ):
    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character

    # Rouse cannot be rerolled so no need to store them in the database
    dice = [randint(1, 10) for _ in range(amount)]
    # Count successes
    successes = sum(i > 5 for i in dice)

    if successes > 0:
        # Success nothing changes
        embed = discord.Embed(title='Success',
                              description='Your hunger stays at {}'.format(character.hunger),
                              color=0x00841f)
    elif character.hunger >= 5:
        # Failure but hunger already at 5
        if character.is_thin_blood:
            description = "You would enter a hunger frenzy, if your blood weren't so thin"
        else:
            description = "Make a Hunger Frenzy test with difficulty 4 or ride the wave"

        embed = discord.Embed(title='Failure', description=description, color=0xff0000)
    else:
        # Failure, increase hunger
        character.hunger += 1
        try:
            from app.models import db
            db.session.add(character)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
            await ctx.send("ERROR saving character: {}".format(e))

        embed = discord.Embed(title='Failure',
                              description='Your hunger increases to {}'.format(character.hunger),
                              color=0)

    await ctx.send(embed=embed)
