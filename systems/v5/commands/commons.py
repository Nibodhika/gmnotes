from discord.ext import commands


class Who:
    def __init__(self, ctx, who=None):
        # Just storing now, but it might be necessary to reload from database in case of changes during run
        self.campaign = ctx.bot.campaign
        self.author = ctx.author
        self.author_player = self._get_player(ctx.author.id)
        self.author_character = self._get_character(self.author_player)
        # Only the GM can affect anyone else, for all others revert to the sender
        if self.is_gm:
            self.affected = who or ctx.author
        else:
            self.affected = ctx.author

        self.affected_player = self._get_player(self.affected.id)
        self.affected_character = self._get_character(self.affected_player)

    @classmethod
    async def convert(cls, ctx, argument):
        member = await commands.MemberConverter().convert(ctx, argument)
        return cls(ctx, member)

    def _get_player(self, discord_id):
        from app.models import Player
        return Player.query.filter_by(discord_id=discord_id).first()

    def _get_character(self, player):
        from app.models import Character
        if player is None:
            return None
        return Character.query.filter_by(player=player, campaign=self.campaign).first()

    @property
    def is_gm(self):
        return self.campaign.gm == self.author_player


def humanity_bar(total, stains):
    stain_boxes = 10 - stains
    out = ''
    for i in range(10):
        if i < total:
            out += '[#]'
        elif i < stain_boxes:
            out += '[ ]'
        else:
            out += '[/]'
    return out


def embed_title(title, total_len=60, character='='):
    amount = (total_len - (len(title) + 2)) // 2
    part = character * amount
    return '{} {} {}'.format(part, title, part)


def embed_divider(embed, title, total_len=60, character='='):
    divider = embed_title(title, total_len, character)
    embed.add_field(name='\u200b', value=divider, inline=False)
