from discord.ext import commands
from typing import Optional

from .commons import Who


@commands.command(
    brief="Shows or alters a character Hunger",
    usage="[who(GM)] [option(GM)] [amount(GM)=1]",
    help="""who: Discord mention
option: increase (i, inc), decrease (d, dec), set (s)
amount: The amount to increase/decrease or set

If no parameter is passed prints the character's Hunger

usage examples:

!hunger -> Shows your own character hunger
!hunger @player -> Shows the hunger of @player (GM only)
!hunger @player i -> Increases the hunger of the character (GM only)
!hunger @player d 2 -> Decreases the hunger of the character by 2 (GM only)
!hunger @player s 3 -> Sets the hunger to 3 (GM only)
"""
)
async def hunger(ctx,
                 who: Optional[Who] = None,
                 option=None,
                 amount=1,
                 ):
    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    hunger = who.affected_character.hunger

    # Only the GM can change the hunger
    if not who.is_gm:
        option = None

    if option is None:
        await ctx.send("{} is at {} Hunger".format(who.affected.mention, hunger))
        return
    elif option in ['i', 'inc', 'increase']:
        # Increase
        hunger += amount
    elif option in ['d', 'dec', 'decrease']:
        # Decrease
        hunger -= amount
    elif option in ['s', 'set']:
        hunger = amount
    else:
        await ctx.send('Uknown option "{}"'.format(option))
        return

    pre_msg = ''
    if hunger > 5:
        pre_msg = "This would have raised Hunger to more than 5\n"
        hunger = 5
    elif hunger < 0:
        pre_msg = "Cannot set hunger to less than 0\n"
        hunger = 0

    await ctx.send("{}{} is now at {} Hunger".format(pre_msg, who.affected.mention, hunger))

    who.affected_character.hunger = hunger

    from app.models import db
    try:
        db.session.add(who.affected_character)
        db.session.commit()
    except Exception as e:
        from colors import cprint
        cprint(e, color='red')
        db.session.rollback()
        await ctx.send("ERROR saving to database: {}".format(str(e)))
