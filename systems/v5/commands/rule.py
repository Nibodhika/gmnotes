import os

import discord
import yaml
from discord.ext import commands

from colors import cprint

rules_file = None

topics = {
        'Attributes': ['a', 'attr', 'attribute', 'attributes'],
        'Skills': ['s', 'skill', 'skills'],
        'Disciplines': ['d', 'disc', 'discipline', 'disciplines'],
        'Advantages': ['adv', 'advantage', 'advantages'],
        'Thin Blood Advantages': ['thin', 'tba', 'thin_blood', 'thin blood', 'thin blood advantage'],
        'Powers': ['p', 'pow', 'power', 'powers'],
}


def validate_rules():

    # The file might not exist either on test server or before the admin creates it
    if not os.path.exists(rules_file):
        cprint('WARNING: rule.yaml does not exist, cannot validate', color='yellow')
        return

    with open(rules_file, 'r') as stream:
        data = yaml.safe_load(stream)
    for topic, elems in data.items():
        if topic not in topics:
            cprint('WARNING: rule.yaml "{}" is not a valid topic'.format(topic), color='yellow')
        for e in elems:
            name = e
            if 'name' in e:
                name = e.pop('name')
            else:
                cprint('WARNING: rule.yaml {} "{}" does not have name'.
                       format(topic, e), color='yellow')
            if 'description' in e:
                desc = e.pop('description')
                if len(desc) > 2000:
                    cprint('WARNING: rule.yaml {} "{}" has a description longer than 2000 characters ({})'.
                           format(topic, name, len(desc)), color='yellow')
            else:
                cprint('WARNING: rule.yaml {} "{}" does not have description'.
                       format(topic, name), color='yellow')

            for k, v in e.items():
                if type(v) != str:
                    v = str(v)
                v = v.strip()
                if len(v) > 1024:
                    cprint('WARNING: rule.yaml {} "{}" has a field "{}" with more than 1024 characters ({})'.
                           format(topic, name, k, len(v)), color='yellow')


def get_rule(topic=None, specification=None):
    with open(rules_file, 'r') as stream:
        data = yaml.safe_load(stream)
    if topic is not None:
        if topic not in data:
            return {}
        data = data[topic]
        if specification is not None:
            for d in data:
                if d['name'].lower() == specification.lower():
                    return d
            return {}
    return data


@commands.command(
    brief="Prints information about the rules",
    usage="[topic] [specification]",
    help="""topic: Topic of the rule
specification: Specification inside topic (can be an Alias)

This command depends on the rules.yaml file
If the command does not seem to be working contact the administrator of the server that is running the bot.

To get a list of available topics call the command without arguments.

usage examples:

!rule -> Lists all known topics
!rule a -> List all known Attributes
!rule d pro -> List the rules for the Discipline Protean
"""
)
async def rule(ctx, topic=None, specification=None):
    from systems.v5.models import Alias

    if not os.path.exists(rules_file):
        await ctx.send("You need to create the rules file {}".format(rules_file))
        return

    for k, v in topics.items():
        if topic in v:
            topic = k
            break

    # Unalias specification if it was passed
    if specification is not None:
        specification = Alias.unalias(specification)

    if topic is not None:
        rule_data = get_rule(topic, specification)
        if not rule_data:
            embed = discord.Embed(title="Error", description="No information for requested topic")
        else:
            name = specification.capitalize() if specification is not None else topic
            if type(rule_data) is list:
                msg = {}
                for r in rule_data:
                    if 'Type' in r:
                        t = r['Type']
                    elif 'Discipline' in r:
                        t = r['Discipline']
                    else:
                        t = None
                    if t not in msg:
                        msg[t] = []
                    r_name = r['name']
                    aliases = ', '.join(Alias.aliases(r_name))
                    if 'points' in r:
                        r_name += ' ({})'.format(r['points'])
                    if aliases:
                        r_name += ' ({})'.format(aliases)
                    msg[t].append(r_name)
                # If all have types create an empty msg
                if None not in msg:
                    msg[None] = ['\u200b']

                description = "\n".join([i for i in msg.pop(None)])
                from .commons import embed_title
                embed = discord.Embed(title=embed_title(name), description=description)
                for k, v in msg.items():
                    embed.add_field(name=k, value='\n'.join(v))
            else:
                n = rule_data.pop('name')
                desc = rule_data.pop('description').strip()
                if len(desc) > 2000:
                    cprint("Warning clipping description for {} ({} is over the 2000 characters limits for an embed)".
                           format(n, len(desc)), color='yellow')
                    txt = "... (Too long)"
                    desc = desc[:2000 - len(txt)] + txt
                embed = discord.Embed(title=n.capitalize(), description=desc)
                for k, v in rule_data.items():
                    if type(v) is list:
                        v = ', '.join(v)
                    v = str(v).strip()
                    if len(v) > 1024:
                        cprint(
                            "Warning clipping {} for {} ({} is over the 1024 characters limits for a field)".
                            format(k, n, len(v)), color='yellow')
                        txt = "... (Too long)"
                        v = v[:1024 - len(txt)] + txt
                    embed.add_field(name=k.capitalize(), value=v)

    else:
        t = []
        for k, v in topics.items():
            aliases = ', '.join(v)
            t.append('{} ({})'.format(k, aliases))

        embed = discord.Embed(title="Topics", description="\n".join(t))

    await ctx.send(embed=embed)
