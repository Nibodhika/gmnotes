import discord
from discord.ext import commands
from typing import Optional

from .commons import Who


@commands.command(
    brief="Prints or alters information about a character's belief",
    usage="[who(GM)] [option(GM)] [conviction(GM)] [touchstone(GM)] [specification(GM)]",
    help="""who: Discord mention
option: None, add (a) or remove (r)
conviction: Conviction text
touchstone: Touchstone text
specification: Specification text

If no parameter is passed prints the character's convictions and touchstones
GMs can use "who" to see/alter other player's characters beliefs
GMs can use "option" to add/remove convictions and touchstones
GMs can use "conviction", required both to add and remove
GMs can use "touchstone", required for add
GMs can use "specification" optional to add

usage examples:

!belief -> Shows your own character beliefs
!belief @player -> Shows the beliefs of @player (GM only)
!belief @player a "Leave no man behind" "James" "ex-military" -> 
Adds a new conviction to the player of @player (GM only)
!belief @player r "Leave no man behind"  -> Removes the conviction added before (GM only)
"""
)
async def belief(ctx,
                 who: Optional[Who] = None,
                 option=None,
                 conviction=None,
                 touchstone=None,
                 specification=None):
    from systems.v5.models import Belief
    from app.models import db

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character

    if option is None:
        embed = discord.Embed(title="Convictions and Touchstones", description=who.affected.mention)
        for b in character.believes:
            touchstone = b.touchstone
            if b.specification:
                touchstone += "\n{}".format(b.specification)
            embed.add_field(name=b.conviction, value=touchstone, inline=False)
        await ctx.send(embed=embed)
        return

    if not who.is_gm:
        await ctx.send("Only GMs can do change values")
        return

    if option in ['a', 'add']:
        if conviction is None or touchstone is None:
            await ctx.send("Must specify a conviction and a touchstone to add")
            return
        b = Belief(character=character, conviction=conviction, touchstone=touchstone, specification=specification)
        try:
            db.session.add(b)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
            await ctx.send("ERROR adding belief: {}".format(str(e)))
        else:
            await ctx.send("{} now has conviction {}".format(who.affected.mention, b.conviction))

    elif option in ['r', 'remove']:
        if conviction is None:
            await ctx.send("Must specify a conviction to remove")
            return
        elem = Belief.query.filter_by(character=character, conviction=conviction).first()
        if elem is None:
            await ctx.send('Could not find conviction "{}" for character {}'.format(conviction, character.name))
        else:
            try:
                db.session.delete(elem)
                db.session.commit()
            except Exception as e:
                from colors import cprint
                cprint(e, color='red')
                await ctx.send("ERROR removing belief: {}".format(str(e)))
            else:
                await ctx.send("{} no longer has conviction {}".format(who.affected.mention, conviction))
    else:
        await ctx.send("Unknown option {}".format(option))
        return
