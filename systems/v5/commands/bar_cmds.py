from discord.ext import commands
from typing import Optional

from .commons import Who


def build_bar(total, aggravated, superficial):
    total_damage = aggravated + superficial
    out = ''
    for i in range(total):
        if i < aggravated:
            out += '[X]'
        elif i < total_damage:
            out += '[/]'
        else:
            out += '[ ]'
    return out


def fix_extrapolated_superficial(total, aggravated, superficial):
    """
    Fixes the extrapolated values, converts from superficial to aggravated if needed.

    :param total:
    :param aggravated:
    :param superficial:
    :return:
    """
    extrapolated = (aggravated + superficial) - total
    if extrapolated > 0:
        # If we extrapolated by an x amount we need to remove twice that much
        # once to get to the maximum of the health
        # and once again because these are being converted to aggravated
        superficial -= extrapolated * 2
        aggravated += extrapolated
        # Use a list to allow item assignment
    return [total, aggravated, superficial]


async def bar_cmd(ctx,
                  affected_attributes,
                  who=None,
                  option=None,
                  amount=1,
                  type_str='s',
                  ):
    """
    Base command used for both health and willpower

    :param affected_attributes: A tupple containing the name of the attributes for total, aggravated and superficial,
    e.g. ('health', 'h_aggravated', 'h_superficial')
    :param ctx:
    :param who:
    :param option:
    :param amount:
    :param type_str:
    :return:
    """
    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    values = [getattr(who.affected_character, e) for e in affected_attributes]

    # Only the GM can change the hunger
    if not who.is_gm:
        option = None

    if type_str in ['s', 'superficial']:
        position = 2
    elif type_str in ['a', 'aggravated']:
        position = 1

    if option is None:
        bar = build_bar(*values)
        await ctx.send("{} is at {}/{}/{}\n{}".format(who.affected.mention, *values, bar))
        return
    elif option in ['h', 'heal']:
        values[position] -= amount
    elif option in ['d', 'damage']:
        values[position] += amount
    elif option in ['s', 'set']:
        values[position] = amount
    else:
        await ctx.send('Uknown option "{}"'.format(option))
        return

    # Don't allow less than 0, otherwise extrapolated might get calculated wrongly
    if values[position] < 0:
        values[position] = 0

    # Fix the bar, if superficial damage extrapolates it should be converted to aggravated
    values = fix_extrapolated_superficial(*values)

    # Normalize the value so that it never goes below 0 or above the maximum total
    for i, v in enumerate(values):
        values[i] = max(v, 0)

    bar = build_bar(*values)
    await ctx.send("{} is at {}/{}/{}\n{}".format(
        who.affected.mention, *values, bar))

    for e, v in zip(affected_attributes, values):
        setattr(who.affected_character, e, v)

    from app.models import db
    try:
        db.session.add(who.affected_character)
        db.session.commit()
    except Exception as e:
        from colors import cprint
        cprint(e, color='red')
        db.session.rollback()
        await ctx.send("ERROR saving to database: {}".format(str(e)))


@commands.command(
    brief="Shows or alters a character Health bar",
    usage="[who(GM)] [option(GM)] [amount(GM)=1] [type(GM)=s]",
    help="""who: Discord mention
option: heal (h), damage (d) or set (s)
amount: The amount to heal/damage or set
type: superficial (s) or aggravated (a)

If no parameter is passed prints the character's Health

usage examples:

!health -> Shows your own character health
!health @player -> Shows the health of @player (GM only)
!health @player h -> Heals 1 point of superficial damage (GM only)
!health @player d 2 a -> Causes 2 points of aggravated damage (GM only)
"""
)
async def health(ctx,
                 who: Optional[Who] = None,
                 option=None,
                 amount=1,
                 type='s',
                 ):
    await bar_cmd(ctx, ('health', 'h_aggravated', 'h_superficial'), who, option, amount, type)


@commands.command(
    brief="Shows or alters a character Willpower bar",
    usage="[who(GM)] [option(GM)] [amount(GM)=1] [type(GM)=s]",
    help="""who: Discord mention
option: heal (h), damage (d) or set (s)
amount: The amount to heal/damage or set
type: superficial (s) or aggravated (a)

If no parameter is passed prints the character's Willpower

usage examples:

!willpower -> Shows your own character willpower
!willpower @player -> Shows the willpower of @player (GM only)
!willpower @player h -> Heals 1 point of superficial willpower damage (GM only)
!willpower @player d 2 a -> Causes 2 points of aggravated willpower damage (GM only)
"""
)
async def willpower(ctx,
                 who: Optional[Who] = None,
                 option=None,
                 amount=1,
                 type='s'):
    await bar_cmd(ctx, ('willpower', 'w_aggravated', 'w_superficial'), who, option, amount, type)
