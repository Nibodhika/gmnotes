import discord
from discord.ext import commands
from typing import Optional

from .commons import Who, humanity_bar


@commands.command(
    brief="Shows the character status",
    usage="[who(GM)]",
    help="""who: Discord mention

Shows the status of a character.
The Status is composed of:
- Name and Player
- Desire
- Humanity and stains
- Resonance
- Willpower bar
- Health bar
- Hunger

usage examples:

!status -> Shows the status of your character
!status @player -> Shows the status of @player's character (GM only)
"""
)
async def status(ctx,
                 who: Optional[Who] = None,
                 ):

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character
    from .bar_cmds import build_bar

    embed = discord.Embed(title=character.name, description=who.affected.mention)

    embed.add_field(name="Desire",
                    value=character.desire or '-',
                    inline=False)

    embed.add_field(name="Humanity {}/{}/{}".format(
                    character.humanity, character.free_humanity, character.stains),
                    value=humanity_bar(character.humanity, character.stains),
                    inline=False)

    embed.add_field(name="Resonance",
                    value=character.resonance.name if character.resonance else '-',
                    inline=False)

    embed.add_field(name="Willpower {}/{}".format(
                    character.willpower, character.current_willpower),
                    value=build_bar(character.willpower, character.w_aggravated, character.w_superficial),
                    inline=False)

    embed.add_field(name="Health {}/{}".format(
                    character.health, character.current_health),
                    value=build_bar(character.health, character.h_aggravated, character.h_superficial),
                    inline=False)

    embed.add_field(name="Hunger",
                    value=character.hunger,
                    inline=False)

    await ctx.send(embed=embed)
