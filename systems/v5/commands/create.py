from discord.ext import commands
from typing import Optional

from .commons import Who


@commands.command()
async def create(ctx,
                 who: Optional[Who] = None
                 ):
    if who is None:
        who = Who(ctx)

    if who.affected_character is not None:
        await ctx.send("You already have a character named {}".format(who.affected_character.name))
        return
