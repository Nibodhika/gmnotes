from discord.ext import commands
from random import randint
from typing import Optional

from .commons import Who


@commands.command(
    brief="Rerolls the last roll made by the character",
    usage="[who(GM)] [args...]",
    help="""who: Discord mention
args: A space separated list of values to reroll, default to fail

WARNING: This costs one point of superficial willpower damage

You can set the individual dice results or use the following abbreviations:
- fail: 1, 2, 3, 4, 5
- tens: 10
- nontens: 1, 2, 3, 4, 5, 6, 7, 8, 9
- messy: 1, 2, 3, 4, 5, 10

Then performs the roll again and shows the new result as well as the updated willpower bar.

usage examples:

!reroll -> Rerolls all failed dice of the last roll
!reroll @player 1 2 -> Rerolls all dice with value 1 or 2 from the last roll of the character (GM only)
!reroll fail tens -> Rerolls all dice with values 1, 2, 3, 4, 5 or 10 from the last roll
"""
)
async def reroll(ctx,
                 who: Optional[Who] = None,
                 *args
                 ):
    from systems.v5.models import Roll

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    character = who.affected_character

    last_roll = Roll.query.filter_by(character=character).order_by(Roll.id.desc()).first()

    if last_roll is None:
        await ctx.send("This player has not rolled anything yet")
        return

    available_willpower = character.willpower * 2 - character.w_aggravated * 2 - character.w_superficial
    if available_willpower <= 0:
        await ctx.send("Your character has no willpower left, and can't reroll dice")
        return

    # First damage willpower
    character.w_superficial += 1
    from systems.v5.commands.bar_cmds import fix_extrapolated_superficial
    character.willpower, character.w_aggravated, character.w_superficial = fix_extrapolated_superficial(
        character.willpower, character.w_aggravated, character.w_superficial)

    try:
        from app.models import db
        db.session.add(character)
        db.session.commit()
    except Exception as e:
        db.rollback()
        from colors import cprint
        cprint(e, color='red')
        await ctx.send("ERROR trying to save willpower change: {}".format(str(e)))
        return

    reroll_types = {
        'fail': [1, 2, 3, 4, 5],
        'tens': [10],
        'nontens': [1, 2, 3, 4, 5, 6, 7, 8, 9],
        'messy': [1, 2, 3, 4, 5, 10]
    }

    if len(args) == 0:
        # Default is to do a reroll fail
        args = ('fail',)

    dice_to_reroll = []
    for d in args:
        try:
            v = [int(d)]
        except ValueError:
            if d not in reroll_types:
                await ctx.send('"{}" is not a valid reroll type or number'.format(d))
                return
            v = reroll_types[d]
        dice_to_reroll.extend(v)

    reroll_values = set(dice_to_reroll)

    dice, hunger_dice, regular_dice = last_roll.get_dice()
    # Only regular_dice can be rerolled
    before = len(regular_dice)
    kept_dice = [d for d in regular_dice if d not in reroll_values]
    after = len(kept_dice)
    if before == after:
        await ctx.send('No dice selected to reroll')
        return

    roll_amount = before - after
    rolled_dice = [randint(1, 10) for _ in range(roll_amount)]
    new_dice = hunger_dice + kept_dice + rolled_dice

    from systems.v5.commands.roll import RollResult
    result = RollResult(new_dice, last_roll.difficulty, last_roll.hunger)

    embed = result.embed()
    # Since this spends willpower it's a good idea to add a new field with it at the end
    from systems.v5.commands.bar_cmds import build_bar
    bar = build_bar(character.willpower, character.w_aggravated, character.w_superficial)
    embed.add_field(name="Willpower", value=bar)
    await ctx.send(embed=embed)

    e = result.save(who.affected_character)
    if e is not None:
        await ctx.send("ERROR saving to database: {}".format(str(e)))
