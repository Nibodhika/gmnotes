import discord
from discord.ext import commands
from typing import Optional

from .commons import Who, embed_divider


def embed_sheet(who):
    character = who.affected_character

    embed = discord.Embed(title=character.name, description=character.concept or '-')

    embed.add_field(name="Ambition",
                    value=character.ambition or '-',
                    inline=False)

    embed.add_field(name='Player',
                    value=who.affected.mention,
                    inline=True)
    embed.add_field(name='Clan',
                    value=character.clan.name or '-',
                    inline=True)
    embed.add_field(name='Generation',
                    value=character.generation or '-',
                    inline=True)

    # Attributes
    embed_divider(embed, "Attributes")

    for attribute_type, attributes in character.get_attributes().items():
        attributes_str = '\n'.join(['{}: {}'.format(*i) for i in attributes])
        embed.add_field(name=attribute_type, value=attributes_str, inline=True)

    # Skills
    embed_divider(embed, "Skills")

    from systems.v5.models import Skill, SkillType
    skills = {i: [] for i in SkillType}
    for skill in Skill.query.all():
        skill_str = str(character.skill(skill))
        specialization = character.specialization(skill)
        if len(specialization):
            skill_str += ' ({})'.format(', '.join(specialization))
        skills[skill.skill_type].append((skill.name, skill_str))

    for skill_type, skills in skills.items():
        skills_str = '\n'.join(['{}: {}'.format(*i) for i in skills])
        embed.add_field(name=skill_type.name, value=skills_str, inline=True)

    if len(character.disciplines) > 0:
        embed_divider(embed, "Disciplines")
        for d in character.disciplines:
            title = '{}: {}'.format(d.discipline.name, d.value)
            content = []
            for p in character.powers:
                if p.power.requirements[0].discipline == d.discipline:
                    content.append(p.power.name)
            content = '\n'.join(content)

            # If no power we need something
            if not content:
                content = '\u200b'
            embed.add_field(name=title, value=content, inline=True)

    if len(character.advantages) > 0 or len(character.thin_blood_advantages) > 0:
        embed_divider(embed, "Advantages")
        advantages = {
            'Merits': [],
            'Flaws': [],
            'Thin Blood': []
        }
        for a in character.advantages:
            adv_str = "**{} ({})**\n{}".format(a.advantage.name, a.value, a.specification or '-')
            if a.advantage.is_flaw:
                advantages['Flaws'].append(adv_str)
            else:
                advantages['Merits'].append(adv_str)
        for a in character.thin_blood_advantages:
            adv_str = "**{} ({})**\n{}".format(a.thin_blood_advantage.name,
                                               'flaw' if a.thin_blood_advantage.is_flaw else 'merit',
                                               a.specification or '-')
            advantages['Thin Blood'].append(adv_str)

        for adv_type, values in advantages.items():
            if not values:
                continue
            adv_str = "\n".join(values)
            embed.add_field(name=adv_type, value=adv_str, inline=True)

    return embed


@commands.command(
    brief="Prints or alters information about a character",
    usage="[who(GM)] [what] [value(GM)]",
    help="""who: Discord mention
    what: The name element (or valid Alias)
    value: The value to set to the element
    
    If no parameter is passed prints the character sheet.
    If a "what" is set returns the value of that element.
    GMs can use "who" to see/alter other player's characters.
    GMs can use "value" to alter the value of the element.
    
    usage examples:
    
    !character @player -> Shows the sheet of the character of @player (GM only)
    !character str     -> Shows the value of your own character strength
    !character @player pro 2 -> Sets the level 2 to the discipline Protean (GM only) 
    """
)
async def character(ctx,
                    who: Optional[Who] = None,
                    what=None,
                    value=None,
                    ):
    from systems.v5.models import Alias

    if who is None:
        who = Who(ctx)

    if who.affected_character is None:
        await ctx.send("Could not find a character for {}".format(who.affected.mention))
        return

    char = who.affected_character

    if what is None:
        await ctx.send(embed=embed_sheet(who))
        return

    if not who.is_gm:
        await ctx.send("Only GMs can do change values")
        return

    if value is None:
        value, name = Alias.get_referenced_value_and_name(what, char)
        await ctx.send("{}'s {}: {}".format(who.affected.mention, name, value))
    else:
        try:
            value = int(value)
        except ValueError:
            await ctx.send('{} is not a valid int'.format(value))
            return

        try:
            name = Alias.set_referenced_value(what, char, value)
        except ValueError:
            await ctx.send('Could not set value "{}" to "{}"'.format(what, value))
        else:
            await ctx.send("{} now has {} in {}".format(who.affected.mention, value, name))
