"""
Example of a minimum working system.
"""
from random import randint
from discord.ext import commands
from app.system import System


@commands.command()
async def roll(ctx):
    dice = [randint(1, 6) for _ in range(3)]
    result = sum(dice)
    await ctx.send('You rolled {} ({})'.format(result, dice))

system = System("gurps", cmd_list=[roll])
