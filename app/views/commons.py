from flask_security.utils import hash_password
from flask_security.views import logout
from flask_wtf import FlaskForm
from flask import Blueprint, render_template, redirect, request, url_for
from flask_login import login_required, current_user
from wtforms import SelectField, StringField, TextAreaField, validators

from app.models import Campaign, Player
from app.system import manager


bp = Blueprint('main', __name__)


def campaign_url(campaign):
    from app.system import manager
    return '/'.join([manager.get_base_url(campaign.system), str(campaign.id)])


@bp.context_processor
def inject_campaign_url():
    return {
        'campaign_url': campaign_url
    }


@bp.route('/')
@login_required
def index():
    return render_template('index.html')


@bp.route('/campaign/new', methods=('GET', 'POST'))
@login_required
def new_campaign():
    from app.models import db
    from app.views import CampaignForm

    form = CampaignForm()
    form.system.choices = [(s, s) for s in manager.systems]
    if form.validate_on_submit():
        campaign = Campaign(name=form.name.data,
                            description=form.description.data,
                            system=form.system.data,
                            gm=current_user
                            )
        try:
            db.session.add(campaign)
            db.session.commit()
        except Exception as e:
            form.errors['SQL'] = [str(e)]
        else:
            return redirect(campaign_url(campaign))

    return render_template('edit_campaign.html', form=form, href=url_for('main.new_campaign'))


@bp.route('/campaign/<campaign:campaign>', methods=('GET', 'POST'))
@login_required
def campaign(campaign):
    from app.models import db
    from app.views import CampaignForm

    if not campaign or campaign.gm != current_user:
        generic_error = "Only the GM can edit the campaign"
        return render_template("base/error.html", campaign=None, errors=[generic_error]), 400

    form = CampaignForm(obj=campaign)
    # Cannot modify system once created
    form.system.choices = [(campaign.system, campaign.system)]
    if form.validate_on_submit():
        form.populate_obj(campaign)
        try:
            db.session.add(campaign)
            db.session.commit()
        except Exception as e:
            form.errors['SQL'] = [str(e)]
        else:
            return redirect(campaign_url(campaign))

    return render_template('edit_campaign.html', form=form, href=url_for('main.campaign', campaign=campaign))


@bp.route('/campaign/<campaign:campaign>/del')
@login_required
def del_campaign(campaign):
    if not campaign or campaign.gm != current_user:
        generic_error = "Only the GM can delete the campaign"
        return render_template("base/error.html", campaign=None, errors=[generic_error]), 400

    try:
        from app.models import db
        db.session.delete(campaign)
        db.session.commit()
    except Exception as e:
        db.rollback()
        from colors import cprint
        cprint(e, color='red')
        return render_template("base/error.html", campaign=campaign, errors=[str(e)]), 400

    return redirect(url_for('main.index'))


@bp.route('/settings', methods=('GET', 'POST'))
@login_required
def settings():
    from app.views.model_forms import PlayerSettingsForm
    from flask_security.forms import ChangePasswordForm
    from app.models import db

    user_form = PlayerSettingsForm(obj=current_user)
    change_password_form = ChangePasswordForm()

    if request.form:
        # We have a hidden field to say which form is submitted
        submitted_form = request.form['submitted_form']

        if submitted_form == 'general':
            if user_form.validate_on_submit():
                if not user_form.email.data:
                    user_form.email.data = None
                user_form.populate_obj(current_user)
                try:
                    db.session.add(current_user)
                    db.session.commit()
                except Exception as e:
                    # TODO parse the error better
                    print(e)
                    db.session.rollback()

        elif submitted_form == 'change_password':
            if change_password_form.validate_on_submit():
                print("Validated change password")
                new_password = change_password_form.new_password.data
                current_user.password = hash_password(new_password)
                try:
                    db.session.add(current_user)
                    db.session.commit()
                except Exception as e:
                    # TODO parse the error better
                    print(e)
                    db.session.rollback()

    return render_template('settings/settings.html', user_form=user_form, change_password_form=change_password_form)


@bp.route('/del_my_acc')
@login_required
def del_my_acc():
    delete_user(current_user)
    logout()
    return redirect(url_for('security.login'))


def delete_user(user):
    """
    Helper function to delete a user.
    Only admins can delete anyone else, but a regular user should only call this to delete his own account.

    It should be impossible for a regular user to call this with anything other than current_user,
    but as a safeguard if a non-admin user tries to call this delete his account independent of parameter.
    :param user:
    :return:
    """
    from app.models import db

    if not current_user.has_role('admin'):
        user = current_user
    elif user is None:
        return "Could not find specified user"

    db.session.delete(user)
    db.session.commit()
    return None


@bp.route('/players')
@login_required
def list_players():
    like = request.args.get('like')
    if like is None:
        like = '%'
    players = Player.query.filter(Player.name.like(like)).all()
    return {'players': [p.name for p in players]}
