from .commons import bp
from .dashboard import *
# While not expected to be used, some systems might create classes with the same name, e.g. Character
# if the model forms are not imported first SQLAlchemy gets confused about it
# (even if it's passing the class object to the meta)
from .model_forms import *
