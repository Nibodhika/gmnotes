from flask import render_template, url_for
from flask_security import roles_accepted, roles_required
from werkzeug.utils import redirect

from app.views.commons import bp, delete_user


@bp.route('/dashboard', methods=('GET', 'POST'))
@roles_accepted('admin', 'manager')
def dashboard():
    from app.config import ConfigForm
    form = ConfigForm()

    if form.validate_on_submit():
        form.save_config()
    else:
        form.load_config()

    return render_template('dashboard/dashboard.html', dashboard=True, form=form)


@bp.route('/users')
@roles_required('admin')
def users():
    from app.models import Player
    from app.views.model_forms import PlayerForm

    user_list = {
        'users': []
    }

    for p in Player.query.all():
        user_form = PlayerForm(p)
        user_form.load_obj(p)
        content = render_template('dashboard/user_short.html', dashboard=True, user_form=user_form, user=p)
        user_list['users'].append({
            'title': p.name,
            'content': content,
            'id': 'user_{}'.format(p.id)
        })

    return render_template('dashboard/users.html', dashboard=True, user_list=user_list)


@bp.route('/user/<player:user>', methods=('POST',))
@roles_required('admin')
def save_user(user):
    from app.views.model_forms import PlayerForm
    from app.models import db

    form = PlayerForm(user)
    if form.validate_on_submit():
        if not form.email.data:
            form.email.data = None
        form.populate_obj(user)

        try:
            db.session.add(user)
            db.session.commit()
        except Exception as e:
            # TODO parse the error better
            err = str(e)
            db.session.rollback()
            return render_template("base/error.html", dashboard=True, errors=[err])
        else:
            return redirect(url_for('.users'))

    return render_template("base/error.html", dashboard=True, errors=form.errors)


@bp.route('/user/<player:user>/del')
@roles_required('admin')
def del_user(user):
    """
    View to delete a user from the dashboard, automatically redirects to the users view afterwards.
    Note that the actual deletion happens in delete_user

    :param user:
    :return:
    """
    err = delete_user(user)
    if err:
        return render_template("base/error.html", dashboard=True, errors=[err])

    return redirect(url_for('.users'))
