from flask_security.forms import email_validator
from flask_wtf import FlaskForm
from wtforms import SelectField, BooleanField, StringField, DateField
from wtforms_alchemy import model_form_factory

from app.config import config
from app.models import Campaign, Character, Location, GameObject, GameSession, Player

BaseModelForm = model_form_factory(FlaskForm)


def text_or_form(object, form, elem, access_level, parse_markdown=True):
    """
    Helper function to return either the form value or a pure string depending on access_level.
    If access level allows writing to the object returns the editor
    otherwise returns the content

    :param object: The object to get the value from
    :param form: The form to get the form element from
    :param elem: The name of the element to get
    :param access_level: The access level to check
    :param parse_markdown: Whether the element of the object should be parsed as markdown
    :return:
    """
    from app.models import AccessLevel
    from markdown import markdown

    if access_level & AccessLevel.EDIT:
        return form[elem]
    elif parse_markdown:
        return markdown(getattr(object, elem) or "")
    return getattr(object, elem) or ""


class ModelForm(BaseModelForm):
    @classmethod
    def get_session(self):
        from app.models import db
        return db.session


class CampaignForm(ModelForm):
    class Meta:
        model = Campaign

    system = SelectField('System')


class CharacterForm(ModelForm):
    class Meta:
        model = Character

    player_id = SelectField('Player', coerce=int)


class LocationForm(ModelForm):
    class Meta:
        model = Location

    owner_id = SelectField('Character', coerce=int)


class GameObjectForm(ModelForm):
    class Meta:
        model = GameObject

    owner_id = SelectField('Character', coerce=int)


class GameSessionForm(ModelForm):
    class Meta:
        model = GameSession

    date = DateField("Date", format=config.DATE_FORMAT)


class PlayerForm(FlaskForm):
    """
    PlayerForm cannot be a model form because currently we're showing several of them on the same page in the dashboard
    """
    name = StringField()
    email = StringField()
    discord_id = StringField()
    is_admin = BooleanField()
    active = BooleanField()

    def __init__(self, player, *args, **kwargs):

        super(PlayerForm, self).__init__(*args, **kwargs)

        for k in self.all_fields():
            k.id = '{}_{}'.format(k.id, player.id)

    def all_fields(self):
        from wtforms.csrf.core import CSRFTokenField
        for k in self:
            if type(k) == CSRFTokenField:
                continue
            yield k

    def populate_obj(self, player):
        for k in self.all_fields():
            setattr(player, k.name, k.data)

    def load_obj(self, player):
        for k in self.all_fields():
            k.data = getattr(player, k.name)


class PlayerSettingsForm(ModelForm):
    class Meta:
        model = Player
        exclude = ['password', 'active']

    def validate_email(self, field):
        # Allow empty
        if not field.data:
            return
        email_validator(self, field)
