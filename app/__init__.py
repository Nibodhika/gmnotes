from flask import Flask
from flask_htmlmin import HTMLMIN


class GMNotes(Flask):

    def run(self, *args, **kwargs):
        from app.discord_bot import manager as bot_manager
        import os

        debug = kwargs.get('debug', self.debug)
        thread = None
        # When the application runs in debug it starts the run twice
        # Prevent the bot_manager from being started twice in that scenario
        # and only start it in the instance that will actually get called again
        # when the code is reloaded
        if not debug or os.environ.get('WERKZEUG_RUN_MAIN') == 'true':
            thread = bot_manager.run()
        try:
            super(GMNotes, self).run(*args, **kwargs)
        finally:
            if thread is not None:
                bot_manager.stop()
                print("Gracefully exiting bot manager, this might take a while")
                thread.join()


def create_app(config_file=None, config_dict=None):
    app = GMNotes(__name__)

    # Load configurations
    from app.config import config
    if config_file is not None:
        config.load_config(config_file)
    elif config_dict is not None:
        config.load_from_dict(config_dict)
    app.config.from_object(config)

    @app.before_first_request
    def set_api_url():
        """
        Sets the api url to the bot manager so bots know where to make requests if needed.

        This is a hack to fix the fact that the bot has no idea of where the api is.
        The correct way to fix this is to remove the app dependency from the hash_password, by reimplementing it
        reading the values from config. This is however somewhat difficult,
        and it might be ideal to remove the flask-security entirely and go back to flask-login
        and doing the hash by hand.

        :return:
        """
        from app.discord_bot import manager as bot_manager
        from flask import url_for
        api_url = url_for('api.index', _external=True)
        bot_manager.api_url = api_url

    # HTML minifier
    htmlmin = HTMLMIN(app)

    # Init mail
    from app.mail import mail
    mail.init_app(app)

    # Initialize database
    from app.models import commons
    commons.db.app = app
    commons.db.init_app(app)

    # Setup Flask-Security
    from . import security
    security.init_app(app)

    # Flask-Markdown
    from flaskext.markdown import Markdown
    Markdown(app)

    # Converters
    from app.converters import converters
    for name, converter in converters.items():
        app.url_map.converters[name] = converter

    # Filters
    from app.filters import filters
    filters(app)

    # Initialize API
    from app.views.api import bp as api
    app.register_blueprint(api)

    # Initialize views
    from app.views import bp
    app.register_blueprint(bp)

    # Load systems
    from app.system import manager
    manager.init_app(app)

    return app
