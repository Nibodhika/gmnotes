from binascii import unhexlify

from shutil import copyfile


def test_config_defaults():
    """
    Tests the default values of the Configuration, since we don't store the defaults in the configuration file
    changing the defaults is not backwards compatible so we need a test to ensure it doesn't happen
    """
    from app.config import _Config
    
    assert _Config.SECURITY_REGISTERABLE

    assert _Config.SQLALCHEMY_DATABASE_URI == 'sqlite:///gmnotes.db'
    assert not _Config.DEBUG

    assert _Config.MAX_TOKEN_AGE == 86400
    assert _Config.SECRET_KEY_LENGTH == 32
    assert _Config.SECURITY_PASSWORD_SALT_LENGTH == 16

    assert _Config.MAIL_SERVER == 'localhost'
    assert _Config.MAIL_DEFAULT_SENDER == "gmnotes"
    assert not _Config.SECURITY_CONFIRMABLE
    assert not _Config.SECURITY_SEND_REGISTER_EMAIL
    assert not _Config.SECURITY_SEND_PASSWORD_CHANGE_EMAIL
    assert not _Config.SECURITY_SEND_PASSWORD_RESET_EMAIL
    assert not _Config.SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL

    assert _Config.SECRET_KEY is None
    assert _Config.SECURITY_PASSWORD_SALT is None

    assert _Config.SECURITY_USER_IDENTITY_ATTRIBUTES == ['name', 'email']
    assert not _Config.SQLALCHEMY_COMMIT_ON_TEARDOWN
    assert not _Config.SQLALCHEMY_TRACK_MODIFICATIONS
    assert not _Config.TESTING


def test_config_load_on_create(files, tmp_path):
    """
    Tests that when a config is created it loads the config file and creates or loads the key
    """
    from app.config import _Config

    config_file = tmp_path / "config.yaml"
    orig = files / 'non_default_config.yaml'
    copyfile(orig, config_file)

    key_file = tmp_path / 'key'
    salt_file = tmp_path / 'salt'

    c = _Config(config_file=config_file, key_file=key_file, password_salt_file=salt_file)
    # Ensure the parameters were loaded correctly
    assert not c.SECURITY_REGISTERABLE
    assert c.SQLALCHEMY_DATABASE_URI == 'sqlite:///another.db'
    assert c.DEBUG
    assert c.MAX_TOKEN_AGE == 123
    assert c.SECRET_KEY_LENGTH == 16
    assert c.SECURITY_PASSWORD_SALT_LENGTH == 8
    assert c.MAIL_SERVER == 'remotehost'
    assert c.MAIL_DEFAULT_SENDER == "anothername"
    assert c.SECURITY_CONFIRMABLE
    assert c.SECURITY_SEND_REGISTER_EMAIL
    assert c.SECURITY_SEND_PASSWORD_CHANGE_EMAIL
    assert c.SECURITY_SEND_PASSWORD_RESET_EMAIL
    assert c.SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL
    # These were not in the yaml, and should preserve their default values
    assert c.SECURITY_USER_IDENTITY_ATTRIBUTES == ['name', 'email']
    assert not c.SQLALCHEMY_COMMIT_ON_TEARDOWN
    assert not c.SQLALCHEMY_TRACK_MODIFICATIONS
    assert not c.TESTING

    # Key and salt should now be created
    with open(key_file, 'rb') as ifs:
        key = ifs.read()
    assert c.SECRET_KEY == key
    assert len(key) == c.SECRET_KEY_LENGTH

    with open(salt_file, 'r') as ifs:
        salt = ifs.read()
    assert c.SECURITY_PASSWORD_SALT == salt
    # The length of the security is of the binary data, but the file saves an hexdigest
    salt_data = unhexlify(salt)
    assert len(salt_data) == c.SECURITY_PASSWORD_SALT_LENGTH

    # If we create a config after those files exists it should load them and not change them
    c2 = _Config(config_file=config_file, key_file=key_file, password_salt_file=salt_file)
    with open(key_file, 'rb') as ifs:
        key_after = ifs.read()
    assert key_after == key
    assert c2.SECRET_KEY == key

    with open(salt_file, 'r') as ifs:
        salt_after = ifs.read()
    assert salt_after == salt
    assert c2.SECURITY_PASSWORD_SALT == salt


def test_save_config(tmp_path):
    """
    Tests that the save_config is working correctly
    """
    from app.config import _Config
    config_file = tmp_path / 'config.yaml'
    c = _Config(config_file=config_file)

    c.save_config()
    # All options are the default, so the file should be empty
    with open(config_file, 'r') as ifs:
        content = ifs.read()
    assert content == ""

    # Change a setting
    c.DEBUG = True
    c.save_config()
    with open(config_file, 'r') as ifs:
        content = ifs.read()
    assert content == "DEBUG: true\n"

    # If we change it back the file should be empty again
    c.DEBUG = False
    c.save_config()
    with open(config_file, 'r') as ifs:
        content = ifs.read()
    assert content == ""

    # A test with more than one setting changed
    c.SECURITY_REGISTERABLE = False
    c.MAX_TOKEN_AGE = 123
    c.save_config()
    with open(config_file, 'r') as ifs:
        content = ifs.read()
    assert content == "MAX_TOKEN_AGE: 123\nSECURITY_REGISTERABLE: false\n"

    # Set one back to the default
    c.SECURITY_REGISTERABLE = True
    c.save_config()
    with open(config_file, 'r') as ifs:
        content = ifs.read()
    assert content == "MAX_TOKEN_AGE: 123\n"
