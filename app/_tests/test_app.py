from app import GMNotes

def testBotManagerIsRunning(app):
    from app.system import manager
    assert type(app) == GMNotes
    assert 'v5' in manager.systems
    # assert bot_manager.loop is not None
