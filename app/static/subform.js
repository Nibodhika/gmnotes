/**
 * Removes a subform and adjusts the indices
 *
 * @param {object} elem The element that is getting removed
 * @param {string} subform_class The class used for the subforms to find the subform and update the indices
 */
function removeSubform(elem, subform_class) {
    var $removedForm = elem.closest('.' + subform_class);
    var index = $removedForm.getAttribute('index')
    var removedIndex = parseInt(index);

    $removedForm.remove();

    // Update indices
    $('.' + subform_class).each(function(i) {
        var $form = $(this);
        var index = parseInt($form.attr('index'));
        var newIndex = index - 1;
        if (index < removedIndex) {
            // Skip
            return true;
        }

        // Change ID in form itself
        $form.attr('index', newIndex);

        $form.find('select, input, textarea, div').each(function(idx) {
            var $item = $(this);

            var current_id = $item.attr('id')
            if(current_id)
                $item.attr('id', current_id.replace(index, newIndex));

            var current_name = $item.attr('name')
            if(current_name)
                $item.attr('name', current_name.replace(index, newIndex));
        });

    });
}

/**
 * Adds a new subforms from the given template
 *
 * @param {string} template_id The id of the template to use. The template should have a _ where the index should go
 * @param {string} subform_class The class to add to the template after it's copied
 * @param {string} list_id The id of the list where to put the new subform
 */
function addSubform(template_id, subform_class, list_id) {
    var $templateForm = $('#' + template_id);

    if (!$templateForm) {
        console.error('[ERROR] Cannot find template');
        return;
    }

    // Get Last index
    var newIndex = $('.' + subform_class).length;

    // Add elements
    var $newForm = $templateForm.clone();

    // Remove the template id and put the new index
    $newForm.attr('id', null);
    $newForm.attr('index', newIndex);

    // Update all inputs in the form
    $newForm.find('select, input, textarea, div').each(function(idx) {
        var $item = $(this);

        var current_id = $item.attr('id')
        if(current_id)
            $item.attr('id', current_id.replace('?', newIndex));

        var current_name = $item.attr('name')
        if(current_name)
            $item.attr('name', current_name.replace('?', newIndex));
        // Some forms set their elements to disable to allow them to keep the required but not being submitted
        $item.removeAttr('disabled')
    });

    // TODO the textareas are having problems, probably something needs to be initialized here
    $newForm.find('textarea').each(function(idx) {
        var textarea = this;
        a = textarea
    })

    // Append
    $('#' + list_id).append($newForm);
    $newForm.addClass(subform_class);
    $newForm.removeClass('is-hidden');
}
