function get_token() {
console.log(document.getElementById("player_id"))
    var player_url = document.getElementById("player_id").value
    console.log("Copying: ", player_url)
    return fetch(player_url)
    .then((resp) => resp.text())
}

// According to every source this is the correct way to set clipboard content on js
// I feel this deserves a joke about it, but I'm just too sad to do so
function setClipboardText(text){
    var id = "secret-token-hidden-id";
    var textarea = document.getElementById(id);

    if(!textarea){
        var textarea = document.createElement("textarea");
        textarea.id = id;
        // Place in top-left corner of screen regardless of scroll position.
        textarea.style.position = 'fixed';
        textarea.style.top = 0;
        textarea.style.left = 0;

        // Ensure it has a small width and height. Setting to 1px / 1em
        // doesn't work as this gives a negative w/h on some browsers.
        textarea.style.width = '1px';
        textarea.style.height = '1px';

        // We don't need padding, reducing the size if it does flash render.
        textarea.style.padding = 0;

        // Clean up any borders.
        textarea.style.border = 'none';
        textarea.style.outline = 'none';
        textarea.style.boxShadow = 'none';

        // Avoid flash of white box if rendered for any reason.
        textarea.style.background = 'transparent';
        document.querySelector("body").appendChild(textarea);
    }

    textarea.value = text;
    textarea.select();

    try {
        var status = document.execCommand('copy');
        if(!status){
            alert("Problem copying the token")
        }else{
            alert("Copied the token")
        }
    } catch (err) {
        console.log("Problem copying the token")
    }
}


 function send_token_via_mail() {
    get_token()
    .then(function(token){
        console.log("Sending token via mail" + token)
    })

 }

 function copy_token() {
    get_token()
    .then(function(token){
        console.log("copying", token)
        setClipboardText(token)
    })
 }

 function send_token_via_discord(){
    get_token()
    .then(function(token){
        console.log("Sending token via discord" + token)
    })
 }