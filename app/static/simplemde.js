/*
Simple script that converts every textarea into a SimpleMDE.

It also registers on the current page's tab show function to call the codemirror refresh
Otherwise editors inside a tab that starts hidden would not initialize their content until clicked.

And fixes problems of compatibility with FontAwesome 5
*/

$('textarea').each(function () {
    var simplemde = new SimpleMDE({
        element: this,
        spellChecker: false
    });
    simplemde.render();

    // Listen to any shown events on tab to refresh
    // a bit of overkill but better than having to rewrite this code specifically for each page that has a tab with a textarea
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        simplemde.codemirror.refresh();
    });

    // Same for collapsables
    $('div[class="collapse"]').on('shown.bs.collapse', function (e) {
        simplemde.codemirror.refresh();
    });

    // Same for list-tabs
    $('a[data-toggle="list"]').on('shown.bs.tab', function (e) {
        simplemde.codemirror.refresh();
    });


});

// Add the new names of icons for the old ones
$(document).ready(function() {
  // fa-header was renamed to fa-heading
  $('.fa-header').addClass('fa-heading');
  // Closest to fa-picture-o is fa-image
  $('.fa-picture-o').addClass('fa-image');
});