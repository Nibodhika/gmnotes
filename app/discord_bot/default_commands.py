import discord
import json
import re
import requests
from collections import namedtuple
from discord.ext import commands
from flask import url_for


def is_gm(ctx):
    return discord.utils.get(ctx.author.roles, name='GM') is not None


def get_who(ctx, who):
    """
    Helper function that given a context and a who parameter either returns the who that was sent or the sender of the message depending on the roles of the sender.

    If the sender is a GM and he sent a parameter returns that parameter
    otherwise replaces it with the sender

    After calling this function on a parameter it's ensured that non-GMs are only able to alter their own accounts

    :param ctx:
    :param who:
    :return:
    """

    if who is None or not is_gm(ctx):
        who = ctx.author
    return who


nds = re.compile(r'\b(\d*)d(\d+)(?:\+(\d+)|\b)', re.IGNORECASE)


def roll_nds(string):
    """
    Helper function to parse a string in the format <number>d<sides>[+<plus>] where:
    - number is the number of dice to roll or 1 if not specified
    - sides is the number of sides of the dice to roll
    - plus is an optional parameter that is returned directly or 0 if not given

    It uses the nds regex defined globally above in case any system wants to use it.
    It can make multiple rolls, which is why it returns an array of the results.
    Each result is a tuple where the first element is an array of the dice results
    and the second element is the plus value, e.g. the string "1d6 2d10+3" might result in:
    [ ( [ 3 ], 0 ), ( [2, 8], 3 ) ]
          |    |       |  |   |---- Second roll was +3
          |    |       |  |-------- Second d10 rolled an 8
          |    |       |----------- First d10 rolled a 2
          |    |------------------- First roll was + 0
          |------------------------ The only d6 rolled a 3

    :param string: The String to parse
    :return: The result of the dice throws, an array of tuples of array and int [ ( [int], int) ]
    """
    from random import randint

    values = [(int(n or 1), int(s), int(p or 0)) for n, s, p in nds.findall(string)]

    output = [
        ([randint(1, s) for _ in range(n)], p)
        for n, s, p in values
    ]
    return output


@commands.command(
    brief="Generic dice rolling command",
    help="""Finds any string in the format <x>d<s>[+p] (e.g. 3d6+2) in the message and runs the correct dice,
    The [+p] part is optional, so for example 3d6 is valid
    If no <x> is provided 1 is assumed.
    
    It prints the result for each roll as a list and the sum, e.g.
    Result: 1, 2, 3 (6)
    
    usage examples:

!roll 3d6 -> rolls 3 6-sided dice
!roll d20 -> rolls one 20-sided dice
!roll 2d4+3 -> rolls 2 4-sided dice and adds 3 to the sum result
"""
)
async def roll(ctx, *, arg=''):
    results = roll_nds(arg)
    out = 'Not a valid roll'
    if len(results) > 0:
        out = '\n'.join(['Result: {} ({})'.format(
            ', '.join(list(map(str, r))),
            sum(r) + p
        ) for r, p in results])
    await ctx.send(out)


@commands.command(
    brief="Manage players in the campaign",
    usage="[option] [parameter]",
    help="""option: create (c)
parameter: option dependent parameter

Create:
Creates a user on the server, the parameter is the username.
If no parameter is given uses your nick.
After creation sends you your password via PM

usage examples:

!player create -> Creates a player using your nick as username
!player create MyNick -> Creates a player using MyNick as username
"""
)
async def player(ctx, option=None, parameter=None):
    from app.models import Player

    discord_id = ctx.author.id
    player = Player.query.filter_by(discord_id=discord_id).first()

    async def create(parameter):
        from app.discord_bot import manager as bot_manager

        nonlocal player, discord_id
        # If no parameter was passed use the nick of the user
        if parameter is None:
            parameter = ctx.author.name

        if player is not None:
            await ctx.send("You already have an account with username {}".format(player.name))
        elif Player.query.filter_by(name=parameter).count() > 0:
            await ctx.send("Username already taken")
        elif bot_manager.api_url is None:
            await ctx.send("Please visit any page on the server to set the url for the api on the bot automatically")
        else:
            r = requests.post(bot_manager.api_url + 'user',
                              headers={'Content-Type': 'application/json'},
                              data=json.dumps({'name': parameter, 'discord_id': discord_id}))
            if r.status_code == 200:
                ret = r.json()
                await ctx.author.send("Created player with username {} and password {}".format(
                    ret['name'], ret['password']))
            else:
                print("Problem accessing {}".format(bot_manager.api_url + 'user'))
                await ctx.send("Problem creating user: {}".format(r.content))

    Option = namedtuple('Option', 'accepted_values func description')
    valid_options = {
        'create': Option(accepted_values=['c', 'create'], func=create,
                         description="Creates a player with the passed name"),
    }

    if option is None:
        msg = "You must provide a valid option. Valid options are:"
        for k, v in valid_options.items():
            accepted_values = ','.join([i for i in v.accepted_values if i != k])
            msg += "\n  - {} ({}): {}".format(k, accepted_values, v.description)
        await ctx.send(msg)
    for k, v in valid_options.items():
        if option in v.accepted_values:
            await v.func(parameter)
            return

cmd_list = [
    roll,
    player
]
