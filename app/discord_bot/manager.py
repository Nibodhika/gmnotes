"""
This is a singleton to manage the bots that are running.
"""

import asyncio
import threading

import discord

from colors import cprint as print

loop = None
bots = {}
api_url = None


def is_running(bot):
    return bot.token in bots


def run():
    manager_thread = threading.Thread(target=_run)
    manager_thread.start()
    return manager_thread


async def _check_dead():
    """
    If there is no await in the loop it never closes.
    This is a helper coroutine to ensure that even if no bot is present we can still gracefully close the loop
    """
    while True:
        await asyncio.sleep(1)
        

def _run():
    global loop
    from app.models import Bot
    loop = asyncio.new_event_loop()
    loop.create_task(_check_dead())
    # Set it as the event loop for the current thread
    asyncio.set_event_loop(loop)
    # Start all bots
    for b in Bot.query.all():
        _start_bot(b.token, b.campaign, b.prefix)

    print("Running bot manager", color='green')
    try:
        loop.run_forever()
    finally:
        # Close the loop
        loop.close()
        print("Bot manager closed gracefully", color='green')    
        

def _start_bot(token, campaign, prefix):
    from app.system import manager
    # Bot is already running
    if token in bots:
        print(campaign.name, "is already running")
        return

    from app.discord_bot.bot import DiscordBot
    discord_bot = DiscordBot(campaign, command_prefix=prefix)

    @discord_bot.event
    async def on_ready():
        msg = 'Bot "{}" logged in as {} ({})'.format(
            campaign.name,
            discord_bot.user.name,
            discord_bot.user.id)
        print(msg, color='blue')
            
    for cmd in manager.get_commands(campaign.system):
        discord_bot.add_command(cmd)
    loop.create_task(discord_bot.start(token))
    bots[token] = discord_bot


async def _stop_bot(token, campaign):
    discord_bot = bots[token]
    await discord_bot.close()
    msg = 'Bot "{}" stopped'.format(
        campaign.name,
        )
    print(msg, color='blue')
    del bots[token]


def stop_bot(bot):
    if is_running(bot):
        loop.create_task(_stop_bot(bot.token, bot.campaign))


async def _restart_bot(token, campaign, prefix):
    if token in bots:
        await _stop_bot(token, campaign)
    _start_bot(token, campaign, prefix)


def restart_bot(bot):
    loop.create_task(_restart_bot(bot.token, bot.campaign, bot.prefix))


async def _send_msg(bot, msg, channel_id=None):
    discord_bot = bots[bot.token]
    if channel_id is None:
        channels = [c for c in discord_bot.get_all_channels() if type(c) == discord.TextChannel]
    else:
        channels = [discord_bot.get_channel(channel_id)]

    for c in channels:
        await c.send(msg)


def send_msg(bot, msg, channel_id=None):
    loop.create_task(_send_msg(bot, msg, channel_id))


def _get_voice(discord_bot, channel_id):
    voice = None
    for client in discord_bot.voice_clients:
        if client.channel.id == channel_id:
            voice = client
            break
    return voice


async def _get_or_create_voice(bot, channel_id):
    discord_bot = bots[bot.token]
    voice = _get_voice(discord_bot, channel_id)
    if voice is None:
        channel = discord_bot.get_channel(channel_id)
        voice = await channel.connect()
    return voice


async def _play_audio(bot, file, channel_id):
    voice = await _get_or_create_voice(bot, channel_id)
    if voice.is_playing():
        voice.stop()
    # Add the FFmpeg source wrapped in a PCMVolumeTransformer so we can control the volume
    source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(file), volume=bot.volume)
    voice.play(source)


def play_audio(bot, file, channel_id):
    loop.create_task(_play_audio(bot, file, channel_id))


async def _toggle_pause_audio(bot, channel_id):
    voice = await _get_or_create_voice(bot, channel_id)
    if voice.is_paused():
        voice.resume()
    else:
        voice.pause()


def toggle_pause_audio(bot, channel_id):
    loop.create_task(_toggle_pause_audio(bot, channel_id))


async def _stop_audio(bot, channel_id):
    voice = await _get_or_create_voice(bot, channel_id)
    voice.stop()


def stop_audio(bot, channel_id):
    loop.create_task(_stop_audio(bot, channel_id))


def volume_audio(bot, channel_id, volume=None, inc=None):
    discord_bot = bots[bot.token]
    voice = _get_voice(discord_bot, channel_id)
    if voice is None:
        return None
    if volume is not None:
        voice.source.volume = volume
    elif inc is not None:
        voice.source.volume += inc
    return voice.source.volume


def stop():
    if loop is not None:
        loop.stop()
