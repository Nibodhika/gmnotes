from discord.ext import commands


class DiscordBot(commands.Bot):

    def __init__(self, campaign, *args, **kwargs):
        super(DiscordBot, self).__init__(*args, **kwargs)
        self.campaign = campaign

