import pytest
import random


def test_roll_nds(mocker):
    """
    Tests that roll_nds function is working properly.

    :param mocker:
    :return:
    """
    from app.discord_bot.default_commands import roll_nds
    from unittest.mock import call

    randint = mocker.patch.object(random, 'randint', side_effect=[2])

    assert roll_nds('1d6') == [([2], 0)]
    randint.assert_called_once_with(1, 6)
    randint.reset_mock()

    randint.side_effect = [1, 2, 3]
    assert roll_nds('3d10') == [([1, 2, 3], 0)]
    assert randint.call_args_list == [call(1, 10)]*3
    randint.reset_mock()

    randint.side_effect = [4, 5, 6]
    assert roll_nds('1d4 2d6') == [([4], 0), ([5, 6], 0)]
    assert randint.call_args_list == [call(1, 4), call(1, 6), call(1, 6)]
    randint.reset_mock()

    # invalid values, but setting randint to return something so the error is more intuitive
    randint.side_effect = [5]
    assert roll_nds('wrong') == []
    assert roll_nds('wrong1d6') == []
    assert roll_nds('1d6wrong') == []
    assert roll_nds('wrong1d6wrong') == []

    # However valid nds in the middle of a sentence are valid
    randint.side_effect = [7, 8, 9]
    assert roll_nds('I would like 1d20 please') == [([7], 0)]
    randint.assert_called_once_with(1, 20)
    randint.reset_mock()

    # Rolling without the x should default to 1
    assert roll_nds('d8') == [([8], 0)]
    randint.assert_called_once_with(1, 8)
    randint.reset_mock()

    # However the number of sides is required
    assert roll_nds('1d') == []

    # Default in a phrase
    assert roll_nds('roll d10') == [([9], 0)]
    randint.assert_called_once_with(1, 10)
    randint.reset_mock()

    # Additional plus parameter
    randint.side_effect = [6, 3, 4]
    assert roll_nds('3d6+2') == [([6, 3, 4], 2)]
    assert randint.call_args_list == [call(1, 6)] * 3
    randint.reset_mock()

    # Don't consider it if there's a space
    randint.side_effect = [2]
    assert roll_nds('1d6 +3') == [([2], 0)]
    randint.assert_called_once_with(1, 6)
    randint.reset_mock()

    # Other word delimiters should not produce a plus argument
    randint.side_effect = [3]
    assert roll_nds('1d6*3') == [([3], 0)]
    randint.assert_called_once_with(1, 6)
    randint.reset_mock()

    # All together now
    randint.side_effect = [5, 4, 2, 1]
    assert roll_nds('roll d10 not3d5 2d6+3 d2 + 4') == [([5], 0), ([4, 2], 3), ([1], 0)]
    assert randint.call_args_list == [call(1, 10), call(1, 6), call(1, 6), call(1, 2)]
    randint.reset_mock()


@pytest.mark.asyncio
async def test_roll_cmd(mocker):
    """
    Tests that the roll_cmd is working properly.
    Note that it mocks the roll_nds function, which is tested separately.

    :param mocker:
    :return:
    """
    from app.discord_bot import default_commands

    roll_nds = mocker.patch.object(default_commands, 'roll_nds', return_value=[])
    ctx = mocker.Mock()
    sent = []

    async def mocked_send(value):
        sent.append(value)

    ctx.send = mocked_send

    # Empty values for arg are acceptable
    await default_commands.roll.callback(ctx)
    roll_nds.assert_called_once_with('')
    assert sent == ['Not a valid roll']
    roll_nds.reset_mock()
    sent = []

    # Whatever is sent is passed directly to roll_nds
    await default_commands.roll.callback(ctx, arg='not a roll')
    roll_nds.assert_called_once_with('not a roll')
    assert sent == ['Not a valid roll']
    roll_nds.reset_mock()
    sent = []

    # A single roll result
    roll_nds.return_value = [([2], 0)]
    await default_commands.roll.callback(ctx)
    assert sent == ['Result: 2 (2)']
    roll_nds.reset_mock()
    sent = []

    # Results with plus arguments
    roll_nds.return_value = [([5, 6], 2)]
    await default_commands.roll.callback(ctx)
    assert sent == ['Result: 5, 6 (13)']
    roll_nds.reset_mock()
    sent = []

    # Multiple rolls results
    roll_nds.return_value = [([1, 2], 0), ([3], 2)]
    await default_commands.roll.callback(ctx)
    assert sent == ['Result: 1, 2 (3)\nResult: 3 (5)']
    roll_nds.reset_mock()
    sent = []


def test_default_command_list():
    from app.discord_bot.default_commands import cmd_list, roll, player
    assert cmd_list == [
        roll,
        player
    ]
