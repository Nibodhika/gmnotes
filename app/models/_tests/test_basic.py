import pytest
import sqlalchemy

from app.models import *


def test_system_sql_mixin():
    """
    Tests the SystemMetaMixin is installed and working
    """
    from app.models import db

    # Check that the tables on app.models have the correct name
    assert Campaign.__tablename__ == "campaign"
    assert Character.__tablename__ == "character"
    assert GameSession.__tablename__ == "game_session"
    assert Location.__tablename__ == "location"
    assert GameObject.__tablename__ == "game_object"

    # Check that we're generating the correct name on regular situations
    class TestModel(db.Model):
        id = db.Column(db.Integer, primary_key=True)

    assert TestModel.__tablename__ == 'test_model'

    # Check that if __tablename__ is set it's used
    class WithTableName(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        __tablename__ = 'use_this'

    assert WithTableName.__tablename__ == 'use_this'

    # When set directly from the __system__
    class ModelWithSystem(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        __system__ = 'system'

    assert ModelWithSystem.__tablename__ == 'system_model_with_system'

    # When setting system and tablename it should use both
    class SystemAndTableName(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        __system__ = 'system2'
        __tablename__ = 'model_name'

    assert SystemAndTableName.__tablename__ == 'system2_model_name'

    # Via inheritance from an abstract model
    class AbstractSystemModel(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        __abstract__ = True
        __system__ = 'system3'

    class InheritsFromSystem(AbstractSystemModel):
        pass

    assert InheritsFromSystem.__tablename__ == 'system3_inherits_from_system'

    # Via mixin
    class TestMixin(object):
        __system__ = 'system4'

    class WithMixin(TestMixin, db.Model):
        id = db.Column(db.Integer, primary_key=True)
    assert WithMixin.__tablename__ == 'system4_with_mixin'


def test_generic_game_fixture(generic_game):
    """
    Tests that the generic_game fixture is working correctly
    """
    assert generic_game.name == 'foo'
    assert generic_game.system == 'generic'
    assert len(generic_game.sessions) == 2
    assert len(generic_game.characters) == 3
    assert len(generic_game.locations) == 2
    assert len(generic_game.objects) == 2


def test_clean_db(session):
    """
    Ensures the fixtures are working correctly.
    The previous test created a campaign, it should not exist in this test.
    """
    assert len(Campaign.query.all()) == 0
    assert len(GameSession.query.all()) == 0
    assert len(Player.query.all()) == 0
    assert len(Character.query.all()) == 0
    assert len(Location.query.all()) == 0
    assert len(GameObject.query.all()) == 0


@pytest.mark.parametrize("model_tested", [
    ('locations', Location),
    ('objects', GameObject),
    ('characters', Character),
])
def test_unique_constraint(session, generic_game, model_tested):
    """
    Test the unique constraint of several models.
    Gets the first element of the list of backref provided
    Creates a new instace with the same name using the constructor provided
    Asserts attempting to commit it fails
    Then checks that the same name can be used on a different campaign
    """
    campaign = Campaign(name='foo2', system='generic', gm=generic_game.gm)
    session.add(campaign)
    session.commit()
    assert campaign.id is not None
    campaign_id = campaign.id

    backref_name, constructor = model_tested
    backref_list = sqlalchemy.inspect(generic_game).attrs[backref_name].value
    object_to_mimic = backref_list[0]

    used_name = object_to_mimic.name
    # Creating an object with same name and campaign as the one that already exists should fail
    new_elem = constructor(name=used_name, campaign=generic_game)

    session.add(new_elem)
    with pytest.raises(sqlalchemy.exc.IntegrityError):
        session.commit()

    assert new_elem.id is None
    session.rollback()

    # Saving the same name on a different campaign should work
    new_elem2 = constructor(name=used_name, campaign_id=campaign_id)
    session.add(new_elem2)
    session.commit()
    assert new_elem2.id is not None


