from datetime import timedelta, datetime

from app.models import Campaign, GameSession, Player


def test_game_session_order(session):
    gm, _ = Player.create_player(name='gm')
    campaign = Campaign(name='campaign test', gm=gm)
    session.add(campaign)

    last_week = datetime.utcnow() - timedelta(days=7, hours=1)
    session_1 = GameSession(name='Session 1', campaign=campaign, date=last_week)
    session.add(session_1)

    session_2 = GameSession(name='Session 2', campaign=campaign, date=datetime.utcnow())
    session.add(session_2)

    two_weeks = datetime.utcnow() - timedelta(days=14, hours=1)
    session_0 = GameSession(name='Session 0', campaign=campaign, date=two_weeks)
    session.add(session_0)

    session.commit()

    # Expect them to be ordered by date and not id
    assert campaign.sessions == [session_2, session_1, session_0]


def test_game_session_safe_name(session):
    from app.config import config

    gm, _ = Player.create_player(name='gm')
    campaign = Campaign(name='campaign test', gm=gm)
    session.add(campaign)

    session_1 = GameSession(campaign=campaign)
    session.add(session_1)

    session.commit()

    original_format = config.DATE_FORMAT
    try:
        assert session_1.name is None
        assert session_1.safe_name == session_1.date.strftime(config.DATE_FORMAT)

        # Change the format and ensure it's respecting it
        config.DATE_FORMAT = "%d/%m/%Y"
        assert config.DATE_FORMAT != original_format
        assert session_1.safe_name == session_1.date.strftime(config.DATE_FORMAT)

        # Sessions with name should use the name instead
        session_1.name = 'a name'
        assert session_1.safe_name == session_1.name
    finally:
        config.DATE_FORMAT = original_format
