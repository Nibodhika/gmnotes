def test_object_access_level(session, generic_game):
    """
    Test the behavior of GameObject.access_level
    """
    from app.models import AccessLevel, Player, GameObject

    player = Player.query.get(2)
    assert generic_game.gm != player
    assert player in generic_game.players
    # Get a player not in campaign
    not_in_campaign = [p for p in Player.query.all() if p not in generic_game.players and p != generic_game.gm][0]

    player_character = generic_game.get_character(player)
    obj = GameObject.query.get(1)
    obj.owner = player_character
    session.add(obj)
    session.commit()

    # Private object with owner
    assert obj.private is True
    assert obj.access_level(generic_game.gm) is AccessLevel.GM
    assert obj.access_level(player) is AccessLevel.OWNER
    assert obj.access_level(not_in_campaign) is AccessLevel.NONE

    # Public object with owner
    obj.private = False
    session.add(obj)
    session.commit()
    assert obj.access_level(generic_game.gm) is AccessLevel.GM
    assert obj.access_level(player) is AccessLevel.OWNER
    assert obj.access_level(not_in_campaign) is AccessLevel.NONE

    other_objs = [o for o in generic_game.objects if o != obj]

    other_obj = other_objs[0]
    # Private object without owner
    assert other_obj.private is True
    assert other_obj.owner is None
    assert other_obj.access_level(generic_game.gm) is AccessLevel.GM
    assert other_obj.access_level(player) is AccessLevel.NONE
    assert other_obj.access_level(not_in_campaign) is AccessLevel.NONE

    # Make it public
    other_obj.private = False
    session.add(other_obj)
    session.commit()

    # Public object without owner
    assert other_obj.private is False
    assert other_obj.access_level(generic_game.gm) is AccessLevel.GM
    assert other_obj.access_level(player) is AccessLevel.VIEW
    assert other_obj.access_level(not_in_campaign) is AccessLevel.NONE
