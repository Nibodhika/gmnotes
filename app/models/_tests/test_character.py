def test_character_access_level(session, generic_game):
    """
    Test the behavior of Character.access_level
    """
    from app.models import AccessLevel, Player

    player = Player.query.get(2)
    # Create a player not in the campaign
    not_in_campaign = [p for p in Player.query.all() if p not in generic_game.players and p != generic_game.gm][0]

    assert generic_game.gm != player
    assert player in generic_game.players

    player_character = generic_game.get_character(player)

    # Private Character with player
    assert player_character.private is True
    assert player_character.access_level(generic_game.gm) is AccessLevel.GM
    # Default is that the owner does not have edit access
    assert player_character.access_level(player) is (AccessLevel.OWNER - AccessLevel.EDIT)
    # However if the campaign allows to edit it should allow it
    generic_game.allow_player_edit = True
    assert player_character.access_level(player) is AccessLevel.OWNER
    generic_game.allow_player_edit = False
    assert player_character.access_level(not_in_campaign) is AccessLevel.NONE

    player_character.private = False
    session.add(player_character)
    session.commit()

    # Public character with player
    assert player_character.access_level(generic_game.gm) is AccessLevel.GM
    assert player_character.access_level(player) is (AccessLevel.OWNER - AccessLevel.EDIT)
    # However if the campaign allows to edit it should allow it
    generic_game.allow_player_edit = True
    assert player_character.access_level(player) is AccessLevel.OWNER
    generic_game.allow_player_edit = False
    assert player_character.access_level(not_in_campaign) is AccessLevel.NONE

    other_characters = [c for c in generic_game.characters if c != player_character and c.player is None]
    other_char = other_characters[0]

    # Private NPC
    assert other_char.private is True
    assert other_char.access_level(generic_game.gm) is AccessLevel.GM
    assert other_char.access_level(player) is AccessLevel.NONE
    assert other_char.access_level(not_in_campaign) is AccessLevel.NONE

    # Make him public
    other_char.private = False
    session.add(other_char)
    session.commit()

    # Public NPC
    assert other_char.private is False
    assert other_char.access_level(generic_game.gm) is AccessLevel.GM
    assert other_char.access_level(player) is AccessLevel.VIEW
    assert other_char.access_level(not_in_campaign) is AccessLevel.NONE
