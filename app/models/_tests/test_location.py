def test_location_access_level(session, generic_game):
    """
    Test the behavior of GameObject.access_level
    """
    from app.models import AccessLevel, Player, Location

    player = Player.query.get(2)
    assert generic_game.gm != player
    assert player in generic_game.players
    # Get a player not in campaign
    not_in_campaign = [p for p in Player.query.all() if p not in generic_game.players and p != generic_game.gm][0]

    player_character = generic_game.get_character(player)
    location = Location.query.get(1)
    location.owner = player_character
    session.add(location)
    session.commit()

    # Private Location with owner
    assert location.private is True
    assert location.access_level(generic_game.gm) is AccessLevel.GM
    assert location.access_level(player) is AccessLevel.OWNER
    assert location.access_level(not_in_campaign) is AccessLevel.NONE

    # Public Location with owner
    location.private = False
    session.add(location)
    session.commit()
    assert location.access_level(generic_game.gm) is AccessLevel.GM
    assert location.access_level(player) is AccessLevel.OWNER
    assert location.access_level(not_in_campaign) is AccessLevel.NONE

    other_locations = [o for o in generic_game.locations if o != location]

    other_location = other_locations[0]
    # Private Location without owner
    assert other_location.private is True
    assert other_location.owner is None
    assert other_location.access_level(generic_game.gm) is AccessLevel.GM
    assert other_location.access_level(player) is AccessLevel.NONE
    assert other_location.access_level(not_in_campaign) is AccessLevel.NONE

    # Make it public
    other_location.private = False
    session.add(other_location)
    session.commit()

    # Public Location without owner
    assert other_location.private is False
    assert other_location.access_level(generic_game.gm) is AccessLevel.GM
    assert other_location.access_level(player) is AccessLevel.VIEW
    assert other_location.access_level(not_in_campaign) is AccessLevel.NONE
