from .commons import GenericMixin, db, build_fk, AccessLevel


class Location(GenericMixin,db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)
    description = db.Column(db.Text, nullable=True)

    owner_id, owner = build_fk('character.id', 'Character', 'locations', nullable=True)

    # Variables to control the view that only the GM and owner have access to
    private = db.Column(db.Boolean, nullable=False, default=True)
    private_description = db.Column(db.Text)

    # GM Notes
    notes = db.Column(db.Text, nullable=True)

    campaign_id, campaign = build_fk('campaign.id', 'Campaign', 'locations')

    __table_args__ = (
        db.UniqueConstraint('name', 'campaign_id'),
    )

    def __repr__(self):
        return '<Location {}>'.format(self.name)

    def get_owner_name(self):
        if self.owner is not None:
            return self.owner.name
        return None

    def access_level(self, player):
        """
        Returns the access level a given player has to this Location

        :param player:
        :return:
        """
        if player == self.campaign.gm:
            return AccessLevel.GM
        elif self.owner and player == self.owner.player:
            return AccessLevel.OWNER
        elif self.private:
            return AccessLevel.NONE
        elif player in self.campaign.players:
            return AccessLevel.VIEW
        return AccessLevel.NONE
