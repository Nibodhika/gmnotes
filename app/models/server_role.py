"""
These models are used for the administration of the website and not for the game.
No need to extend them on systems.
"""
from flask_security import RoleMixin

from app.models.commons import db


roles_player = db.Table('roles_player',
                        db.Column('player_id', db.Integer(), db.ForeignKey('player.id')),
                        db.Column('role_id', db.Integer(), db.ForeignKey('server_role.id'))
                        )


class ServerRole(db.Model, RoleMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    users = db.relationship('Player',
                            secondary=roles_player,
                            backref=db.backref('roles', lazy=True)
                            )

    def __repr__(self):
        return '<Role {}>'.format(self.name)
