from datetime import date

from .commons import db, build_fk, AccessLevel


class GameSession(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=False, nullable=True)
    date = db.Column(db.Date, default=date.today(), nullable=False)
    description = db.Column(db.Text, nullable=True)

    notes = db.Column(db.Text, nullable=True)

    campaign_id, campaign = build_fk('campaign.id', 'Campaign', 'sessions', order_by='desc(GameSession.date)')

    @property
    def safe_name(self):
        """
        Whenever we allow name to be nullable we need to implement a safe_name that should return an identifiable name

        :return:
        """
        from app.config import config
        if self.name:
            name = self.name
        else:
            name = self.date.strftime(config.DATE_FORMAT)

        return name

    def __repr__(self):
        return '<Session {}>'.format(self.safe_name)

    def access_level(self, player):
        """
        Returns the access level a given player has to this Session

        :param player:
        :return:
        """
        if player == self.campaign.gm:
            return AccessLevel.GM
        elif player in self.campaign.players:
            return AccessLevel.VIEW
        return AccessLevel.NONE
