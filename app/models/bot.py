import os

from .commons import db, build_fk

AUDIO_FOLDER = 'audio'


class Bot(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    campaign_id, campaign = build_fk('campaign.id', 'Campaign', 'bots', unique=True)
    audio_channel = db.Column(db.Integer, nullable=True)
    _volume = db.Column(db.Float, nullable=False, default=1)
    # My Discord tokens have 59 chars, I'm assuming all of them do
    token = db.Column(db.String(60), unique=True, nullable=False)
    prefix = db.Column(db.String(3), default="!")

    def __repr__(self):
        return '<Bot {}>'.format(self.campaign.name)

    def send(self, message):
        from app.discord_bot import manager
        manager.send_msg(self, message, self.audio_channel)

    def _audio_folder(self):
        return os.path.join(AUDIO_FOLDER, str(self.campaign_id))

    def get_audio_files(self):
        audio_files = {}
        audio_folder = self._audio_folder()
        for category in os.scandir(audio_folder):
            if not category.is_dir():
                from colors import cprint
                cprint("Ignoring {}. Only folders inside {} are considered".format(
                    category.name, audio_folder), color='red')
                continue

            category_dict = {}
            for audio in os.scandir(category):
                name = os.path.splitext(audio.name)[0]
                category_dict[name] = audio.path
            audio_files[category.name] = category_dict
        return audio_files

    def play(self, category, file):
        from app.discord_bot import manager
        audio_files = self.get_audio_files()
        if category not in audio_files:
            return False
        category_dict = audio_files[category]
        if file not in category_dict:
            return False
        file_path = category_dict[file]
        manager.play_audio(self, file_path, self.audio_channel)

    def pause(self):
        from app.discord_bot import manager
        manager.toggle_pause_audio(self, self.audio_channel)

    def stop(self):
        from app.discord_bot import manager
        manager.stop_audio(self, self.audio_channel)

    @property
    def volume(self):
        return self._volume

    @volume.setter
    def volume(self, value):
        from app.discord_bot import manager
        # Ensure 0-1 range
        value = max(min(value, 1), 0)
        self._volume = value
        manager.volume_audio(self, self.audio_channel, value)

        try:
            db.session.add(self)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
        return self._volume
