from .commons import GenericMixin, db, build_fk, AccessLevel


class Character(GenericMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)

    is_alive = db.Column(db.Boolean, nullable=False, default=True)
    description = db.Column(db.Text, nullable=True)
    bio = db.Column(db.Text, nullable=True)

    # Variables to control the view that only the GM and player has access to
    private = db.Column(db.Boolean, nullable=False, default=True)
    private_description = db.Column(db.Text)
    private_bio = db.Column(db.Text)

    # GM Notes
    notes = db.Column(db.Text, nullable=True)

    # A character can have an actual player (i.e. someone registered in the site)
    player_id, player = build_fk('player.id', 'Player', 'characters', nullable=True)
    # Or a dummy player, just a name
    dummy_player = db.Column(db.String(80), nullable=True)

    campaign_id, campaign = build_fk('campaign.id', 'Campaign', 'characters')

    __table_args__ = (
        db.UniqueConstraint('name', 'campaign_id'),
        db.UniqueConstraint('player_id', 'campaign_id'),
    )

    def __repr__(self):
        return '<Character %r (%r)>' % (self.name, self.id)
    
    @property
    def is_npc(self):
        return self.player is None and not self.dummy_player

    def access_level(self, player):
        """
        Returns the access level a given player has to this character

        :param player:
        :return:
        """
        if player == self.campaign.gm:
            return AccessLevel.GM
        elif player == self.player:
            if self.campaign.allow_player_edit:
                return AccessLevel.OWNER
            else:
                return AccessLevel.OWNER - AccessLevel.EDIT
        elif self.private:
            return AccessLevel.NONE
        elif player in self.campaign.players:
            return AccessLevel.VIEW
        return AccessLevel.NONE

    @property
    def player_name(self):
        if self.player is not None:
            return self.player.name
        elif self.dummy_player:
            return self.dummy_player
        return None
