import enum
from flask_sqlalchemy import SQLAlchemy

from flask_sqlalchemy.model import NameMetaMixin, BindMetaMixin, Model, should_set_tablename, camel_to_snake_case
from sqlalchemy.ext.declarative import DeclarativeMeta, declarative_base


class SystemMetaMixin(NameMetaMixin):
    """
    This Mixin will overwrite the default mixin used to get the table name
    and allow for it to add a system name in front.

    Note that it will create a __tablename__ attribute before calling the NameMetaMixin,
    but it should deal correctly with creating it when needing
    """
    def __init__(self, name, bases, d):
        # Checking if abstract
        abstract = self.__dict__.get('__abstract__', False)
        if not abstract:
            # If a class specifies __system__ use that
            system_name = self.__dict__.get('__system__', None)
            # Otherwise see if one of its parents did
            if system_name is None:
                for base in self.__mro__:
                    if '__system__' not in base.__dict__:
                        continue
                    system_name = base.__system__

            # If there is a system create the table name
            if system_name is not None:
                # If NameMetaMixin would have created it get the name it would have given it,
                # otherwise get the __tablename__ attribute
                if should_set_tablename(self):
                    table_name = camel_to_snake_case(self.__name__)
                else:
                    table_name = self.__tablename__
                # Prepend the system name, and set the __tablename__ to ensure NameMetaMixin uses it
                new_tablename = system_name + '_' + table_name
                self.__tablename__ = new_tablename

        super(SystemMetaMixin, self).__init__(name, bases, d)


class MySystemMeta(SystemMetaMixin, BindMetaMixin, DeclarativeMeta):
    pass


db = SQLAlchemy(
    model_class=declarative_base(
        cls=Model,
        metaclass=MySystemMeta,
        name='Model'
    ),
    session_options={"autoflush": False}
    )


class GenericMixin(object):
    type = db.Column(db.String(50))
    __mapper_args__ = {
        'polymorphic_identity': 'generic',
        'polymorphic_on': type
    }


def build_fk(ref_field, backref_model, backref_name, nullable=False, unique=False, order_by=False, cascade='all'):
    """
    Helper function to build a foreign key, returns the field for the id and for the relationship.
    Assumes the id is an integer

    :param ref_field:
    :param backref_model:
    :param backref_name:
    :param nullable:
    :param unique:
    :param order_by: Note that build_fk is for one-to-many, so the order_by is passed to the backref
    :param cascade: Note that build_fk is for one-to-many, so the cascade is passed to the backref.
    You can set to False to set to null instead of delete
    :return:
    """

    fk_id = db.Column(db.Integer,
                      db.ForeignKey(ref_field),
                      nullable=nullable,
                      unique=unique,
                      )

    fk = db.relationship(backref_model,
                         backref=db.backref(backref_name, lazy=True, order_by=order_by, cascade=cascade),
                         foreign_keys=[fk_id]
                         )

    return fk_id, fk


class AccessLevel(enum.IntEnum):
    NONE = 0
    VIEW = 1
    VIEW_PRIVATE = 2  # Eventually maybe allow access to view the private attributes without being the owner
    EDIT = 4
    OWNER = EDIT+VIEW_PRIVATE+VIEW  # All previous values
    _GM = 16  # Not to be used, just to leave explicit the value
    GM = OWNER+_GM  # All previous plus a new one
