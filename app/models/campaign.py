from .commons import db, build_fk


class Campaign(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    system = db.Column(db.String(20), nullable=False, default='generic')
    description = db.Column(db.Text, nullable=True)
    allow_player_edit = db.Column(db.Boolean, default=False, nullable=False,
                                  info={'label': 'Allow player to edit characters'})
    gm_id, gm = build_fk('player.id', 'Player', 'narrating')

    def __repr__(self):
        return '<Campaign {}>'.format(self.name)

    def is_valid(self):
        from app.system import manager
        return self.system in manager.systems

    @property
    def system_long(self):
        from app.system import manager
        # Default to the value stored as system if we don't know it
        system_long = self.system
        if self.system in manager.systems:
            system_long = manager.systems[self.system].long_name
        return system_long

    @property
    def players(self):
        return [c.player for c in self.characters if c.player is not None]

    def get_character(self, player):
        from app.models import Character
        return Character.query.filter_by(campaign=self, player=player).first()

    @property
    def as_dict(self):
        """

        :return: Object data in serializable format
        """
        
        return {
            'id': self.id,
            'name': self.name,
            'system': self.system,
            'system_long': self.system_long,
            'description': self.description,
            'allow_player_edit': self.allow_player_edit,
            'gm_id': self.gm_id
        }
