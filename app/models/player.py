import random
import string
from flask_security import UserMixin

from .commons import db, AccessLevel


def random_password(length=8):
    """
    Generates a random string of length, used to initialize the password of new player.
    
    Note this uses random.choice and is not cryptographically secure, but it's meant to create the default password for the player.
    """
    return ''.join(random.choice(string.ascii_letters + string.digits) for _ in range(length))


class Player(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    email = db.Column(db.String(80), unique=True, nullable=True)
    active = db.Column(db.Boolean, default=True)
    password = db.Column(db.String(100), nullable=False)
    discord_id = db.Column(db.String(20), unique=True, nullable=True)

    def __init__(self, *args, **kwargs):
        """
        flask-security sends an email:'' in the constructor, if we don't transform it to None it only allows one
        user to be registered without email.

        It's likely that the same fix will be needed for discord_id in the future.

        :param args:
        :param kwargs:
        """
        if 'email' in kwargs:
            if not kwargs['email']:
                kwargs['email'] = None
        super(Player, self).__init__(*args, **kwargs)

    def __repr__(self):
        return '<Player {}>'.format(self.name)

    def access_level(self, character):
        if self in [character.campaign.gm, character.player]:
            return AccessLevel.OWNER
        elif character.private:
            return AccessLevel.NONE
        elif self in character.campaign.players:
            return AccessLevel.VIEW
        return AccessLevel.NONE

    @classmethod
    def create_player(cls, name, discord_id=None, password=None):
        """
        Creates an player given at least a name.
        If no password is given generates a new one.

        This is the preferred way to create a new player to ensure the password is being stored as a salted hash

        :param name: Name of the player
        :param discord_id: discord id
        :param password:
        :return: tuple( Player, str)
        """
        from flask_security.utils import hash_password
        created_password = None
        if password is None:
            created_password = random_password()
            password = created_password

        password = hash_password(password)
        player = cls(name=name,
                   discord_id=discord_id,
                   password=password)

        db.session.add(player)
        db.session.commit()
        
        return player, created_password

    def authenticate(self, password):
        from flask_security.utils import verify_password
        return verify_password(self.password, password)

    @property
    def campaigns(self):
        out = set(self.narrating)
        for c in self.characters:
            out.add(c.campaign)
        return out

    @property
    def is_admin(self):
        return self.has_role('admin')

    @is_admin.setter
    def is_admin(self, value):
        from app.models import ServerRole
        if value == self.is_admin:
            return
        admin_role = ServerRole.query.filter_by(name='admin').first()
        if value:
            self.roles.append(admin_role)
        else:
            self.roles.remove(admin_role)

        db.session.add(self)
        db.session.commit()
