from werkzeug.routing import BaseConverter
from flask_login import current_user

from app.models import *


class AuthenticatedFromIdConverter(BaseConverter):
    """
    Basic converter that works as base for all converters that deal with elements with an int id
    """

    table = None

    def to_python(self, value):
        try:
            id = int(value)
        except ValueError:
            return None
        elem = None
        if current_user.is_authenticated:
            elem = self.table.query.get(id)
        return elem

    def to_url(self, value):
        return str(value.id)


class CampaignConverter(AuthenticatedFromIdConverter):
    table = Campaign


class CharacterConverter(AuthenticatedFromIdConverter):
    table = Character


class PlayerConverter(AuthenticatedFromIdConverter):
    table = Player


class LocationConverter(AuthenticatedFromIdConverter):
    table = Location


class GameObjectConverter(AuthenticatedFromIdConverter):
    table = GameObject


class GameSessionConverter(AuthenticatedFromIdConverter):
    table = GameSession


converters = {
    'campaign': CampaignConverter,
    'character': CharacterConverter,
    'player': PlayerConverter,
    'location': LocationConverter,
    'game_object': GameObjectConverter,
    'game_session': GameSessionConverter,
}
