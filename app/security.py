from flask_security import SQLAlchemyUserDatastore, Security, RegisterForm, LoginForm
from flask_security.forms import email_validator, unique_user_email
from wtforms import StringField
from wtforms.validators import InputRequired, ValidationError

from app.models import db, Player, ServerRole

user_datastore = SQLAlchemyUserDatastore(db, Player, ServerRole)
security = None


class ExtendedLoginForm(LoginForm):
    email = StringField('Username or Email Address', [InputRequired()])


class ExtendedRegisterForm(RegisterForm):
    email = StringField('Email Address', [unique_user_email])
    name = StringField('Username', [InputRequired()])

    def validate_name(self, field):
        if Player.query.filter_by(name=field.data).count() != 0:
            raise ValidationError("Username already taken")

    def validate_email(self, field):
        # Allow empty
        if not field.data:
            return
        email_validator(self, field)


def init_app(app):
    global security

    security = Security(app=app,
                        datastore=user_datastore,
                        login_form=ExtendedLoginForm,
                        register_form=ExtendedRegisterForm,
                        )
