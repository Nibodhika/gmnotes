import os
import yaml
import re

from flask_wtf import FlaskForm
from wtforms import fields, validators


# Helper dict, re and function to convert a DATE_FORMAT intended for strftime into one for bootstrap-datetimepicker
date_conversion_dict = {
    '%Y':'yyyy',
    '%m': 'mm',
    '%d':'dd'
}
convert_date_format_re = re.compile("|".join(date_conversion_dict.keys()))


def convert_date_format(date_format):
    return convert_date_format_re.sub(lambda m: date_conversion_dict[re.escape(m.group(0))], date_format)


class _Config(object):
    # # Base # #
    SECURITY_REGISTERABLE = True
    _DATE_FORMAT = "%Y-%m-%d"  # there is a property without the underscore

    # # Intermediate # #
    SQLALCHEMY_DATABASE_URI = 'sqlite:///gmnotes.db'
    DEBUG = False

    # # Advanced # #
    MAX_TOKEN_AGE = 86400  # default is a day
    # You also need to delete secret_key and password_salt files respectively for these changes to take effect
    SECRET_KEY_LENGTH = 32
    # While this can be changed after the first run, notice that doing so will invalidate all passwords in the database
    SECURITY_PASSWORD_SALT_LENGTH = 16
    # Probably only worthy to set to False for development.
    # Not exposed on the settings menu, but should work if added to the config.yaml file
    MINIFY_PAGE = True

    # # Email Configuration # #
    MAIL_SERVER = 'localhost'
    MAIL_DEFAULT_SENDER = "gmnotes"
    SECURITY_CONFIRMABLE = False
    SECURITY_SEND_REGISTER_EMAIL = False
    SECURITY_SEND_PASSWORD_CHANGE_EMAIL = False
    SECURITY_SEND_PASSWORD_RESET_EMAIL = False
    SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = False

    # # Should not be messed with # #
    SECRET_KEY = None  # Will be loaded from secret_key file, or created and stored there
    SECURITY_PASSWORD_SALT = None  # Will be loaded from password_salt file, or created and stored there
    # This is used to allow login either by email or by name
    SECURITY_USER_IDENTITY_ATTRIBUTES = ['name', 'email']
    SQLALCHEMY_COMMIT_ON_TEARDOWN = False
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # These are added to be able to create hashed passwords outside of application context
    SECURITY_PASSWORD_SINGLE_HASH = False

    # Only set to True during tests
    TESTING = False
    # Used for the datepickers, will be autopopulated based on DATE_FORMAT
    DATEPICKER_FORMAT = "yyyy-mm-dd"

    # Do not load/store the secret key and salt on the config, they're stored on their own files
    # DATEPICKER_FORMAT is built from DATE_FORMAT when setting DATE_FORMAT
    ignore_attrs = ['SECRET_KEY', 'SECURITY_PASSWORD_SALT', 'DATEPICKER_FORMAT']

    def __init__(self, config_file="config.yaml", key_file='secret_key', password_salt_file='password_salt'):
        self._config_file = config_file
        self._key_file = key_file
        self._password_salt_file = password_salt_file
        self.load_config()
        self.load_or_create_key()
        self.load_or_create_password_salt()

    def load_config(self, config_file=None):
        if config_file is None:
            config_file = self._config_file
        try:
            with open(config_file, 'r') as stream:
                data = yaml.safe_load(stream)
            # Load the values
            self.load_from_dict(data)
        except IOError:
            # File does not exist, just use the defaults
            pass

    def load_from_dict(self, data):
        # First reload the default values, in case something is not defined in the dict but was changed before
        for k in self.__dict__:
            if k in _Config.__dict__ and k not in self.ignore_attrs:
                self.__dict__[k] = _Config.__dict__[k]
        if data is not None:
            # Remove non-existing keys
            filtered_data = {k: v for k, v in data.items() if k in _Config.__dict__}
            # Date format
            if 'DATE_FORMAT' in data:
                self.DATE_FORMAT = data['DATE_FORMAT']
            self.__dict__.update(filtered_data)

    def save_config(self, out_file=None):
        if out_file is None:
            out_file = self._config_file

        d = {}

        for k, v in self.__dict__.items():
            if k.startswith('_') or k in self.ignore_attrs:
                continue
            # No need to store attributes that still have their default value
            if _Config.__dict__[k] == v:
                continue
            d[k] = v

        with open(out_file, 'w') as yaml_file:
            # If the dictionary is empty do not store it, otherwise it will write {}\n to the file
            if d:
                yaml.dump(d, yaml_file, default_flow_style=False)

    def load_or_create_key(self):
        try:
            with open(self._key_file, 'rb') as ifs:
                self.SECRET_KEY = ifs.read()
        except IOError:
            self.SECRET_KEY = os.urandom(self.SECRET_KEY_LENGTH)
            with open(self._key_file, 'wb') as ofs:
                ofs.write(self.SECRET_KEY)

    def load_or_create_password_salt(self):
        try:
            with open(self._password_salt_file, 'r') as ifs:
                self.SECURITY_PASSWORD_SALT = ifs.read()
        except IOError:
            self.SECURITY_PASSWORD_SALT = os.urandom(self.SECURITY_PASSWORD_SALT_LENGTH).hex()
            with open(self._password_salt_file, 'w') as ofs:
                ofs.write(self.SECURITY_PASSWORD_SALT)

    @property
    def DATE_FORMAT(self):
        return self._DATE_FORMAT

    @DATE_FORMAT.setter
    def DATE_FORMAT(self, value):
        self.DATEPICKER_FORMAT = convert_date_format(value)
        self._DATE_FORMAT = value

config = _Config()


class ConfigForm(FlaskForm):
    SECURITY_REGISTERABLE = fields.BooleanField(
        label="Allow user registration",
    )
    DATE_FORMAT = fields.StringField(
        label="Date format"
    )
    DEBUG = fields.BooleanField(
        label="Debug",
    )
    SQLALCHEMY_DATABASE_URI = fields.StringField(
        label="Database URI",
        validators=[validators.DataRequired()]
    )
    MAX_TOKEN_AGE = fields.IntegerField(
        label="Maximum token age (in seconds)",
        validators=[validators.DataRequired()]
    )
    # Email configurations
    MAIL_SERVER = fields.StringField(
        label="E-Mail server",
        validators=[validators.DataRequired()]
    )
    MAIL_DEFAULT_SENDER = fields.StringField(
        label="E-Mail default sender",
        validators=[validators.DataRequired()]
    )
    SECURITY_CONFIRMABLE = fields.BooleanField(
        label="Send confirmation E-Mail",
    )
    SECURITY_SEND_REGISTER_EMAIL = fields.BooleanField(
        label="Send registration E-Mail",
    )
    SECURITY_SEND_PASSWORD_CHANGE_EMAIL = fields.BooleanField(
        label="Send password change E-Mail",
    )
    SECURITY_SEND_PASSWORD_RESET_EMAIL = fields.BooleanField(
        label="Send password reset E-Mail",
    )
    SECURITY_SEND_PASSWORD_RESET_NOTICE_EMAIL = fields.BooleanField(
        label="Send password reset notice E-Mail",
    )

    def __init__(self, *args, **kwargs):
        global config
        super(ConfigForm, self).__init__(*args, **kwargs)

    def load_config(self):
        for k in self.all_forms():
            self[k].data = getattr(config, k)

    def save_config(self):
        from flask import current_app
        for k in self.all_forms():
            setattr(config, k, self[k].data)
        config.save_config()
        current_app.config.from_object(config)

    def all_forms(self):
        attributes_to_ignore = ['meta', 'csrf_token']
        for k in self.__dict__:
            if k.startswith('_') or k in attributes_to_ignore:
                continue
            yield k
