import datetime


def filters(app):

    @app.template_filter()
    def date(date_obj):
        from app.config import config

        if not date_obj:
            date_obj = datetime.date.today()

        return date_obj.strftime(config.DATE_FORMAT)

    @app.template_filter()
    def cut(text, max_length=200):
        if len(text) > max_length:
            return text[:max_length]+'...'
        return text
