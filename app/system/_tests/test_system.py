import pytest


def test_system_manager_listdir(system_manager, mocker, tmp_path):
    """
    Checks that SystemManager is correctly calling import_system

    """

    # Create a systems folder to use for the imports
    d = tmp_path / "systems"
    d.mkdir()

    # Files starting with __ should be ignored
    init = d / "__whatever"
    init.write_text(" ")
    # Files with .py should call the import with the module name
    py_ext = d / "my_system.py"
    py_ext.write_text(" ")
    # Folders without .py should be called entirely
    dir_sys = d / "other_system"
    dir_sys.mkdir()

    app = system_manager.mock_app()
    mocker.patch.object(system_manager, 'import_system')
    system_manager.init_app(app, d)
    assert system_manager.import_system.call_count == 2
    
    # Since the order depends on the return of os.listdir it's better to check as a set
    call_set = set([ a[0] for a in system_manager.import_system.call_args_list])
    
    assert call_set == set([(str(py_ext),), (str(dir_sys),)])


def test_import_system(system_manager, tmp_path):
    # Create a systems folder to use for the imports
    d = tmp_path / "systems"
    d.mkdir()

    file_system = d / "file_system.py"
    file_system.write_text("""
from app.system import System
system = System('test1','A test system in a file')
""")

    # Attempting to call import before init should raise an exception
    with pytest.raises(RuntimeError):
        system_manager.import_system(str(file_system))

    # Pretend we ran the initialization
    system_manager.mock_init()
    # Sanity test
    assert system_manager.systems == {'generic': system_manager.generic}

    # Testing import of a single file
    assert system_manager.import_system(str(file_system))
    assert len(system_manager.systems) == 2
    assert "test1" in system_manager.systems

    # Testing import passing a module path
    module_system = d / "module_system"
    module_system.mkdir()
    module_init = module_system / "__init__.py"
    module_init.write_text("""
from app.system import System
system = System('test2','A test system in a module')
""")
    assert system_manager.import_system(str(module_system))
    assert len(system_manager.systems) == 3
    assert "test2" in system_manager.systems

    # Folders without a __init__.py should not be imported
    folder_without_init = d / "empty_folder"
    folder_without_init.mkdir()
    something = folder_without_init / "main.py"
    something.write_text("""
from app.system import System
system = System('test3','Not __init__.py file')
""")
    assert not system_manager.import_system(str(folder_without_init))
    assert len(system_manager.systems) == 3

    # Files without py extension should not import
    not_py_ext = d / "system.txt"
    not_py_ext.write_text("""
from app.system import System
system = System('test4','Not py extension')
""")
    assert not system_manager.import_system(str(not_py_ext))
    assert len(system_manager.systems) == 3

    # Files that named the system with another name would not import
    not_system = d / "not_system.py"
    not_system.write_text("""
from app.system import System
not_system = System('test4','Not py extension')
""")
    assert not system_manager.import_system(str(not_system))
    assert len(system_manager.systems) == 3


def test_system_manager_get_base_url(app):
    '''
    get_base_url should return /<system> for known systems
    and an empty string for unknown ones
    '''
    from app.system.manager import manager
    # v5 is registered in the default app
    assert manager.get_base_url('v5') == '/v5'
    # A system that does not exist should point to the generic system
    assert manager.get_base_url('wrong') == '/{}'.format(manager.generic.name)


def test_system_validates_name():
    from app.system.system import GenericSystem, System
    generic1 = GenericSystem('gen')
    # assert generic1.view.overview(campaign)
    sys1 = System('sys1')
    # assert generic1.view.overview(campaign)