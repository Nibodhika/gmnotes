import pytest


@pytest.fixture()
def base_system(request, app):
    """
    Installs a basic system with the same name as the test

    :param request:
    :param app:
    :return:
    """
    from app.system import System, manager

    system = System(request.node.name)
    system.init_app(app)
    manager.systems[system.name] = system

    yield system

    del manager.systems[system.name]
