from datetime import date, timedelta

from flask import url_for
from flask_login import current_user, login_user, logout_user


def test_campaign_system_check(app, generic_game):
    from app.system.system_blueprint import campaign_system_check

    access_string = "This should only be returned by my_function"

    @campaign_system_check(generic_game.system)
    def my_function(campaign):
        return access_string

    assert not current_user.is_authenticated
    assert my_function(generic_game) != access_string
    # Invalid values should not work
    assert my_function(None) != access_string
    assert my_function('not a campaign') != access_string

    # The GM should be able to see it
    gm = generic_game.gm
    login_user(gm)
    assert my_function(generic_game) == access_string
    # Invalid values should still not work
    assert my_function(None) != access_string
    assert my_function('not a campaign') != access_string

    # Reset and check
    logout_user()
    assert my_function(generic_game) != access_string

    # Logged in users that belong to the campaign should be able to see it
    player = generic_game.players[0]
    assert player is not gm  # Ensure we're testing a different player
    login_user(player)
    assert my_function(generic_game) == access_string

    # Reset and check
    logout_user()
    assert my_function(generic_game) != access_string

    # TODO test the other functionality (gm and token)


def test_system_urls(app, base_system, session, client, authentication_mock):
    from app.models import Player, Campaign, Character, Location, GameObject, GameSession

    # Let's create some data in the new system
    gm = Player(name="system test GM", password=" ")
    session.add(gm)
    player = Player(name="system test Player", password=" ")
    session.add(player)

    c = Campaign(name="system test", system=base_system.name, gm=gm)
    session.add(c)

    # Characters
    npc1 = Character(name="NPC1", campaign=c)
    session.add(npc1)
    pc1 = Character(name="PC1", campaign=c, player=player)
    session.add(pc1)
    # Used for deletion test
    npc2 = Character(name="NPC2", campaign=c)
    session.add(npc2)

    # Locations
    loc1 = Location(name="Location1", campaign=c)
    session.add(loc1)
    loc_with_owner = Location(name="Location with owner", campaign=c, owner=pc1)
    session.add(loc_with_owner)
    # Used for deletion test
    loc2 = Location(name="Location for delete", campaign=c)
    session.add(loc_with_owner)

    # Objects
    obj1 = GameObject(name="Object1", campaign=c)
    session.add(obj1)
    obj_with_owner = GameObject(name="Object with owner", campaign=c, owner=pc1)
    session.add(obj_with_owner)
    # Used for deletion test
    obj2 = GameObject(name="Object for delete", campaign=c)
    session.add(obj2)

    # Sessions
    last_week = date.today() - timedelta(days=7)
    session1 = GameSession(campaign=c, date=last_week)
    session.add(session1)
    session2 = GameSession(campaign=c)
    session.add(session2)

    session.commit()

    def system_url(*args):
        url = '/'.join(map(str, args))
        return "/{}/{}".format(base_system.name, url)

    # List of urls to test and whether they're exclusive to the GM
    base_urls = [
        (system_url(c.id), False),
        # List views
        (system_url(c.id, 'sessions'), False),
        (system_url(c.id, 'characters'), False),
        (system_url(c.id, 'locations'), False),
        (system_url(c.id, 'objects'), False),

        # Only GM can manage the bot
        (system_url(c.id, 'bot'), True),
        (system_url(c.id, 'del_bot'), True),
        (system_url(c.id, 'bot/play'), True),
        (system_url(c.id, 'bot/pause'), True),
        (system_url(c.id, 'bot/stop'), True),
        (system_url(c.id, 'bot/vol_up'), True),
        (system_url(c.id, 'bot/vol_down'), True),

        (system_url(c.id, 'character', pc1.id), False),
        # Only GM can create/delete characters
        (system_url(c.id, 'character', 'new'), True),
        (system_url(c.id, 'character', 'new_token', '1'), True),
        (system_url(c.id, 'character', npc2.id, 'del'), True),

        (system_url(c.id, 'location', obj_with_owner.id), False),
        # Only GM can create/delete locations
        (system_url(c.id, 'location', 'new'), True),
        (system_url(c.id, 'location', loc2.id, 'del'), True),

        (system_url(c.id, 'object', obj_with_owner.id), False),
        # Only GM can create/delete objects
        (system_url(c.id, 'object', 'new'), True),
        (system_url(c.id, 'object', obj2.id, 'del'), True),

        (system_url(c.id, 'session', session1.id), False),
        # Only GM can create/delete sessions
        (system_url(c.id, 'session', 'new'), True),
        (system_url(c.id, 'session', session2.id, 'del'), True),
    ]

    # Ensure we're testing all endpoints
    base_url = system_url()
    all_endpoints = [r for r in app.url_map.iter_rules() if r.rule.startswith(base_url)]
    assert len(all_endpoints) == len(base_urls)

    extra_urls = [
        # These are private, so only the GM should be able to access them
        (system_url(c.id, 'character', npc1.id), True),
        (system_url(c.id, 'location', loc1.id), True),
        (system_url(c.id, 'object', obj1.id), True),
    ]

    urls_to_test_gm = base_urls + extra_urls

    # Used to check if a webpage directed to the login page by following the redirect and comparing the data
    login_data = client.get("/login").data

    # When not authenticated we expect all of them to redirect to login
    assert not current_user.is_authenticated
    for url, _ in urls_to_test_gm:
        res = client.get(url)
        assert res.status_code == 302
        assert client.get(res.location).data == login_data

    # Maps the pages that are expected redirects to the page they're supposed to redirect to
    expected_redirects = {
        system_url(c.id, 'del_bot'): system_url(c.id),
        system_url(c.id, 'character', npc2.id, 'del'): system_url(c.id, 'characters'),
        system_url(c.id, 'location', loc2.id, 'del'): system_url(c.id, 'locations'),
        system_url(c.id, 'object', obj2.id, 'del'): system_url(c.id, 'objects'),
        system_url(c.id, 'session', session2.id, 'del'): system_url(c.id, 'sessions'),
    }

    # As the GM all should be valid
    authentication_mock.user = gm
    for url, _ in urls_to_test_gm:

        if url in expected_redirects:
            res = client.get(url)
            assert res.status_code == 302
            assert client.get(res.location).data == client.get(expected_redirects[url]).data
        else:
            assert client.get(url).status_code == 200

    # As a user some pages should not be accessible
    authentication_mock.user = player
    for url, gm_only in urls_to_test_gm:
        expected_code = 400 if gm_only else 200
        assert client.get(url).status_code == expected_code


def test_new_character_token(session, client, authentication_mock):
    from app.models import Player, Campaign
    gm = Player(name='the gm', password=' ')
    session.add(gm)
    player = Player(name='the player', password=' ')
    session.add(player)
    campaign = Campaign(name='the campaign', gm=gm)
    session.add(campaign)
    session.commit()

    authentication_mock.user = gm
    new_token_url = url_for('generic.new_character_create_token', campaign=campaign, player_id=player.id)
    # The API responds with the full url to create a new character
    new_char_with_token_url = client.get(new_token_url).data.decode('utf-8')

    authentication_mock.user = player
    assert client.get(new_token_url).status_code == 400
    new_character_url = url_for('generic.new_character', campaign=campaign)
    assert new_character_url in new_char_with_token_url
    assert client.get(new_character_url).status_code == 400
    assert client.get(new_character_url + '?token=wrong').status_code == 400
    assert client.get(new_char_with_token_url).status_code == 200
