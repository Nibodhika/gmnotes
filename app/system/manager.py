import sys
import os
from colors import cprint as print
from .system import GenericSystem, System


class _SystemManager(object):

    def __init__(self):
        self.systems = {}
        self.app = None
        self.generic = None
        
    def init_app(self, app, systems_folder=None):
        self.app = app

        self.init_generic(app)

        # TODO maybe read some config argument from app to allow for configuration
        if systems_folder is None:
            import systems
            systems_folder = os.path.dirname(systems.__file__)

        for system in os.listdir(systems_folder):
            # Ignore __pycache__ and __init__
            if system.startswith('__'):
                continue

            abs_path = os.path.join(systems_folder, system)
            # Do the actual import
            self.import_system(abs_path)

    def init_generic(self, app):
        from app.system import SystemView

        self.generic = GenericSystem('generic')
        self.generic.init_app(app)
        self.systems[self.generic.name] = self.generic

    def import_system(self, script_path):
        from importlib.util import spec_from_file_location, module_from_spec

        if self.app is None:
            raise RuntimeError("Attempted to import a system before initializing System manager. Call init_app first.")
        
        system_folder, system_name = os.path.split(script_path)
        if os.path.isdir(script_path):
            system_folder = script_path
            script_path = os.path.join(script_path, '__init__.py')
            # A folder without an __init__.py, ignore
            if not os.path.exists(script_path):
                return False
        elif script_path.endswith('.py'):
            # remove the .py from the system_name
            system_name = system_name[:-3]
        else:
            # Not a folder nor a .py file, ignore
            return False
        
        # Put the systems inside systems to keep it organized
        full_module_name = 'systems.{}'.format(system_name)

        # At this point script_path is an absolute path to a python script
        spec = spec_from_file_location(full_module_name, script_path)
        module = module_from_spec(spec)
        sys.modules[spec.name] = module
        spec.loader.exec_module(module)

        # module is now populated with the contents of the script

        # Try to access the variables to ensure the system was designed correctly
        try:
            system = module.system
            assert isinstance(system, System), "Attribute system is not an actual system"
            system.set_main_folder(system_folder)
            system.init_app(self.app)
        except Exception as e:
            print("Error importing {}\n  {}".format(system_name, e), color='red')
            return False

        if system.name in self.systems:
            print("A system with name {} already exists, ignoring the new instance from {}".format(
                system.name, script_path), color='red')
            return False
        self.systems[system.name] = system
        print('System {} ({}) imported'.format(system.name, system.long_name), color='green')
        return True

    def get_base_url(self, system):
        """
        Given a system returns it's base path if it's register or an empty string otherwise.

        Useful for the navigation bar to go to the generic view for non-registered systems

        :param system:
        :return: the url for this system
        """
        return "/{}".format(self.get_system_name(system))

    def get_system_name(self, system):
        if system in self.systems:
            return system
        return self.generic.name

    def get_commands(self, system_name):
        """
        Gets the Discord bot commands for a given system,
        if the system does not exists returns the default list of commands.
        If the system exists extends the default commands to include the cmd_list.
        Note that this might override some of the default commands.

        :param system_name: The name of the system for which to get the list of commands
        :return: an array containing the commands to use for this system
        """
        from app.discord_bot.default_commands import cmd_list
        # Create a dict with the name of the Command so systems can override commands by creating one with the same name
        default_commands = {c.name: c for c in cmd_list}
        if system_name in self.systems:
            default_commands.update({c.name: c for c in self.systems[system_name].cmd_list})
        # But returning just the list of Commands
        return default_commands.values()


manager = _SystemManager()

