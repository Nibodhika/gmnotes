from app.system.system_view import SystemView


class GenericSystem(object):
    """
    Generic System used for the basic stuff all Systems should have.
    """
    def __init__(self, name, long_name=None, cmd_list=[], view_class=SystemView):
        """

        :param name: Name of the system, such as v5
        :param long_name: Long Name, such as Vampire the masquerade 5th
        :param cmd_list: List of commands to add to bots of this system
        :param view_class: The View to use for the system
        """
        self.name = name
        self.view = view_class(name)

        self.long_name = long_name if long_name is not None else name
        self.cmd_list = cmd_list

    def init(self):
        """
        Command that is called via the init command of the manager.
        It should initialize the database.
        :return:
        """
        pass

    def set_main_folder(self, value):
        self.view.set_main_folder(value)

    def init_app(self, app):
        self.view.init_app(app)


class System(GenericSystem):
    """
    Class to be used for creating new systems.

    Defines a Mixin and a Model that will create tables named <system_name>_<model_name>
    to help with the creation of new database models and avoid collisions
    """

    def __init__(self, name, long_name=None, cmd_list=[], view_class=SystemView):
        super(System, self).__init__(name, long_name, cmd_list, view_class)
        from app.models import db

        class MyMixin(object):
            __system__ = name
            
            __mapper_args__ = {
                'polymorphic_identity': name,
            }
        
        self.Mixin = MyMixin
        self.db = db

        class MyModel(MyMixin, db.Model):
            __abstract__ = True

        self.Model = MyModel


