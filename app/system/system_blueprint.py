import os

from flask import Blueprint, render_template, url_for, request
from flask_login import login_required, current_user
from functools import wraps
from werkzeug.utils import redirect

from colors import cprint as print


def campaign_signer(campaign):
    """
    Helper function that creates a TimestampSigner with a secret key that's a combination of the app secret key
    and the campaign id.

    This is an extra safety prevention so that gms from one campaign can't sign another campaign tokens
    in the event of a bug in the api that would allow it.

    :param campaign:
    :return:
    """
    from flask import current_app
    from itsdangerous import URLSafeTimedSerializer
    base_secret_key = current_app.config['SECRET_KEY']
    return URLSafeTimedSerializer(base_secret_key, salt=str(campaign.id))


def campaign_sign(campaign, data):
    """
    Helper function that signs a message

    :param campaign:
    :return:
    """
    signer = campaign_signer(campaign)
    return signer.dumps(data)


def campaign_check(campaign, token):
    """
    Helper function that verifies the signature of a message using the app's config to get the max_age

    :param campaign:
    :param signature:
    :return:
    """
    from itsdangerous import BadSignature
    from flask import current_app

    if token is None:
        return None

    max_age = current_app.config['MAX_TOKEN_AGE']

    signer = campaign_signer(campaign)

    try:
        content = signer.loads(token, max_age=max_age)
    except BadSignature as e:
        msg = "Someone attempted to use an invalid token: {}\n{}".format(token, e)
        print(msg, color='red')
        return None

    return content


def campaign_system_check(system_name, gm=False, check_token=False):
    """
    A decorator that takes a system name as parameter and checks that the campaign is of this system,
    otherwise renders the error page.
    Note that it calls login_required, so it should be used instead of it.

    It can make a page only visible by the gm by setting gm=True

    Optionally it can allow to show a campaign to someone who's not in it by providing a valid token.
    Note that by default it does not check the token, so the views that can be accessed via tokens need to specify it

    :param system_name: Name of the system to use as filter
    :param gm: Whether this page is only visible to the GM
    :param check_token: Whether this endpoint can be accessed by a user with an adequate token
    :return: The decorator with this setup
    """
    def _decorator(func):
        @wraps(func)
        @login_required
        def decorated_view(campaign, *args, **kwargs):
            from app.system import manager

            no_permission = "The selected campaign does not exist or you don't have permissions for it"
            invalid_token = "Invalid or expired token"
            system_missmatch = "The selected campaign is not of this system"
            gm_required = "You are not the GM of this campaign"

            token_data = None
            if campaign is None:
                return render_template("base/error.html", errors=[no_permission]), 400
            elif campaign not in current_user.campaigns:
                if check_token:
                    token = request.args.get('token')
                    token_data = campaign_check(campaign, token)
                    # TODO Token should probably be more complex than just the user id
                    if token_data != str(current_user.id):
                        return render_template("base/error.html", errors=[invalid_token]), 400
                else:
                    return render_template("base/error.html", errors=[no_permission]), 400

            # Only check the campaign's system if the user has access to it
            # to avoid leaking information about the system
            if manager.get_system_name(campaign.system) != system_name:
                return render_template("base/error.html", errors=[system_missmatch]), 400

            if token_data is None and gm and campaign.gm != current_user:
                return render_template("base/error.html", errors=[gm_required]), 400

            return func(campaign, *args, **kwargs)
        return decorated_view
    return _decorator


def build_system_bp(system_view, system_name):
    """
    Creates a Blueprint with the pages a given system needs.
    Most pages perform basic checks such as authenticated, if the campaign is correct and/or if the user is the GM
    Then call the corresponding view from the SystemView instance passed as first parameter

    :param system_view: The system view to use to get the views from
    :param system_name: The name of the system, will prefix all pages in the returned Blueprint
    :return: The Created Blueprint
    """
    bp = Blueprint(system_name, __name__,
                   url_prefix='/{}'.format(system_name),
                   )

    @bp.context_processor
    def inject_topbar():
        return {
            'topbar': system_view.topbar
        }

    # Main views
    @bp.route('/<campaign:campaign>', strict_slashes=False)
    @campaign_system_check(system_name)
    def overview(campaign):
        return system_view.overview(campaign)

    @bp.route('/<campaign:campaign>/sessions')
    @campaign_system_check(system_name)
    def game_sessions(campaign):
        return system_view.sessions(campaign)

    @bp.route('/<campaign:campaign>/characters')
    @campaign_system_check(system_name)
    def characters(campaign):
        return system_view.characters(campaign)

    @bp.route('/<campaign:campaign>/locations')
    @campaign_system_check(system_name)
    def locations(campaign):
        return system_view.locations(campaign)

    @bp.route('/<campaign:campaign>/objects')
    @campaign_system_check(system_name)
    def objects(campaign):
        return system_view.objects(campaign)

    @bp.route('/<campaign:campaign>/bot', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True)
    def bot(campaign):
        from app.models import Bot, db
        from app.views.model_forms import ModelForm

        bot_obj = Bot.query.filter_by(campaign=campaign).first()

        class BotForm(ModelForm):
            class Meta:
                model = Bot

        form = BotForm(obj=bot_obj)
        if form.validate_on_submit():
            if bot_obj is None:
                bot_obj = Bot(campaign=campaign)
            form.populate_obj(bot_obj)
            try:
                db.session.add(bot_obj)
                db.session.commit()
            except Exception as e:
                form.errors['SQL'] = [str(e)]
            else:
                # All good, we can start the bot now
                from app.discord_bot import manager as bot_manager
                bot_manager.restart_bot(bot_obj)

        bot_menu = {}
        bot_config = []

        general_config = render_template("components/bot_general.html",
                                         form=form, campaign=campaign,
                                         form_submit=url_for('.bot', campaign=campaign),
                                         )

        bot_config.append({
            'title': 'General',
            'content': general_config,
            'id': 'general'
        })

        bot_menu['Configuration'] = bot_config
        audio_title = ""
        play_url = url_for('.bot_play', campaign=campaign)
        if bot_obj is not None:
            current_volume = bot_obj.volume
            # Formatting as a 0-100%
            current_volume = round(current_volume * 100)
            audio_title = render_template("components/bot_audio_header.html",
                                          pause_url=url_for('.bot_pause', campaign=campaign),
                                          stop_url=url_for('.bot_stop', campaign=campaign),
                                          vol_down_url=url_for('.bot_vol_down', campaign=campaign),
                                          vol_up_url=url_for('.bot_vol_up', campaign=campaign),
                                          current_volume=current_volume,
                                          )
            bot_menu['Audio'] = []
            for k, v in bot_obj.get_audio_files().items():
                content = render_template("components/bot_audio_list.html",
                                          elements=v.keys(), category=k)
                bot_menu['Audio'].append({
                    'title': k,
                    'content': content,
                    'id': k
                })

        return render_template("bot.html", campaign=campaign,
                               audio_title=audio_title, elements=bot_menu,
                               play_url=play_url)

    @bp.route('/<campaign:campaign>/bot/pause')
    @campaign_system_check(system_name, gm=True)
    def bot_pause(campaign):
        from app.models import Bot
        bot_obj = Bot.query.filter_by(campaign=campaign).first()
        if bot_obj is not None:
            bot_obj.pause()
        return "pause"

    @bp.route('/<campaign:campaign>/bot/stop')
    @campaign_system_check(system_name, gm=True)
    def bot_stop(campaign):
        from app.models import Bot
        bot_obj = Bot.query.filter_by(campaign=campaign).first()
        if bot_obj is not None:
            bot_obj.stop()
        return "stop"

    @bp.route('/<campaign:campaign>/bot/vol_up')
    @campaign_system_check(system_name, gm=True)
    def bot_vol_up(campaign):
        from app.models import Bot
        bot_obj = Bot.query.filter_by(campaign=campaign).first()
        if bot_obj is None:
            return "No Bot"
        bot_obj.volume += 0.05
        return {'volume': bot_obj.volume}

    @bp.route('/<campaign:campaign>/bot/vol_down')
    @campaign_system_check(system_name, gm=True)
    def bot_vol_down(campaign):
        from app.models import Bot
        bot_obj = Bot.query.filter_by(campaign=campaign).first()
        if bot_obj is None:
            return "No Bot"
        bot_obj.volume -= 0.05
        return {'volume': bot_obj.volume}

    @bp.route('/<campaign:campaign>/bot/play', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True)
    def bot_play(campaign):
        from app.models import Bot
        bot_obj = Bot.query.filter_by(campaign=campaign).first()
        # Allowing a get that does nothing for now, eventually it will return what the bot is playing
        # We allow the get to be in conformity with the other methods and be easy to test
        if request.method == 'GET':
            return ""
        category = request.form.get('category')
        audio = request.form.get('audio')
        out = None
        if bot_obj is not None:
            out = bot_obj.play(category, audio)
        return {'played': out}

    @bp.route('/<campaign:campaign>/del_bot')
    @campaign_system_check(system_name, gm=True)
    def del_bot(campaign):
        from app.models import db, Bot
        bot = Bot.query.filter_by(campaign=campaign).first()
        if bot is not None:
            from app.discord_bot import manager as bot_manager
            bot_manager.stop_bot(bot)
            db.session.delete(bot)
            db.session.commit()
        return redirect(url_for(".overview", campaign=campaign))

    @bp.route('/<campaign:campaign>/character/new', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True, check_token=True)
    def new_character(campaign):
        from app.models import AccessLevel

        form = system_view.get_character_form(campaign, None)
        if form.validate_on_submit():
            character = system_view.get_character_model()
            character.campaign = campaign
            return system_view.save_character(campaign, character, form)

        access_level = AccessLevel.GM if current_user == campaign.gm else AccessLevel.OWNER
        return system_view.character_details(campaign, None, access_level, form)

    @bp.route('/<campaign:campaign>/character/new_token/<player_id>')
    @campaign_system_check(system_name, gm=True)
    def new_character_create_token(campaign, player_id):
        # TODO Token should probably be more complex than just the user id
        token = campaign_sign(campaign, player_id)
        return url_for('.new_character', campaign=campaign, token=token, _external=True)

    # Detail views Character
    @bp.route('/<campaign:campaign>/character/<character:character>', methods=('GET', 'POST'))
    @campaign_system_check(system_name)
    def character_detail(campaign, character):
        from app.models import AccessLevel

        if character is None or character.campaign != campaign:
            return system_view.filtered(campaign, character)
        access_level = character.access_level(current_user)
        if access_level == AccessLevel.NONE:
            return system_view.filtered(campaign, character)

        form = system_view.get_character_form(campaign, character)

        if form.validate_on_submit() and access_level & AccessLevel.EDIT:
            return system_view.save_character(campaign, character, form)
        return system_view.character_details(campaign, character, access_level, form)

    @bp.route('/<campaign:campaign>/character/<character:character>/del')
    @campaign_system_check(system_name, gm=True)
    def del_character(campaign, character):
        from app.models import db
        from app.models import AccessLevel

        if character is None or character.campaign != campaign:
            return system_view.filtered(campaign, character)
        access_level = character.access_level(current_user)
        if access_level < AccessLevel.OWNER:
            return system_view.filtered(campaign, character)

        db.session.delete(character)
        db.session.commit()

        return redirect(url_for(".characters", campaign=campaign))

    # Location
    @bp.route('/<campaign:campaign>/location/<location:location>', methods=('GET', 'POST'))
    @campaign_system_check(system_name)
    def location_detail(campaign, location):
        from app.models import AccessLevel

        if location is None or location.campaign != campaign:
            return system_view.filtered(campaign, location)
        access_level = location.access_level(current_user)
        if access_level == AccessLevel.NONE:
            return system_view.filtered(campaign, location)

        form = system_view.get_location_form(campaign, location)
        if form.validate_on_submit() and access_level & AccessLevel.EDIT:
            return system_view.save_location(campaign, location, form)
        return system_view.location_details(campaign, location, access_level, form)

    @bp.route('/<campaign:campaign>/location/new', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True, check_token=True)
    def new_location(campaign):
        from app.models import AccessLevel

        form = system_view.get_location_form(campaign, None)
        if form.validate_on_submit():
            location = system_view.get_location_model()
            location.campaign = campaign
            return system_view.save_location(campaign, location, form)

        access_level = AccessLevel.GM if current_user == campaign.gm else AccessLevel.OWNER
        return system_view.location_details(campaign, None, access_level, form)

    @bp.route('/<campaign:campaign>/location/<location:location>/del')
    @campaign_system_check(system_name, gm=True)
    def del_location(campaign, location):
        from app.models import db
        from app.models import AccessLevel

        if location is None or location.campaign != campaign:
            return system_view.filtered(campaign, location)
        access_level = location.access_level(current_user)
        if access_level < AccessLevel.OWNER:
            return system_view.filtered(campaign, location)

        db.session.delete(location)
        db.session.commit()

        return redirect(url_for(".locations", campaign=campaign))

    # Object
    @bp.route('/<campaign:campaign>/object/<game_object:game_object>', methods=('GET', 'POST'))
    @campaign_system_check(system_name)
    def object_detail(campaign, game_object):
        from app.models import AccessLevel

        if game_object is None or game_object.campaign != campaign:
            return system_view.filtered(campaign, game_object)
        access_level = game_object.access_level(current_user)
        if access_level == AccessLevel.NONE:
            return system_view.filtered(campaign, game_object)

        form = system_view.get_object_form(campaign, game_object)
        if form.validate_on_submit() and access_level & AccessLevel.EDIT:
            return system_view.save_object(campaign, game_object, form)
        return system_view.object_details(campaign, game_object, access_level, form)

    @bp.route('/<campaign:campaign>/object/new', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True, check_token=True)
    def new_object(campaign):
        from app.models import AccessLevel

        form = system_view.get_object_form(campaign, None)
        if form.validate_on_submit():
            game_object = system_view.get_object_model()
            game_object.campaign = campaign
            return system_view.save_object(campaign, game_object, form)

        access_level = AccessLevel.GM if current_user == campaign.gm else AccessLevel.OWNER
        return system_view.object_details(campaign, None, access_level, form)

    @bp.route('/<campaign:campaign>/object/<game_object:game_object>/del')
    @campaign_system_check(system_name, gm=True)
    def del_object(campaign, game_object):
        from app.models import db
        from app.models import AccessLevel

        if game_object is None or game_object.campaign != campaign:
            return system_view.filtered(campaign, game_object)
        access_level = game_object.access_level(current_user)
        if access_level < AccessLevel.OWNER:
            return system_view.filtered(campaign, game_object)

        db.session.delete(game_object)
        db.session.commit()

        return redirect(url_for(".objects", campaign=campaign))

    # Session
    @bp.route('/<campaign:campaign>/session/<game_session:game_session>', methods=('GET', 'POST'))
    @campaign_system_check(system_name)
    def session_detail(campaign, game_session):
        from app.models import AccessLevel
        if game_session is None or game_session.campaign != campaign:
            return system_view.filtered(campaign, game_session)

        access_level = game_session.access_level(current_user)
        if access_level == AccessLevel.NONE:
            return system_view.filtered(campaign, access_level)

        form = system_view.get_session_form(campaign, game_session)
        if form.validate_on_submit() and access_level & AccessLevel.EDIT:
            return system_view.save_session(campaign, game_session, form)
        return system_view.session_details(campaign, game_session, access_level, form)

    @bp.route('/<campaign:campaign>/session/new', methods=('GET', 'POST'))
    @campaign_system_check(system_name, gm=True, check_token=True)
    def new_session(campaign):
        from app.models import AccessLevel
        form = system_view.get_session_form(campaign, None)
        if form.validate_on_submit():
            game_session = system_view.get_session_model()
            game_session.campaign = campaign
            return system_view.save_session(campaign, game_session, form)

        access_level = AccessLevel.GM if current_user == campaign.gm else AccessLevel.NONE
        return system_view.session_details(campaign, None, access_level, form)

    @bp.route('/<campaign:campaign>/session/<game_session:game_session>/del')
    @campaign_system_check(system_name, gm=True)
    def del_session(campaign, game_session):
        from app.models import db

        if game_session is None or game_session.campaign != campaign:
            return system_view.filtered(campaign, game_session)

        db.session.delete(game_session)
        db.session.commit()

        return redirect(url_for(".game_sessions", campaign=campaign))

    return bp
