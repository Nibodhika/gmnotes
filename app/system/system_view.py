import os

from flask import render_template, redirect, url_for
from flask_login import current_user
from wtforms import SelectField


class SystemView(object):

    LIST_VIEW_TEMPLATE = 'components/list_view.html'

    def __init__(self, name):
        """

        :param name: The name of the system, will prepend all pages in this view
        """
        from app.system.system_blueprint import build_system_bp
        self._bp = build_system_bp(self, name)

    def set_main_folder(self, value):
        template_folder = os.path.join(value, 'templates')
        self._bp.template_folder = template_folder
        static_folder = os.path.join(value, 'static')
        from colors import cprint
        cprint("Setting static folder to ", static_folder, color='cyan')
        self._bp.static_folder = static_folder

    def init_app(self, app):
        app.register_blueprint(self._bp)

    def list_view(self, campaign, elements, elem_view, title, name, new_url=None):
        """
        Function used for all basic list views.

        Note that if you override this all lists, i.e. Characters, Locations, etc will change.

        :param campaign:
        :param elements:
        :param elem_view:
        :param title:
        :param name:
        :param new_url:
        :return:
        """

        def generate(l):
            return [{
                # Elements that allow no name should implement a safe_name
                'title': e.name if e.name else e.safe_name,
                'content': elem_view(e),
                'id': 'elem_{}'.format(e.id)
            } for e in l]

        generated = {}
        if type(elements) == dict:
            for k in elements:
                generated[k] = generate(elements[k])
            # Filter empty elements
            generated = {k: v for k, v in generated.items() if v}
        else:
            generated[''] = generate(elements)

        return render_template(self.LIST_VIEW_TEMPLATE,
                               campaign=campaign,
                               elements=generated,
                               title=title,
                               element_name=name,
                               new_url=new_url)

    def topbar(self, campaign):
        """
        Given a campaign returns the topbar to use.
        The topbar is represented as a list of tuples, each element has:
            - Title to be shown to the user
            - endpoint to direct to
            - Whether this element is available for all users (True), or only for the gm (False)

        You can overwrite or extend this on your view if your system has different models.

        :return: list( tuple( str, str, bool))
        """
        return [
            (campaign.name, '.overview', True),
            ('Sessions', '.game_sessions', True),
            ('Characters', '.characters', True),
            ('Locations', '.locations', True),
            ('Objects', '.objects', True),
            ('Bot', '.bot', False),
        ]

    # # Master views # #
    # Each of these can be overridden to change the entire view of one of the main default views
    # If you do that ensure there's a link to / somewhere in the page
    # so that the user can exit your system back to the main view to switch to another campaign
    def overview(self, campaign):
        """
        Renders the page at /<system>/<campaign_id> that should show a brief overview of the campaign.

        :param campaign:
        :return:
        """
        from app.models import Player
        from flask_wtf import FlaskForm

        player_choices = []
        # TODO eventually this needs to list the friends of the current user instead of all users
        for p in Player.query.all():
            # Only add players that don't have already have a player in this campaign
            if p in campaign.players or p == campaign.gm:
                continue
            url = url_for('.new_character_create_token', campaign=campaign, player_id=p.id)
            player_choices.append((url, p.name))

        class PlayersForm(FlaskForm):
            player_id = SelectField(
                'Player',
                choices=player_choices
            )

        form = PlayersForm()

        return render_template("campaign.html", campaign=campaign, player_form=form)

    def sessions(self, campaign):
        """
        Renders the page at /<system>/<campaign_id>/sessions that should list the sessions of the campaign.

        :param campaign:
        :return:
        """
        add_btn = 'Session' if current_user == campaign.gm else None
        new_url = url_for('.new_session', campaign=campaign)
        return self.list_view(campaign, campaign.sessions, self.session_short_view, 'Sessions', add_btn, new_url=new_url)

    def characters(self, campaign):
        """
        Renders the page at /<system>/<campaign_id>/characters that should list the characters of the campaign.

        :param campaign:
        :return:
        """
        from app.models import AccessLevel
        characters = {'PC': [], 'NPC': []}
        for c in campaign.characters:
            if c.access_level(current_user) & AccessLevel.VIEW == 0:
                continue
            if c.is_npc:
                characters['NPC'].append(c)
            else:
                characters['PC'].append(c)

        add_btn = 'Character' if current_user == campaign.gm else None
        new_url = url_for('.new_character', campaign=campaign)
        return self.list_view(campaign, characters, self.character_short_view, 'Characters', add_btn, new_url=new_url)

    def locations(self, campaign):
        """
        Renders the page at /<system>/<campaign_id>/locations that should list the locations of the campaign.

        :param campaign:
        :return:
        """
        from app.models import AccessLevel
        locations = []
        for c in campaign.locations:
            if c.access_level(current_user) & AccessLevel.VIEW == 0:
                continue
            locations.append(c)

        add_btn = 'Location' if current_user == campaign.gm else None
        new_url = url_for('.new_location', campaign=campaign)
        return self.list_view(campaign, locations, self.location_short_view, 'Locations', add_btn, new_url=new_url)

    def objects(self, campaign):
        """
        Renders the page at /<system>/<campaign_id>/objects that should list the objects of the campaign.

        :param campaign:
        :return:
        """
        from app.models import AccessLevel
        objects = []
        for c in campaign.objects:
            if c.access_level(current_user) & AccessLevel.VIEW == 0:
                continue
            objects.append(c)
        add_btn = 'Object' if current_user == campaign.gm else None
        new_url = url_for('.new_object', campaign=campaign)
        return self.list_view(campaign, objects, self.objects_short_view, 'Objects', add_btn, new_url=new_url)

    # # Short views # #
    # These are the condensed views for each of the default elements
    # You can override them to change what's displayed for each element on the list view
    def character_short_view(self, character):
        """
        Renders the short view of a character in the list of all characters.

        :param character:
        :return:
        """
        elements = []
        if character.description:
            elements.append({
                'title': 'Description',
                'content': character.description
            })
        if character.bio:
            elements.append({
                'title': 'Bio',
                'content': character.bio
            })

        return render_template("components/ul_short_view.html", elements=elements,
                               view_link=url_for('.character_detail', campaign=character.campaign, character=character))

    def location_short_view(self, location):
        """
        Renders the short view of a location in the list of all locations.

        :param location:
        :return:
        """
        elements = []
        if location.description:
            elements.append({
                'title': 'Description',
                'content': location.description
            })
        return render_template("components/ul_short_view.html", elements=elements,
                               view_link=url_for('.location_detail', campaign=location.campaign, location=location))

    def objects_short_view(self, game_object):
        """
        Renders the short view of an object in the list of all objects.

        :param game_object:
        :return:
        """
        elements = []
        if game_object.description:
            elements.append({
                'title': 'Description',
                'content': game_object.description
            })
        return render_template("components/ul_short_view.html", elements=elements,
                               view_link=url_for('.object_detail', campaign=game_object.campaign, game_object=game_object))

    def session_short_view(self, game_session):
        """
        Renders the short view of a session in the list of all sessions.

        :param game_session:
        :return:
        """
        elements = []
        if game_session.description:
            elements.append({
                'title': 'Description',
                'content': game_session.description
            })

        return render_template("components/ul_short_view.html", elements=elements,
                               view_link=url_for('.session_detail', campaign=game_session.campaign,
                                                 game_session=game_session))

    # # Filter view ##
    def filtered(self, campaign, filtered_object):
        """
        This View is called when the user tries to access a character/location/object/etc he does not have access
        or does not belong to the current campaign, show a generic error but preserve the current campaign in the view

        The instance of the filtered object is passed in case a system wishes to show a more detailed error.
        Just be mindful that this object might not belong to this campaign, so you might be exposing information from
        someone else.

        :param campaign: The current campaign
        :param filtered_object: The object that was filtered
        :return:
        """
        generic_error = "Stop messing around, either this doesn't exist, you have no access to it or it's not of this campaign"
        return render_template("base/error.html", campaign=campaign, errors=[generic_error]), 400

    # View
    def character_details(self, campaign, character, access_level, form):
        """
        Renders the page at /<system>/<campaign_id>/character/<character_id>
        that should show the detailed information on a given character

        If you are OK with the default view and just want to add stuff override build_character_elements instead

        :param campaign:
        :param character:
        :param access_level:
        :param form:
        :return:
        """
        from app.models import AccessLevel

        # If character is None this is a new character
        if character is None:
            title = "New Character"
            submit_url = url_for('.new_character', campaign=campaign)
        else:
            title = character.name
            submit_url = url_for('.character_detail', campaign=campaign, character=character)

        title_block = render_template("components/character_detail_header.html",
                                      form=form, campaign=campaign, character=character,
                                      can_edit=access_level & AccessLevel.EDIT)

        elements = self.build_character_elements(character, form, access_level)

        return render_template(self.LIST_VIEW_TEMPLATE,
                               campaign=campaign,
                               elements=elements,
                               title=title,
                               title_block=title_block,
                               form=form,
                               form_submit=submit_url,
                               )

    def build_character_elements(self, character, form, access_level):
        """
        Builds the elements of the side-menu of the character view.
        If you added information to the character class of your system you want to extend this
        to return those extra details. Alternatively you can completely override it to not show the default values

        It should return either a dictionary where each key is a category or a list
        The values should be objects that contain a title, content and id.

        The title will be used in the navigation list on the list
        The content is what will be shown when that item is selected
        And the id is internally used in the html to map the element in the list to the content

        :param character:
        :param form:
        :param access_level:
        :return:
        """
        from app.views.model_forms import text_or_form
        from app.models import AccessLevel

        def text_field(elem, parse_markdown=True):
            return text_or_form(character, form, elem, access_level, parse_markdown)

        elements = {
            'Public': [
                {
                    'title': 'Description',
                    'content': text_field('description'),
                    'id': 'description'
                },
                {
                    'title': 'Bio',
                    'content': text_field('bio'),
                    'id': 'bio'
                },
            ]
        }
        if access_level & AccessLevel.VIEW_PRIVATE:
            elements['Private'] = [
                {
                    'title': 'Private Description',
                    'content': text_field('private_description'),
                    'id': 'private_description'
                },
                {
                    'title': 'Private Bio',
                    'content': text_field('private_bio'),
                    'id': 'private_bio'
                },
            ]
        if access_level == AccessLevel.GM:
            elements['GM'] = [
                {
                    'title': 'Notes',
                    'content': form['notes'],
                    'id': 'notes'
                },
            ]
        return elements

    def get_character_form(self, campaign, character):
        """
        Method used to create the form that is passed to the detail view
        Should be changed if the system implements an extension of character

        :param campaign:
        :param character:
        :return:
        """
        from app.views.model_forms import CharacterForm
        from app.models import Player
        form = CharacterForm(obj=character)

        player_choices = [(0, 'NPC')]
        # TODO eventually this needs to list the friends of the current user instead of all users
        for p in Player.query.all():
            # Only add players that don't have already have a player in this campaign
            # But also add the owner if the character exists
            if p in campaign.players:
                if not character or character and p is not character.player:
                    continue
            # GM can't have a player
            if p == campaign.gm:
                continue
            player_choices.append((p.id, p.name))
        form.player_id.choices = player_choices

        return form

    def get_character_model(self):
        """
        Method used to create an empty character to pass to the form
        Should be changed if the system implements an extension of character

        :return:
        """
        from app.models import Character
        return Character()

    def save_character(self, campaign, character, form):
        """
        Saves the object from the form into the character instance.
        WARNING: this is destructive and will override basically everything except id from the character

        :param campaign:
        :param character:
        :param form:
        :return:
        """
        from app.models import db

        is_new_character = character.id is None
        # Cleaning player_id, if 0 set None
        # otherwise sqlalchemy stores the foreign_key 0 and that prevents from having 2 NPCs
        if form.player_id.data == 0:
            form.player_id.data = None

        form.populate_obj(character)
        try:
            db.session.add(character)
            db.session.commit()
        except Exception as e:
            from colors import cprint
            cprint(e, color='red')
            # TODO parse the error better
            form.errors['sql'] = [str(e)]
            db.session.rollback()
            access_level = character.access_level(current_user)
            # character_details expect
            if is_new_character:
                character = None
            return self.character_details(campaign, character, access_level, form)

        return redirect(url_for(".characters", campaign=campaign))

    # Location
    def location_details(self, campaign, location, access_level, form):
        """
        Renders the page at /<system>/<campaign_id>/location/<location_id>
        that should show the detailed information on a given location

        If you are OK with the default view and just want to add stuff override build_location_elements instead

        :param campaign:
        :param location:
        :param access_level:
        :param form:
        :return:
        """
        from app.models import AccessLevel

        # If location is None this is a new location
        if location is None:
            title = "New Location"
            submit_url = url_for('.new_location', campaign=campaign)
        else:
            title = location.name
            submit_url = url_for('.location_detail', campaign=campaign, location=location)

        del_url = None
        if location:
            del_url = url_for(".del_location", campaign=campaign, location=location)

        title_block = render_template("components/generic_detail_header.html",
                                      form=form, campaign=campaign, element=location,
                                      del_url=del_url,
                                      can_edit=access_level & AccessLevel.EDIT)

        elements = self.build_location_elements(location, form, access_level)

        return render_template(self.LIST_VIEW_TEMPLATE,
                               campaign=campaign,
                               elements=elements,
                               title=title,
                               title_block=title_block,
                               form=form,
                               form_submit=submit_url,
                               )

    def build_location_elements(self, location, form, access_level):
        """
        Builds the elements of the side-menu of the location view.
        If you added information to the location class of your system you want to extend this
        to return those extra details. Alternatively you can completely override it to not show the default values

        It should return either a dictionary where each key is a category or a list
        The values should be objects that contain a title, content and id.

        The title will be used in the navigation list on the list
        The content is what will be shown when that item is selected
        And the id is internally used in the html to map the element in the list to the content

        :param location:
        :param form:
        :param access_level:
        :return:
        """
        from app.views.model_forms import text_or_form
        from app.models import AccessLevel

        def text_field(elem, parse_markdown=True):
            return text_or_form(location, form, elem, access_level, parse_markdown)

        elements = {
            'Public': [
                {
                    'title': 'Description',
                    'content': text_field('description'),
                    'id': 'description'
                },
            ]
        }
        if access_level & AccessLevel.VIEW_PRIVATE:
            elements['Private'] = [
                {
                    'title': 'Private Description',
                    'content': text_field('private_description'),
                    'id': 'private_description'
                },
            ]
        if access_level == AccessLevel.GM:
            elements['GM'] = [
                {
                    'title': 'Notes',
                    'content': form['notes'],
                    'id': 'notes'
                },
            ]
        return elements

    def get_location_form(self, campaign, location):
        """
        Method used to create the form that is passed to the detail view
        Should be changed if the system implements an extension of location

        :param campaign:
        :param location:
        :return:
        """
        from app.views.model_forms import LocationForm
        form = LocationForm(obj=location)
        character_choices = [(0, '-')]
        character_choices.extend([
            (c.id, c.name)
            for c in campaign.characters
            ])

        form.owner_id.choices = character_choices

        return form

    def get_location_model(self):
        """
        Method used to create an empty location to pass to the form
        Should be changed if the system implements an extension of location

        :return:
        """
        from app.models import Location
        return Location()

    def save_location(self, campaign, location, form):
        """
        Saves the object from the form into the location instance.
        WARNING: this is destructive and will override basically everything except id from the location

        :param campaign:
        :param location:
        :param form:
        :return:
        """
        from app.models import db

        # Cleaning owner_id, if 0 set None
        # otherwise sqlalchemy stores the foreign_key 0 and that prevents from having 2 NPCs
        if form.owner_id.data == 0:
            form.owner_id.data = None

        form.populate_obj(location)
        try:
            db.session.add(location)
            db.session.commit()
        except Exception as e:
            # TODO parse the error better
            form.errors['sql'] = [str(e)]
            db.session.rollback()
            access_level = location.access_level(current_user)
            return self.location_details(campaign, location, access_level, form)

        return redirect(url_for(".locations", campaign=campaign))

    # Object
    def object_details(self, campaign, game_object, access_level, form):
        """
        Renders the page at /<system>/<campaign_id>/object/<object_id>
        that should show the detailed information on a given object

        If you are OK with the default view and just want to add stuff override build_object_elements instead

        :param campaign:
        :param game_object:
        :param access_level:
        :param form:
        :return:
        """
        from app.models import AccessLevel

        # If game_object is None this is a new game_object
        if game_object is None:
            title = "New Object"
            submit_url = url_for('.new_object', campaign=campaign)
        else:
            title = game_object.name
            submit_url = url_for('.object_detail', campaign=campaign, game_object=game_object)

        del_url = None
        if game_object:
            del_url = url_for(".del_object", campaign=campaign, game_object=game_object)

        title_block = render_template("components/generic_detail_header.html",
                                      form=form, campaign=campaign, element=game_object,
                                      del_url=del_url,
                                      can_edit=access_level & AccessLevel.EDIT)

        elements = self.build_object_elements(game_object, form, access_level)

        return render_template(self.LIST_VIEW_TEMPLATE,
                               campaign=campaign,
                               elements=elements,
                               title=title,
                               title_block=title_block,
                               form=form,
                               form_submit=submit_url,
                               )

    def build_object_elements(self, game_object, form, access_level):
        """
        Builds the elements of the side-menu of the object view.
        If you added information to the object class of your system you want to extend this
        to return those extra details. Alternatively you can completely override it to not show the default values

        It should return either a dictionary where each key is a category or a list
        The values should be objects that contain a title, content and id.

        The title will be used in the navigation list on the list
        The content is what will be shown when that item is selected
        And the id is internally used in the html to map the element in the list to the content

        :param game_object:
        :param form:
        :param access_level:
        :return:
        """
        from app.views.model_forms import text_or_form
        from app.models import AccessLevel

        def text_field(elem, parse_markdown=True):
            return text_or_form(game_object, form, elem, access_level, parse_markdown)

        elements = {
            'Public': [
                {
                    'title': 'Description',
                    'content': text_field('description'),
                    'id': 'description'
                },
            ]
        }
        if access_level & AccessLevel.VIEW_PRIVATE:
            elements['Private'] = [
                {
                    'title': 'Private Description',
                    'content': text_field('private_description'),
                    'id': 'private_description'
                },
            ]
        if access_level == AccessLevel.GM:
            elements['GM'] = [
                {
                    'title': 'Notes',
                    'content': form['notes'],
                    'id': 'notes'
                },
            ]
        return elements

    def get_object_form(self, campaign, game_object):
        """
        Method used to create the form that is passed to the detail view
        Should be changed if the system implements an extension of GameObject

        :param campaign:
        :param game_object:
        :return:
        """
        from app.views.model_forms import GameObjectForm
        form = GameObjectForm(obj=game_object)
        character_choices = [(0, '-')]
        character_choices.extend([
            (c.id, c.name)
            for c in campaign.characters
        ])

        form.owner_id.choices = character_choices

        return form

    def get_object_model(self):
        """
        Method used to create an empty GameObject to pass to the form
        Should be changed if the system implements an extension of GameObject

        :return:
        """
        from app.models import GameObject
        return GameObject()

    def save_object(self, campaign, game_object, form):
        """
        Saves the object from the form into the GameObject instance.
        WARNING: this is destructive and will override basically everything except id from the game_object

        :param campaign:
        :param game_object:
        :param form:
        :return:
        """
        from app.models import db

        # Cleaning owner_id, if 0 set None
        # otherwise sqlalchemy stores the foreign_key 0 and that prevents from having 2 NPCs
        if form.owner_id.data == 0:
            form.owner_id.data = None

        form.populate_obj(game_object)
        try:
            db.session.add(game_object)
            db.session.commit()
        except Exception as e:
            # TODO parse the error better
            form.errors['sql'] = [str(e)]
            db.session.rollback()
            access_level = game_object.access_level(current_user)
            return self.object_details(campaign, game_object, access_level, form)

        return redirect(url_for(".objects", campaign=campaign))

    # session
    def session_details(self, campaign, game_session, access_level, form):
        """
        Renders the page at /<system>/<campaign_id>/session/<session_id>
        that should show the detailed information on a given session

        If you are OK with the default view and just want to add stuff override build_session_elements instead

        :param campaign:
        :param game_session:
        :param access_level:
        :param form:
        :return:
        """
        from app.models import AccessLevel

        # If game_session is None this is a new game_session
        if game_session is None:
            title = "New Session"
            submit_url = url_for('.new_session', campaign=campaign)
        else:
            title = game_session.name
            submit_url = url_for('.session_detail', campaign=campaign, game_session=game_session)

        del_url = None
        if game_session:
            del_url = url_for(".del_session", campaign=campaign, game_session=game_session)

        title_block = render_template("components/session_detail_header.html",
                                      form=form, campaign=campaign, element=game_session,
                                      del_url=del_url,
                                      can_edit=access_level & AccessLevel.EDIT)

        elements = self.build_session_elements(game_session, form, access_level)

        return render_template(self.LIST_VIEW_TEMPLATE,
                               campaign=campaign,
                               elements=elements,
                               title=title,
                               title_block=title_block,
                               form=form,
                               form_submit=submit_url,
                               )

    def build_session_elements(self, game_session, form, access_level):
        """
        Builds the elements of the side-menu of the session view.
        If you added information to the session class of your system you want to extend this
        to return those extra details. Alternatively you can completely override it to not show the default values

        It should return either a dictionary where each key is a category or a list
        The values should be objects that contain a title, content and id.

        The title will be used in the navigation list on the list
        The content is what will be shown when that item is selected
        And the id is internally used in the html to map the element in the list to the content

        :param game_session:
        :param form:
        :param access_level:
        :return:
        """
        from app.views.model_forms import text_or_form
        from app.models import AccessLevel

        def text_field(elem, parse_markdown=True):
            return text_or_form(game_session, form, elem, access_level, parse_markdown)

        elements = {
            'Public': [
                {
                    'title': 'Description',
                    'content': text_field('description'),
                    'id': 'description'
                },
            ]
        }

        if access_level == AccessLevel.GM:
            elements['GM'] = [
                {
                    'title': 'Notes',
                    'content': form['notes'],
                    'id': 'notes'
                },
            ]
        return elements

    def get_session_form(self, campaign, game_session):
        """
        Method used to create the form that is passed to the detail view
        Should be changed if the system implements an extension of game_session

        :param campaign:
        :param game_session:
        :return:
        """
        from app.views.model_forms import GameSessionForm
        form = GameSessionForm(obj=game_session)
        return form

    def get_session_model(self):
        """
        Method used to create an empty game_session to pass to the form
        Should be changed if the system implements an extension of game_session

        :return:
        """
        from app.models import GameSession
        return GameSession()

    def save_session(self, campaign, game_session, form):
        """
        Saves the object from the form into the game_session instance.
        WARNING: this is destructive and will override basically everything except id from the game_session

        :param campaign:
        :param game_session:
        :param form:
        :return:
        """
        from app.models import db

        form.populate_obj(game_session)
        try:
            db.session.add(game_session)
            db.session.commit()
        except Exception as e:
            # TODO parse the error better
            form.errors['sql'] = [str(e)]
            db.session.rollback()
            access_level = game_session.access_level(current_user)
            return self.session_details(campaign, game_session, access_level, form)

        return redirect(url_for(".game_sessions", campaign=campaign))
