#!/usr/bin/python


UNDERLINE = '\033[4m'
BOLD = '\033[1m'

BLINK = '\033[5m'
## Colors
BACKGROUND_DARK = '\033[4'
FOREGROUND_DARK = '\033[3'

BACKGROUND_LIGHT = '\033[10'
FOREGROUND_LIGHT = '\033[9'
## Color List
BLACK = '0m'
RED = '1m'
GREEN = '2m'
YELLOW = '3m'
BLUE = '4m'
MAGENTA = '5m'
CYAN = '6m'
WHITE = '7m'

#String used to end the colorization
ENDC = '\033[0m'

def getColor(color):
    if color.lower() == 'green':
        return GREEN
    elif color.lower() == 'magenta':
        return MAGENTA
    elif color.lower() == 'blue':
        return BLUE
    elif color.lower() == 'yellow':
        return YELLOW
    elif color.lower() == 'red':
        return RED
    elif color.lower() == 'cyan':
        return CYAN
    elif color.lower() == 'white':
        return WHITE
    elif color.lower() == 'black':
        return BLACK
    else:
        return 'm' ## No color

# def getForeground(colorType, color):
#     if type(colorType) is str:
#         if colorType.lower() == 'dark':
#             FOREGROUND = FOREGROUND_DARK
#         else:
#             FOREGROUND = FOREGROUND_LIGHT
#     elif type(colorType) is int:
#         if colorType.lower() == 1:
#             FOREGROUND = FOREGROUND_DARK
#         else:
#             FOREGROUND = FOREGROUND_LIGHT
#     else:
#         FOREGROUND = FOREGROUND_LIGHT

#     return FOREGROUND+getColor(color)


def intensity(what, intensity):
    if what.lower() == 'background':
        if intensity.lower() == 'dark':
            return BACKGROUND_DARK
        else:
            return BACKGROUND_LIGHT
    else:
        if intensity.lower() == 'dark':
            return FOREGROUND_DARK
        else:
            return FOREGROUND_LIGHT
    
def colorize(what, colorType, color):
    if type(colorType) is str:
        if colorType.lower() == 'dark':
            TYPE = intensity(what, 'dark')
        else:
            TYPE = intensity(what, 'light')
    elif type(colorType) is int:
        if colorType.lower() == 1:
            TYPE = intensity(what, 'dark')
        else:
            TYPE = intensity(what, 'light')
    else:
        TYPE = intensity(what, 'light')

    return TYPE+getColor(color)


# def getBackground(colorType, color):
#     if type(colorType) is str:
#         if colorType.lower() == 'dark':
#             BACKGROUND = BACKGROUND_DARK
#         else:
#             BACKGROUND = BACKGROUND_LIGHT
#     elif type(colorType) is int:
#         if colorType.lower() == 1:
#             BACKGROUND = BACKGROUND_DARK
#         else:
#             BACKGROUND = BACKGROUND_LIGHT
#     else:
#         BACKGROUND = BACKGROUND_LIGHT

#     return BACKGROUND+getColor(color)

    
def format(string, color=None, back=None, blink = None, underline = None, bold = None, colorType = None, bgType=None):
    printstr = ''

    if color:
        printstr += colorize('foreground', colorType, color)

    if back:
        printstr += colorize('background', bgType, back)

    if blink:
        printstr += BLINK

    if underline:
        printstr += UNDERLINE

    if bold:
        printstr += BOLD
    

    printstr += string

    if color or back or blink or underline or bold:
        printstr+=ENDC

    return printstr

def cprint(*string, color=None, back=None, blink=None, underline=None, bold=None, colorType=None, bgType=None):
    printstr = ''
    for s in string:
        printstr += str(s)+" "

    # if color or back or blink:
    printstr = format(printstr, color, back, blink, underline, bold,colorType, bgType)
        
    print(printstr)


if __name__ == '__main__':
    cprint('no color', 'at all')
    cprint('magenta', color='magenta')
    cprint('blinking blue', blink = True, color='blue')
    cprint('dark black on green', color='black', back='Green', colorType='dark')
    cprint('bold yellow', bold=1, color='YeLloW')
    cprint('underlined red', underline=True, color='red')
    cprint('bold underlined cyan', bold=True, underline=True, color='cyan')
    cprint('bold underlined blinking black on white', bold=True, underline=True, blink=True, color='Black', back='White')



