# (Optional) Virtualenv setup

Create and activate the virtualenv

```bash
virtualenv gmnotes -p python3
source gmnotes/bin/activate
```
# Install requirements

```bash
pip install -r requirements.txt
bower install
```
# Run first time initialization

Before running for the first time we need to initialize the database:

```bash
python manage.py init
```

WARNING: This drops all tables first, so don't use it on a server that has already been initialized,
or you will lose all of your data.
Since there is no migrations this is the way to ensure the databases are created correctly

This also creates the roles "admin" and "manager" and creates a default admin:admin user,
you can change the username and password with the -n and -p options, e.g.

```bash
python manage.py init -n username -p pass
```

Will instead create an admin user with login username and password pass

# Start server

To start the server run

```bash
python manage.py runserver
```

Then in a browser you can navigate to http://localhost:5000

# How to use

TODO
