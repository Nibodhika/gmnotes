from datetime import date, timedelta

import os
import pytest
from pathlib import Path

from app import create_app
from app.models import db as _db


@pytest.fixture()
def files(request):
    """
    Returns a pathlib.Path of the folder _files on the current test path
    """
    files = os.path.join(request.fspath.dirname, '_files')
    files_path = Path(files)
    return files_path


@pytest.fixture(scope='session')
def app(request):
    app = create_app(config_dict={
        'TESTING': True,
        'SQLALCHEMY_DATABASE_URI': "sqlite:////tmp/gmnotes_tests.db",
        'DEBUG': True,
    })

    # Establish an application context before running the tests.
    ctx = app.app_context()
    ctx.push()

    def teardown():
        ctx.pop()

    request.addfinalizer(teardown)
    return app


@pytest.fixture(scope='function')
def db(app, request):
    """
    Session-wide test database was having problems with some rollbacks
    Changing to a function wide for now.

    TODO revert back to session
    This will significantly slow down tests, but test_unique_constraint is leaking the entry from the last commit
    """
    from app.config import config
    # Make a check so that if we start using a different database this starts to fail
    assert config.SQLALCHEMY_DATABASE_URI.startswith('sqlite:///')
    DB_PATH = config.SQLALCHEMY_DATABASE_URI[len('sqlite:///'):]

    if os.path.exists(DB_PATH):
        os.unlink(DB_PATH)

    _db.app = app
    _db.create_all()

    yield _db

    _db.drop_all()
    os.unlink(DB_PATH)


@pytest.fixture(scope='function')
def session(db, request):
    """Creates a new database session for a test."""

    connection = db.engine.connect()
    transaction = connection.begin()

    options = dict(bind=connection, binds={})
    session = db.create_scoped_session(options=options)

    db.session = session

    yield session

    transaction.rollback()
    session.rollback()
    connection.close()
    session.remove()


@pytest.fixture()
def system_manager(mocker):
    from app.system.manager import _SystemManager

    class SystemMockApp(object):
        """
        An empty App that has the functions called when initializing the system manager
        """

        def register_blueprint(self, *args, **kwargs):
            pass

    class MockedSystemManager(_SystemManager):

        def mock_app(self):
            """
            Creates an almost empty object that has the functions that should be called during init.
            
            :return:
            """
            return SystemMockApp()

        def mock_init(self):
            self.app = self.mock_app()
            self.init_generic(self.app)
            return self.app

        def mock_register(self, system_name):
            self.systems[system_name] = {}

    mocked_manager = MockedSystemManager()
    mocker.patch('app.system.manager', mocked_manager)
    return mocked_manager


@pytest.fixture()
def generic_game(session):
    """
    Creates the basic architecture of a game:
    - Three Players, 1 gm and 2 non-gm
    - One Campaign
    - Two sessions
    - A couple of notes on those sessions
    - Two Characters
    - Two Locations
    - Two Objects
    """
    from app.models import Player, Campaign, GameSession, Character, Location, GameObject

    # Players
    gm, _ = Player.create_player("The GM")
    session.add(gm)
    player, _ = Player.create_player("The Player")
    session.add(player)
    player2, _ = Player.create_player("The Player 2")
    session.add(player2)
    # Create a player not in the campaign
    player3, _ = Player.create_player("not in campaign")
    session.add(player3)

    # Campaign
    campaign = Campaign(name='foo', system='generic', gm=gm)
    assert campaign.id is None
    assert not campaign.allow_player_edit
    session.add(campaign)

    # Sessions
    last_week = date.today() - timedelta(days=7)
    first_session = GameSession(name="Foo 1", campaign=campaign, date=last_week, description="Introduction to the game")
    session.add(first_session)

    second_session = GameSession(campaign=campaign)
    session.add(second_session)

    # Characters
    character = Character(name="a PC", player=player, campaign=campaign)
    session.add(character)
    character2 = Character(name="a 2 PC", player=player2, campaign=campaign)
    session.add(character2)
    npc = Character(name="a NPC", campaign=campaign)
    session.add(npc)

    # Locations
    loc1 = Location(name="place 1", campaign=campaign, description="A generic place")
    session.add(loc1)
    loc2 = Location(name="place 2", campaign=campaign)
    session.add(loc2)

    # Objects
    obj1 = GameObject(name="obj 1", campaign=campaign, description="A generic object")
    session.add(obj1)
    obj2 = GameObject(name="obj 2", campaign=campaign)
    session.add(obj2)

    session.commit()
    assert campaign.id > 0
    # Ensure the second session is being created with date today
    assert (second_session.date - first_session.date).days == 7
    # Character.is_npc
    assert not character.is_npc
    assert npc.is_npc

    yield campaign


@pytest.fixture()
def authentication_mock(app, mocker):
    """
    Fixture to use to test different stages of authentication, returns an object that has a user variable
    setting that variable will make the application behave as if said user had logged in.
    Setting it to None mimics the behavior of unauthenticated user

    :param app:
    :param mocker:
    :return:
    """

    class MockLogin(object):
        def __init__(self):
            self.user = None

    mock_login = MockLogin()

    def mocked_callback(*args):
        return mock_login.user

    mocker.patch.object(app.login_manager, 'request_callback', side_effect=mocked_callback)

    yield mock_login
